#include "opencv2\opencv.hpp"
#include <iostream>
#include <string>
#include <vector>

cv::Mat autoRange(cv::Mat& src, int rangeOn, int minpercent, int maxpercent)
{
    cv::Mat dst;
    if (rangeOn == 1)
    {
        double minval, maxval;
        cv::minMaxIdx(src, &minval, &maxval);

        double minimum, maximum;
        minimum = minval + (minpercent / 100.) * (maxval - minval);
        maximum = minval + (maxpercent / 100.) * (maxval - minval);

        double a = 255. / (maximum - minimum);
        double b = -a * minimum;

        src.convertTo(dst, CV_8UC3, a, b);
    }
    else
    {
        dst = src.clone();
    }

    return dst;
}

void convertDepth2World(cv::Mat & depthframe)
{
    float horizontalFov = 45;
    float verticalFov = 60;

    int cols = depthframe.cols;
    int rows = depthframe.rows;

    cv::Mat worldTransform = cv::Mat::zeros(depthframe.size(), CV_32FC3);

    float xzFactor = tan(horizontalFov / 2) * 2;
    float yzFactor = tan(verticalFov / 2) * 2;

    float* normX = new float[cols];
    float* normY = new float[rows];

    for (int i = 0; i < cols; i++)
        normX[i] = (i / static_cast<float>(cols) - 0.5f) * xzFactor;

    for (int i = 0; i < rows; i++)
        normY[i] = (0.5f - i / static_cast<float>(rows)) * yzFactor;

    short* ptrDepth = depthframe.ptr<short>(0, 0);
    cv::Vec3f* ptrWorld = worldTransform.ptr<cv::Vec3f>(0, 0);

    for (int y = 0; y < rows; y++)
        for (int x = 0; x < cols; x++)
            ptrWorld[y * cols + x] = cv::Vec3f(normX[x], normY[y], 1.f);
        
    delete normX;
    delete normY;

    depthframe.convertTo(depthframe, CV_32FC1);
    cv::cvtColor(depthframe, depthframe, cv::COLOR_GRAY2BGR);
    cv::multiply(depthframe, worldTransform, depthframe);
}

//void depthToWorld(float dx, float dy, float dz, float& x float& y, float& z, float horizontalFov, float verticalFov)
//{
//    xzFactor = tan(horizontalFov / 2) * 2;
//    yzFactor = tan(verticalFov / 2) * 2;
//
//    float normalizedX = dx / resolutionX - 0.5f;
//    float normalizedY = 0.5f - dy / resolutionY;
//
//    x = normalizedX * dz * xzFactor;
//    y = normalizedY * dz * yzFactor;
//    z = dz;
//}

void main()
{
    cv::Mat depthframe = cv::imread("depthframe.png", cv::IMREAD_ANYDEPTH);

    convertDepth2World(depthframe);

    std::vector<cv::Mat> channels;
    cv::split(depthframe, channels);

    for (int i = 0; i < channels.size(); i++)
    {
       
        cv::imshow("channel " + std::to_string(i), autoRange(channels[i], 1, 0, 100));
    }

    cv::waitKey();
    cv::destroyAllWindows();
}