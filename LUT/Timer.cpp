﻿/**
 * \file     Timer.cpp
 *
 * \brief    Implementation of Timer class.
 *
 * \author   Gennadi Eirich (gennadi.eirich@pdr-team.com)
 * \date     17.05.2016
 *
 * \version  0.1.0
 *
 * \note     Copyright (c) 2016, PDR-Team GmbH
 */


#include <iostream>

#include "Timer.hpp"


Timer::Timer()
{
}


void Timer::start()
{
    _start = std::chrono::high_resolution_clock::now();
}


void Timer::stop()
{
    _end = std::chrono::high_resolution_clock::now();
    this->printDuration();
}


void Timer::printDuration() const
{
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(_end - _start).count();
    std::cout << "Duration: " << duration << " ms" << std::endl;
}


Timer::~Timer()
{
}
