#include <string>
#include <iostream>
#include "opencv2\opencv.hpp"
#include "Timer.hpp"

cv::Mat grayImgLUT(cv::Mat image, double minval, double maxval)
{
    cv::Mat newimg;
    cv::Mat falsecolorimg = cv::Mat::zeros(image.size(), CV_8UC3);


    //double minval, maxval;
    //cv::minMaxLoc(image, &minval, &maxval);

    double minimum, maximum;
    minimum = minval;
    maximum = minval + (maxval - minval);

    double a = 65536 / (maximum - minimum);
    double b = -a * minimum;



    image.convertTo(newimg, CV_16UC1, a, b);

    Timer time;
    time.start();
    cv::Mat lut = cv::Mat::zeros(1, 65536, CV_8UC3);
    time.stop();

    cv::Vec3b *ptrLUT = lut.ptr<cv::Vec3b>(0);
    int h = 0, s = 0, v = 0;
    int scalefactor = 10;
    for (int i = 0; i < lut.cols; i++)
    {
        h = 131- ((i+100) / 500);
        if (i / (256*scalefactor) % 2 == 0)
        {
            v = i % (256 * scalefactor) / scalefactor;
            s = 255;
        }
        else
        {
            v = 255;
            s = 255 - ((i % (256 * scalefactor)) / scalefactor);
        }
        ptrLUT[i][0] = h;
        ptrLUT[i][1] = s;
        ptrLUT[i][2] = v;

    }

    cv::cvtColor(lut, lut, cv::COLOR_HSV2BGR);



    cv::Vec3b*  ptrImg;
    ushort*      ptrNew;

    ptrImg = falsecolorimg.ptr<cv::Vec3b>(0, 0);
    ptrNew = newimg.ptr<ushort>(0, 0);

    for (int y = 0; y < falsecolorimg.rows * falsecolorimg.cols; y++)
        ptrImg[y] = ptrLUT[ptrNew[y]];
    

    return falsecolorimg;
}

void main()
{
    // Create testimage
    cv::Mat testimg = cv::Mat::zeros(256, 4000, CV_32FC1);

    float* ptr;
    for (int y = 0; y < testimg.rows; y++)
    {
        ptr = testimg.ptr<float>(y, 0);
        for (int x = 0; x < testimg.cols; x++)
            ptr[x] = x;

    }

    testimg = cv::imread("frame.png", cv::IMREAD_ANYDEPTH);

    // Use LUT
    //Timer mytime;
    //mytime.start();
    cv::Mat newimage = grayImgLUT(testimg, 0, 3000);
    //mytime.stop();

    // Display result
    cv::imshow("newimage", newimage);
    cv::waitKey();
    cv::destroyAllWindows();
}