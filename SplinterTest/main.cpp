#include <iostream>
#include <stdlib.h>
#include "opencv2\opencv.hpp"
#include "datatable.h"
#include "bspline.h"
#include "bsplinebuilder.h"

using std::cout;
using std::endl;

using namespace SPLINTER;

// Six-hump camelback function
double f(double x)
{
    int randsize = 50;

    if (x > 600.)
        return 360 + (rand() % randsize);

    double value = 0.001 * x * x + (rand() % randsize);

    return value;
}

int main(int argc, char *argv[])
{
    // Create new DataTable to manage samples
    DataTable samples;

    // Sample the function
    double y;
    std::vector<double> yVals;

    int samplecount = 1280;

    for (int i = 0; i < samplecount; i++)
    {
        // Store sample
        yVals.push_back(f(i));
        samples.addSample(i, yVals[i]);
   
    }

    for (int i = 0; i < samplecount; i++)
    {
        cv::Mat image = cv::Mat::zeros(1024, 1280, CV_8UC3);


        // Build penalized B-spline (P-spline) that smooths the samples
        BSpline pspline = BSpline::Builder(samples)
            .degree(1)
            .smoothing(BSpline::Smoothing::PSPLINE)
            .alpha(pow(10., i/100.))
            .build();

        /* Evaluate the approximants at x = (1,1)
        * Note that the error will be 0 at that point (except for the P-spline, which may introduce an error
        * in favor of a smooth approximation) because it is a point we sampled at.
        */


        std::vector<double> x;
        std::vector<double> splinecurve;

        for (int i = 0; i < samplecount; i++)
        {
            x.clear();
            x.push_back(i);

            splinecurve.push_back(pspline.eval(x));
        }




        for (int i = 1; i < samplecount; i++)
        {
            cv::circle(image, cv::Point(i, 1024 - yVals[i]), 1, cv::Scalar(0, 255, 0), 1);
            cv::line(image, cv::Point(i - 1, 1024 - splinecurve[i - 1]), cv::Point(i, 1024 - splinecurve[i]), cv::Scalar(0, 0, 255), 2, cv::LINE_AA);
        }

        cv::imshow("Image", image);
        cv::waitKey(10);
    }

    return 0;
}