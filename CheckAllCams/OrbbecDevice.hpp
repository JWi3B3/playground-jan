#pragma once


/**
 * \file     OrbbecDevice.hpp
 *
 * \brief    Interface class to the Orbbec Astra cameras.
 *
 * \details  ...
 *
 * \author   Timo Blust (timo.blust@pdr-team.com)
 * \date     29.09.2016
 *
 * \version  0.1.0
 *
 * \note     Copyright (c) 2016, PDR-Team GmbH
 */

#define NOMINMAX

#include <memory>
#include <vector>

#include "CameraDevice.hpp"

#include "OpenNI.h"
#include "XnUSB.h"
#include <opencv2/opencv.hpp>
#include <chrono>

using namespace std::chrono_literals;

class OrbbecDevice : public CameraDevice, public openni::VideoStream::NewFrameListener, /*public openni::OpenNI::DeviceConnectedListener, */public openni::OpenNI::DeviceDisconnectedListener
{
    public:
        OrbbecDevice();
        explicit OrbbecDevice(const char* uri);
        ~OrbbecDevice();

        enum StreamType
        {
            STREAMTYPE_NONE,
            STREAMTYPE_IR,
            STREAMTYPE_COLOR,
            STREAMTYPE_DEPTH,
        };

        /**
         * Initializes orbbec devices
         */
        static void initialize();

        /**
         * Shuts down orbbec devices
         */
        static void shutdown();

        /**
         * \brief Checks, if this instance is connected to a hardware device.
         */
        bool isOpen() const override;

        /**
         * \brief Opens and starts a video stream.
         * \param streamType The type of the sensor that shoud be streamed.
         * \return true, if the stream started successfully.
         */
        bool startStream(StreamType streamType);

        /**
         * \brief Stops the stream and closes it (if it is opened).
         * Use OrbbecDevice::isStreamopen() to check if a stream is running or not.
         */
        void stopStream();

        /**
         * \brief Gets the latest frame of the currently opened stream. If no stream is open, an empty VideoFrameRef is returned.
         * \param timeout The timeout (in ms) to wait before aborting the frame request. If set to -1, the function will wait infinitely.
         * \return A VideoFrameRef that contains the latest frame. The frame is empty, if the timeout is exceeded or the stream is not opened.
         */
        openni::VideoFrameRef getFrame(std::chrono::milliseconds timeout = std::chrono::milliseconds(1000));

        /**
         * \brief Gets the latest frame and converts it to a cv::Mat.
         * \param mat The cv::Mat in which the frame should be stored.
         * \param timeout The timeout (in ms) to wait before aborting the frame request. If set to -1, the function will wait infinitely.
         * \return true, if the frame was successfully captured and converted, otherwise false.
         */
        bool getFrame(cv::Mat &mat, std::chrono::milliseconds timeout);

        /**
        * \brief Gets the latest frame and converts it to a cv::Mat.
        * \param mat The cv::Mat in which the frame should be stored.
        * \return true, if the frame was successfully captured and converted, otherwise false.
        */
        bool getFrame(cv::Mat &mat) override { return getFrame(mat, std::chrono::milliseconds(1000)); }


        /**
         * \brief Checks, if a frame is currently available.
         */
        bool hasFrame() const;

        /**
         * \brief Sets the state of the IR emitter.
         * \param emitterEnabled true, to switch the emitter on. false, to switch the emitter off.
         * \note The emitter needs some time to shutdown. Therefore it could happen that the following few frames, after switching
         *		 off the emitter, are still lighted more or less. Use OrbbecDevice::setEmitterFrameDelay() to prevent lighting corrupted frames.
         */
        void setEmitter(bool emitterEnabled = true);

        /**
         * \attention This function is not implemented yet.
         * \brief Gets the state of the IR emitter.
         */
        bool getEmitter() const;

        /**
         * \brief Sets the minimum delay between switching the IR emitter and capturing the next valid frame.
         * \param delay The minimum delay (in ms).
         */
        void setEmitterFrameDelay(std::chrono::milliseconds delay);

        /**
         * \brief Gets the minimum delay between switching the IR emitter and capturing the next valid frame.
         */
        std::chrono::milliseconds getEmitterFrameDelay() const;

        /**
         * \brief Sets, if the hardware should mirror the frames internally or not.
         * \param mirrorEnabled true, if the frame should be mirrored.
         */
        void setMirror(bool mirrorEnabled);

        /**
         * \brief Gets, whether the hardware should mirror the frames or not.
         */
        bool getMirror() const;

        /**
         * \brief Gets the 12 character sized serial number of the hardware.
         */
        std::string getSerialNumber() const override;

        /**
         * \brief Gets the URI of the hardware.
         */
        std::string getUri() const;

        /**
         * \brief Checks, if this device currently has an open stream or not.
         */
        bool isStreamOpen() const;
        //bool checkHardwareConnection();

        /**
         * \brief Gets the type of the sensor that is streamed.
         * \return 0, if no stream is open, otherwise the StreamType that describes the sensor.
         */
        OrbbecDevice::StreamType getStreamType() const;

        //bool reconnect();

        /**
         * \brief The callback for new frames (inherited from openni::VideoStream::NewFrameListener).
         */
        void onNewFrame(openni::VideoStream&) override;

        //void onDeviceConnected(const openni::DeviceInfo* device)  override;

        /**
         * \brief The callback for new frames (inherited from openni::OpenNI::DeviceDisconnectedListener).
         */
        void onDeviceDisconnected(const openni::DeviceInfo* device) override;

        /**
         * \brief Gets the pointer of the currently used openni::Device.
         */
        openni::Device* getDevice() const;

        /**
         * \brief Creates a new smart pointer to an OrbbecDevice instance and connects it to the next available hardware.
         *		  If no valid hardware is available, the OrbbecDevice::isOpen() function will return false and the pointer can be deleted.
         */
        static std::shared_ptr<OrbbecDevice> create();

        /**
         * \brief Converts the specified frame into a matrix. This function takes care of the 10bit IR data and scales it automatically.
         */
        static void convertFrameToMat(openni::VideoFrameRef &frame, cv::Mat &mat);

    private:
        int sendCmd(uint16_t cmd, void *cmdbuf, uint16_t cmd_len, void *replybuf, uint16_t reply_len);

        std::shared_ptr<openni::Device> _device;
        XN_USB_DEV_HANDLE _usbHandle = nullptr;
        uint16_t _camTag = 0;
        std::shared_ptr<openni::VideoStream> _stream;
        std::string _uri;
        std::string _serial;
        openni::VideoFrameRef _latestFrame;
        std::chrono::time_point<std::chrono::high_resolution_clock> _latestFrameTime;
        std::chrono::time_point<std::chrono::high_resolution_clock> _emitterSetTime;
        bool _readingFrame = false;
        bool _hasNewFrame = false;
        bool _emitter = true;
        bool _hardwareDisconnected = false;
        std::chrono::milliseconds _frameDelay;
        bool _isValid() const;

        static std::vector<std::weak_ptr<OrbbecDevice>> openDevices;

        typedef struct _cam_hdr {
            uint8_t     magic[2];
            uint16_t    len;
            uint16_t    cmd;
            uint16_t    tag;
        } cam_hdr;

};

