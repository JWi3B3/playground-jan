#include <opencv2\opencv.hpp>
#include "IDSDevice.hpp"
#include <string>
#include "OrbbecDevice.hpp"
#include <chrono>
#include <thread>

void main()
{
    
    std::cout << std::endl <<"Search all IDS and Orbbec Cameras" << std::endl;
    std::cout <<             "................................." << std::endl << std::endl;

    cv::Mat mat;

    // Search 3D Cams
    OrbbecDevice::initialize();
    std::vector<std::shared_ptr<OrbbecDevice>> cams3d;
    std::vector<cv::Mat> frames3d;
    std::vector<std::string> serials;
    std::string serial;

    std::cout << "Orbbec:" << std::endl;
    for (int i = 0; i < 20; i++)
    {
        std::cout << " - Cam " << i << " ...";

        auto cam = OrbbecDevice::create();
        cam->startStream(OrbbecDevice::StreamType::STREAMTYPE_DEPTH);
        
        std::this_thread::sleep_for(std::chrono::milliseconds(50));

        cam->getFrame(mat);
        std::this_thread::sleep_for(std::chrono::milliseconds(50));

        cam->stopStream();
        std::this_thread::sleep_for(std::chrono::milliseconds(50));

        std::string serial = cam->getSerialNumber();

        if (serial == "")
        {
            std::cout << "not found!" << std::endl;
            break;
        }

        std::cout << "found!  Serial: " << serial << std::endl;

        cams3d.push_back(cam);
        mat.convertTo(mat, CV_8UC1, 255. / 6000.);
        frames3d.push_back(mat.clone());
    }

    

    // Search 2D Cams
    
    std::cout << std::endl << "IDS:" << std::endl;

    std::vector<std::shared_ptr<IDSDevice>> cams2d;
    std::vector<cv::Mat> frames2d;

    for (int i = 0; i < 20; i++)
    {
        std::cout << " - Cam " << i << " ...";

        auto cam = IDSDevice::create();

        if (cam->isOpen())
        {
            cams2d.push_back(cam);

            cams2d[i]->start();
            cams2d[i]->getFrame(mat);
            cams2d[i]->stop();
            frames2d.push_back(mat.clone());
            std::cout << "found!  Serial: " << cam->getSerialNumber() << std::endl;
        }
        else
        {
            std::cout << "not found!" << std::endl;
            break;
        }
    }


    

    // Print Report
    std::cout << std::endl <<  "Result: " << std::endl;
    std::cout << "IDS Cams:     " << cams2d.size() << std::endl;
    std::cout << "Orbbec Cams:  " << cams3d.size() << std::endl;

    
    for (int i = 0; i < frames2d.size(); i++)
    {
        if (!frames2d[i].empty())
            cv::imshow("IDS Frame " + std::to_string(i), frames2d[i]);
    }

    for (int i = 0; i < frames3d.size(); i++)
    {
        if (!frames3d[i].empty())
            cv::imshow("Orbbec Frame " + std::to_string(i), frames3d[i]);
    }

    if (!mat.empty())
        cv::waitKey();

    else
        system("pause");
    
}