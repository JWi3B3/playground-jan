/**
 * \file      IDSDevice.cpp
 *
 * \brief     Implementaion of IDSDevice.hpp
 *
 * \author    Gennadi Eirich (gennadi.eirich@pdr-team.com)
 * \date      01.10.2016
 *
 * \version   0.1.0
 *
 * \note      Copyright (c) 2016, PDR-Team GmbH
 */


#include "IDSDevice.hpp"
#include <synchapi.h>


// Iterations for initialized camers
int iteration = 0;


IDSDevice::IDSDevice() :
    _cam(NULL),
    _event(CreateEventA(nullptr, false, false, nullptr)),
    _eventRet(WAIT_FAILED),
    _open(false),
    _serialNo(std::string()),
    _width(0),
    _height(0),
    _bitsPerPixel(0),
    _memoryID(0),
    _imgMemory(nullptr),
    _colorMode(IDSDevice::ColorMode::GRAYSCALE),
    _pixelclock(87),
    _framerate(95),
    _exposure(100)
{
}


IDSDevice::IDSDevice(HIDS camHandle, const std::string& serialNo) :
    _cam(camHandle),
    _event(CreateEventA(nullptr, false, false, nullptr)),
    _eventRet(WAIT_FAILED),
    _open(true),
    _serialNo(serialNo),
    _width(0),
    _height(0),
    _bitsPerPixel(0),
    _memoryID(0),
    _imgMemory(nullptr),
    _colorMode(IDSDevice::ColorMode::GRAYSCALE),
    _pixelclock(87),
    _framerate(95),
    _exposure(100)
{
    _init();
}


std::shared_ptr<IDSDevice> IDSDevice::create()
{
    int nNumCam;

    if (is_GetNumberOfCameras(&nNumCam) == IS_SUCCESS)
        std::cout << nNumCam << " Cameras found" << std::endl;

    auto instance = std::make_shared<IDSDevice>();

    auto cam = static_cast<HIDS>(iteration + 1 | IS_USE_DEVICE_ID);

    auto ret = IDSDevice::initCamera(&cam);

    if (ret == IS_SUCCESS)
    {
        // Get serial number
        CAMINFO camInfo;
        is_GetCameraInfo(cam, &camInfo);

        instance = std::make_shared<IDSDevice>(cam, camInfo.SerNo);
    }
    //else if (ret == IS_CANT_OPEN_DEVICE)
    //    std::cout << "IDS device is not connected" << std::endl;

    // Incerment iteration
    iteration++;

    return instance;
}


void IDSDevice::_init()
{
    // Disable trigger mode
    is_SetExternalTrigger(_cam, IS_SET_TRIGGER_OFF);

    // Get Sensor info
    SENSORINFO sensorInfo;
    is_GetSensorInfo(_cam, &sensorInfo);

    // Set image size
    _width = sensorInfo.nMaxWidth;
    _height = sensorInfo.nMaxHeight;

    // Set values
    setColorMode(_colorMode);
    setPixelClock(_pixelclock);
}


void IDSDevice::setColorMode(const IDSDevice::ColorMode& colorMode)
{
    auto ret = IS_NO_SUCCESS;
    int mode;
    int bitsPerPix;

    switch (colorMode)
    {
        case IDSDevice::ColorMode::GRAYSCALE:
            mode = IS_CM_MONO8;
            bitsPerPix = 8;
            break;
        default: // Color
            mode = IS_CM_BGR8_PACKED;
            bitsPerPix = 24;
            break;
    }

    // Stop video
    _stopVideo();

    // Set new bits per pix value
    _bitsPerPixel = bitsPerPix;

    // Free the memory
    ret = is_FreeImageMem(_cam, _imgMemory, _memoryID);

    // Set new color mode
    ret = is_SetColorMode(_cam, mode);

    // Initialize memory
    ret = is_AllocImageMem(_cam, _width, _height, _bitsPerPixel, &(_imgMemory), &(_memoryID));
    ret = is_SetImageMem(_cam, _imgMemory, _memoryID); // activate image memory

    // Set instance value
    _colorMode = colorMode;

    // Start video again
    _startVideo();
}


void IDSDevice::setPixelClock(const int& value)
{
    auto ret = IS_NO_SUCCESS;
    int pixelclock;

    int range[3];
    ZeroMemory(range, sizeof(range));

    // Get pixelclock-range
    ret = is_PixelClock(_cam, IS_PIXELCLOCK_CMD_GET_RANGE, static_cast<void*>(range), sizeof(range));

    auto min = range[0];
    auto max = range[1];

    // Calc new pixelclock value. find best pixelclock from list.
    {
        pixelclock = static_cast<int>(min + ((max - min) * (value / 100.0)));

        // receive the pixelclock number count
        UINT count = 0;
        is_PixelClock(_cam, IS_PIXELCLOCK_CMD_GET_NUMBER, static_cast<void*>(&count), sizeof(count));

        // Receive pixelclock list
        int pixelClockList[150];
        ZeroMemory(&pixelClockList, sizeof(pixelClockList));
        is_PixelClock(_cam, IS_PIXELCLOCK_CMD_GET_LIST, static_cast<void*>(pixelClockList), count * sizeof(UINT));

        // Iterate over all available pixelclocks and find ne next bigger one and set that value to new pixelclock
        for (auto i = 0; i < static_cast<int>(count); i++)
        {
            if (pixelClockList[i] >= pixelclock)
            {
                pixelclock = pixelClockList[i];
                break;
            }
        }
    }

    // Set new pixelclock
    ret = is_PixelClock(_cam, IS_PIXELCLOCK_CMD_SET, static_cast<void*>(&pixelclock), sizeof(pixelclock));

    // Set instance value
    _pixelclock = value;

    // Update framerate because it's range is changing on pixelclock change
    setFramerate(_framerate);
}


void IDSDevice::setFramerate(const int& value)
{
    auto ret = IS_NO_SUCCESS;
    int framerate;

    // Framerange values
    auto min = 0.0;
    auto max = 0.0;

    // Get framerate range
    {
        auto Tmin = 0.0;
        auto Tmax = 0.0;
        auto Tstep = 0.0;

        // Get frame time range
        ret = is_GetFrameTimeRange(_cam, &Tmin, &Tmax, &Tstep);

        // Calc frame range (Referring to the documentation)
        min = 1 / Tmax;
        max = 1 / Tmin;
    }

    // Calculate new framerate
    framerate = static_cast<int>(min + ((max - min) * (value / 100.0)));

    // Set new framerate
    auto currentFPS = 0.0;
    ret = is_SetFrameRate(_cam, framerate, &currentFPS);

    // Set instance value
    _framerate = value;

    // Update exposure because range changes on framerate change
    setExposure(_exposure);
}


void IDSDevice::setExposure(const int& value)
{
    auto ret = IS_NO_SUCCESS;
    double exposure;

    double range[3];
    ZeroMemory(range, sizeof(range));

    // Get exposure range
    ret = is_Exposure(_cam, IS_EXPOSURE_CMD_GET_EXPOSURE_RANGE, static_cast<void*>(&range), sizeof(range));

    auto min = range[0];
    auto max = range[1];

    // Calculate new exposure
    exposure = min + ((max - min) * (value / 100.0));

    // Set new exposure
    ret = is_Exposure(_cam, IS_EXPOSURE_CMD_SET_EXPOSURE, static_cast<void*>(&exposure), sizeof(exposure));

    // Set instance value
    _exposure = value;
}


void IDSDevice::setDefaults()
{
    auto ret = IS_NO_SUCCESS;

    // Get default pixelclock
    auto pixelclock = 0;
    {
        auto defPixelclock = 0;
        ret = is_PixelClock(_cam, IS_PIXELCLOCK_CMD_GET_DEFAULT, static_cast<void*>(&defPixelclock), sizeof(defPixelclock));
        if (ret == IS_SUCCESS)
        {
            // restore to default pixelclock
            ret = is_PixelClock(_cam, IS_PIXELCLOCK_CMD_SET, static_cast<void*>(&defPixelclock), sizeof(defPixelclock));

            if (ret == IS_SUCCESS)
            {
                // Get range
                int range[3];
                ret = is_PixelClock(_cam, IS_PIXELCLOCK_CMD_GET_RANGE, static_cast<void*>(range), sizeof(range));

                pixelclock = static_cast<int>(((defPixelclock - range[0]) / static_cast<double>(range[1] - range[0])) * 100.0);
            }
        }
    }

    // Get default framerate
    auto framerate = 0;
    {
        auto defFramerate = 0.0;
        ret = is_SetFrameRate(_cam, IS_GET_DEFAULT_FRAMERATE, &defFramerate);
        if (ret == IS_SUCCESS)
        {
            // restore to default framerate
            double temp;
            ret = is_SetFrameRate(_cam, defFramerate, &temp);

            if (ret == IS_SUCCESS)
            {
                // Framerange values
                auto Tmin = 0.0;
                auto Tmax = 0.0;
                auto Tstep = 0.0;

                ret = is_GetFrameTimeRange(_cam, &Tmin, &Tmax, &Tstep);

                framerate = static_cast<int>(((temp - 1 / Tmax) / static_cast<double>(1 / Tmin - 1 / Tmax)) * 100.0);
            }
        }
    }

    // Get default exposure
    auto exposure = 0;
    {
        auto defExposure = 0.0;
        ret = is_Exposure(_cam, IS_EXPOSURE_CMD_GET_EXPOSURE_DEFAULT, static_cast<void*>(&defExposure), sizeof(defExposure));
        if (ret == IS_SUCCESS)
        {
            // restore to default exposure time
            ret = is_Exposure(_cam, IS_EXPOSURE_CMD_SET_EXPOSURE, static_cast<void*>(&defExposure), sizeof(defExposure));

            if (ret == IS_SUCCESS)
            {
                // Get range
                double range[3];
                ret = is_Exposure(_cam, IS_EXPOSURE_CMD_GET_EXPOSURE_RANGE, static_cast<void*>(&range), sizeof(range));

                exposure = static_cast<int>(((defExposure - range[0]) / static_cast<double>(range[1] - range[0])) * 100.0);
            }
        }
    }

    // Set instance values
    _pixelclock = pixelclock;
    _framerate = framerate;
    _exposure = exposure;
}


void IDSDevice::_startVideo() const
{
    auto ret = is_CaptureVideo(_cam, IS_DONT_WAIT);
}


void IDSDevice::_stopVideo() const
{
    auto ret = is_StopLiveVideo(_cam, IS_WAIT);
}


void IDSDevice::_startEvents() const
{
    is_InitEvent(_cam, _event, IS_SET_EVENT_FRAME); // Initialize event
    is_EnableEvent(_cam, IS_SET_EVENT_FRAME); // Activate event
}


void IDSDevice::_stopEvents() const
{
    is_DisableEvent(_cam, IS_SET_EVENT_FRAME);
    is_ExitEvent(_cam, IS_SET_EVENT_FRAME);
}


bool IDSDevice::getFrame(cv::Mat& frame)
{
    _eventRet = WaitForSingleObject(_event, 1000);

    if (_eventRet == WAIT_OBJECT_0) // Frame available
    {
        if (_colorMode == IDSDevice::ColorMode::GRAYSCALE) // Grayscale
            frame = cv::Mat(_height, _width, CV_8UC1, _imgMemory).clone();
        else // Color
            frame = cv::Mat(_height, _width, CV_8UC3, _imgMemory).clone();

        return true;
    }

    return false;
}


void IDSDevice::start() const
{
    _stopEvents();
    _startEvents();
    _startVideo();
}


void IDSDevice::stop() const
{
    _stopVideo();
    _stopEvents();
}


/**
 * Initilizes the given camera
 */
int IDSDevice::initCamera(HIDS* hCam)
{
    auto nRet = is_InitCamera(hCam, nullptr);
    /************************************************************************************************/
    /*                                                                                              */
    /*  If the camera returns with "IS_STARTER_FW_UPLOAD_NEEDED", an upload of a new firmware       */
    /*  is necessary. This upload can take several seconds. We recommend to check the required      */
    /*  time with the function is_GetDuration().                                                    */
    /*                                                                                              */
    /*  In this case, the camera can only be opened if the flag "IS_ALLOW_STARTER_FW_UPLOAD"        */
    /*  is "OR"-ed to m_hCam. This flag allows an automatic upload of the firmware.                 */
    /*                                                                                              */
    /************************************************************************************************/
    if (nRet == IS_STARTER_FW_UPLOAD_NEEDED)
    {
        //Logger::getInstance().log("IDS Firmware Update needed.", Logger::Type::DEVICE, Logger::Level::WARNING);

        // Time for the firmware upload = 25 seconds by default
        auto nUploadTime = 25000;
        is_GetDuration(*hCam, IS_STARTER_FW_UPLOAD, &nUploadTime);

        // Try again to open the camera. This time we allow the automatic upload of the firmware by
        // specifying "IS_ALLOW_STARTER_FIRMWARE_UPLOAD"
        *hCam = static_cast<HIDS>((static_cast<INT>(*hCam) | IS_ALLOW_STARTER_FW_UPLOAD));
        nRet = is_InitCamera(hCam, nullptr);
    }

    return nRet;
}


IDSDevice::~IDSDevice()
{
    // Free memory and close cam
    is_FreeImageMem(_cam, _imgMemory, _memoryID);
    is_ExitCamera(_cam); // Close cam

    // Close event handle
    CloseHandle(_event);
}
