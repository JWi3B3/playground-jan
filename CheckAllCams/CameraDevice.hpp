#pragma once


/**
 * \file     CameraDevice.hpp
 *
 * \brief    Abstract base class for camera devices.
 *
 * \details  ...
 *
 * \author   Timo Blust (timo.blust@pdr-team.com)
 * \date     29.09.2016
 *
 * \version  0.1.0
 *
 * \note     Copyright (c) 2016, PDR-Team GmbH
 */


#include <string>
#include <opencv2/opencv.hpp>


class CameraDevice
{
    public:
        CameraDevice() = default;
        virtual ~CameraDevice() = default;

        // Gets the serial number of the device.
        virtual std::string getSerialNumber() const = 0;

        // Checks, whether the device is open and available or not.
        virtual bool isOpen() const = 0;

        virtual bool getFrame(cv::Mat &frame) = 0;

};
