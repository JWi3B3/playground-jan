#pragma once


/**
 * \file      IDSDevice.hpp
 *
 * \brief     Holds all functionality for the IDS camera.
 *
 * \details  ...
 *
 * \author    Gennadi Eirich (gennadi.eirich@pdr-team.com)
 * \date      01.10.2016
 *
 * \version   0.1.0
 *
 * \note      Copyright (c) 2016, PDR-Team GmbH
 */


#include <string>
#include <memory>

#include "CameraDevice.hpp"

#include "IDS/uEye.h"


class IDSDevice : public CameraDevice
{
    public:
        /**
         * C'tor for an invalid camera
         */
        IDSDevice();

        /**
         * C'tor for succesfull initialized device
         */
        IDSDevice(HIDS camHandle, const std::string& serialNo);

        ~IDSDevice();

        enum ColorMode { COLOR, GRAYSCALE };

        /**
         * Creates a new instance of IDSDevice
         */
        static std::shared_ptr<IDSDevice> create();
        static int initCamera(HIDS *hCam);

        /**
         * Return serial number of this cam.
         */
        std::string getSerialNumber() const override { return _serialNo; }

        /**
         * Returns whether a cam is open.
         */
        bool isOpen() const override { return _open; }

        /**
         * Writes new frame to given frame.
         */
        bool getFrame(cv::Mat &frame) override;

        /**
         * Starts the capturing
         */
        void start() const;

        /**
         * Stops capturing
         */
        void stop() const;

        /**
         * Sets colormode.
         */
        void setColorMode(const IDSDevice::ColorMode& colorMode);

        /**
         * Return color mode
         */
        IDSDevice::ColorMode getColorMode() const { return _colorMode; };

        /**
         * Sets Pixelclock
         */
        void setPixelClock(const int &value);

        /**
         * Returns pixelxlock
         */
        int getPixelClock() const { return _pixelclock; };

        /**
         * Sets framerate
         */
        void setFramerate(const int& value);

        /**
         * Returns framerate
         */
        int getFramerate() const { return _framerate; }

        /**
         * Sets exposure
         */
        void setExposure(const int& value);

        /**
         * Returns exposure
         */
        int getExposure() const { return _exposure; }

        /**
         * Sets all values back to fatory defaults
         */
        void setDefaults();

    private:
        HIDS              _cam;
        HANDLE            _event;
        DWORD             _eventRet;
        const bool        _open;
        const std::string _serialNo;

        int _width;
        int _height;
        int _bitsPerPixel;

        int   _memoryID;  // buffer id
        char* _imgMemory; // buffer

        IDSDevice::ColorMode _colorMode;
        int                  _pixelclock;
        int                  _framerate;
        int                  _exposure;

        void _init();
        void _startVideo() const;
        void _stopVideo() const;
        void _startEvents() const;
        void _stopEvents() const;

};
