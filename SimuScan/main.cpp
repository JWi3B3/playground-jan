#include <opencv2\opencv.hpp>
#include <vector>
#include <string>
#include <iostream>

void main()
{
    int imagecount = 343;
    std::vector<cv::Mat> frames;

    // Load images
    for (int i = 1; i < imagecount; i++)
    {
        std::string number = std::to_string(i);
        number = std::string(4 - number.length(), '0') + number;
        std::string path = "panoNew/pano" + number + ".png";
        
        std::cout << path << std::endl;

        frames.push_back(cv::imread(path, cv::IMREAD_GRAYSCALE).clone());

        if (frames[0].rows == 0)
        {
            std::cout << "Path is wrong: " << path << std::endl;
            system("pause");
            return;
        }
    }

    // Reverse frames
    std::reverse(frames.begin(), frames.end());

    // Combine images
    cv::Mat map = cv::Mat::zeros(frames.size()*frames[0].rows, frames[0].cols, CV_8UC1);
    cv::Rect roi;

    for (int i = 0; i < frames.size(); i++)
    {
        roi = cv::Rect(0, i*frames[0].rows, frames[0].cols, frames[0].rows);
        cv::flip(frames[i], frames[i], 0);
        cv::imshow("frame", frames[i]);
        cv::waitKey(1);
        frames[i].copyTo(map(roi));
    }

    cv::imwrite("map.png", map);
    cv::imshow("map", map);
    cv::waitKey();
    cv::destroyAllWindows();
}