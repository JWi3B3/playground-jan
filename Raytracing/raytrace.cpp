#include "raytrace.h"



Raytrace::Raytrace() :
    _centerX(400),
    _centerY(300),
    _h(0),
    _px(0),
    _py(30),
    _d(30)
{
    _calcHeight();
}


Raytrace::~Raytrace()
{
}

void Raytrace::set_px(double px)
{
    _px = px;
    _calcHeight();
}

void Raytrace::set_py(double py)
{
    _py = py;
    _calcHeight();
}

void Raytrace::set_d(double d)
{
    _d= d;
    _calcHeight();
}

void Raytrace::_calcHeight()
{
    _h = (_d * _py) / (2 * _d - _px);
}

void Raytrace::draw(cv::Mat & canvas)
{

    cv::Scalar colorCam = cv::Scalar(255, 100, 0);
    cv::Scalar colorRay = cv::Scalar(0, 255, 0);
    cv::Scalar colorEmitter = cv::Scalar(0, 255, 255);


    int camsize = 5;

    

    // draw rays
    cv::line(canvas, cv::Point(_centerX, _centerY), cv::Point(_centerX + _d, _centerY - _h), colorRay);
    cv::line(canvas, cv::Point(_centerX + _d, _centerY - _h), cv::Point(_centerX + _px, _centerY - _py), colorRay);


    //draw emitter
    cv::circle(canvas, cv::Point(_centerX + _px, _centerY - _py), 5, colorEmitter);

    // draw camera
    cv::line(canvas, cv::Point(_centerX, _centerY + camsize), cv::Point(_centerX, _centerY - camsize), colorCam);
    cv::line(canvas, cv::Point(_centerX, _centerY - camsize), cv::Point(_centerX - camsize, _centerY), colorCam);
    cv::line(canvas, cv::Point(_centerX - camsize, _centerY), cv::Point(_centerX, _centerY + camsize), colorCam);
    cv::line(canvas, cv::Point(_centerX - camsize, _centerY + camsize), cv::Point(_centerX - camsize, _centerY - camsize), colorCam);
    cv::line(canvas, cv::Point(_centerX - 3 * camsize, _centerY - camsize), cv::Point(_centerX - camsize, _centerY - camsize), colorCam);
    cv::line(canvas, cv::Point(_centerX - 3 * camsize, _centerY - camsize), cv::Point(_centerX - 3 * camsize, _centerY + camsize), colorCam);
    cv::line(canvas, cv::Point(_centerX - 3 * camsize, _centerY + camsize), cv::Point(_centerX - camsize, _centerY + camsize), colorCam);
}


