#include "opencv2\opencv.hpp"

#pragma once
class Raytrace
{
public:
    Raytrace();
    ~Raytrace();

    void set_px(double px);
    void set_py(double py);
    void set_d(double d);

    void draw(cv::Mat& canvas);

private:
    void _calcHeight();

    double _centerX, _centerY;
    double _h, _px, _py, _d;
};

