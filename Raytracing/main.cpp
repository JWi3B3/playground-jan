#include <opencv2\opencv.hpp>
#define _USE_MATH_DEFINES
#include <math.h>
#include <vector>
#include <string>
#include <iostream>


#include "raytrace.h"

using namespace std;
using namespace cv;


void main()
{
    Mat canvas = Mat::zeros(600, 800, CV_8UC4);

    int distance = 30;
    int base = 60;
    int angle = 90;
    int offset = 200;


    namedWindow("Settings");
    createTrackbar("base", "Settings", &base, 500);
    createTrackbar("angle", "Settings", &angle, 180);
    createTrackbar("distance", "Settings", &distance, 800);
    createTrackbar("offset", "Settings", &offset, 400);

    Raytrace rt1;
    Raytrace rt2;

    while (true)
    {
        canvas *= 0;

        rt1.set_px(sin((angle - 90.)*M_PI / 180.)* (base / 2 + (offset - 200)));
        rt1.set_py(cos((angle - 90.)*M_PI / 180.)* (base / 2 + (offset - 200)));
        rt1.set_d(distance);
        rt1.draw(canvas);

        rt2.set_px(-sin((angle - 90.)*M_PI / 180.) * (base / 2 - (offset - 200)));
        rt2.set_py(-cos((angle - 90.)*M_PI / 180.) * (base / 2 - (offset - 200)));
        rt2.set_d(distance);
        rt2.draw(canvas);

        imshow("Ratracing", canvas);

        if (waitKey(1) == 27)
            break;
    }
    destroyAllWindows();
}