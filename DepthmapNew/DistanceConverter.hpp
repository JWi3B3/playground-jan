/**
 * \file     DistanceConverter.hpp
 *
 * \brief    This class...
 *
 * \details  ...
 *
 * \author   Boris Tatchou (boris.tatchou@pdr-team.com)
 * \date     09.08.2016
 *
 * \version  0.1.0
 *
 * \note     Copyright (c) 2016, PDR-Team GmbH
 */


#pragma once

#include "opencv2\opencv.hpp"
#include <vector>


struct Capture
{
    cv::Mat frame;
    double position;
};

class DistanceConverter
{
    public:
        DistanceConverter();
        DistanceConverter(std::vector<Capture> captureList, double pixel_per_mm);
        double to_mm(double pixelposition);
        double to_cm(double pixelposition);
        double to_pixel(double distance);
        double getPixelwidth();
        double getMinDistance();
        double getMaxDistance();
        ~DistanceConverter();

    private:
        double _slope;
        double _offset;
        double _width_mm;
        double _width_pixel;
        double _minDistance;
        double _maxDistance;

        void _set_min_max(std::vector<Capture>& captureList);
};

