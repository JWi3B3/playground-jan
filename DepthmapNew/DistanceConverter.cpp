/**
* \file     DistanceConverter.cpp
*
* \brief    Implementation of DistanceConverter class.
*
* \author   Boris Tatchou (boris.tatchou@pdr-team.com)
* \date     09.08.2016
*
* \version  0.1.0
*
* \note     Copyright (c) 2016, PDR-Team GmbH
*/

#include "DistanceConverter.hpp"


DistanceConverter::DistanceConverter()
{
}

DistanceConverter::DistanceConverter(std::vector<Capture> captureList, double pixel_per_mm):
    _slope(pixel_per_mm), _offset(0), _width_mm(0), _width_pixel(0)
{
    _set_min_max(captureList);

    _offset = -_slope * this->_minDistance;

    _width_mm = this->_maxDistance - this->_minDistance;
    _width_pixel = std::abs(to_pixel(this->_maxDistance) - to_pixel(this->_minDistance));
}


double DistanceConverter::to_mm(double pixelposition)
{
    return (pixelposition - _offset) / _slope;
}


double DistanceConverter::to_cm(double pixelposition)
{
    return 0.0;
}


double DistanceConverter::to_pixel(double distance)
{
    return distance * _slope + _offset;
}


double DistanceConverter::getPixelwidth()
{
    return _width_pixel;
}


void DistanceConverter::_set_min_max(std::vector<Capture>& captureList)
{
    this->_minDistance = captureList[0].position;
    this->_maxDistance = captureList[0].position;

    for (unsigned i = 0; i < captureList.size(); i++)
    {
        if (captureList[i].position > this->_maxDistance)
            this->_maxDistance = captureList[i].position;

        if (captureList[i].position < this->_minDistance)
            this->_minDistance = captureList[i].position;
    }
}

double DistanceConverter::getMinDistance()
{
    return this->_minDistance;
}

double DistanceConverter::getMaxDistance()
{
    return this->_maxDistance;
}


DistanceConverter::~DistanceConverter()
{
}
