#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include "opencv2\opencv.hpp"
#include "DistanceConverter.hpp"




std::vector<Capture> loadScan(std::string path, int startframe, int endframe)
{
    std::ifstream file(path + "/" + "distances.txt");
    std::string distance_string;

    std::vector<Capture> scan;
    std::vector<double> positions;
    int linecount = 0;
    while (std::getline(file, distance_string))
    {
        positions.push_back(std::stod(distance_string));
        linecount++;
    }
    
    if (endframe < startframe)
        std::cout << "ERROR: loadScan: startframe and endframe are bad!" << std::endl;

    startframe = std::max(0, std::min(startframe, linecount-1));
    endframe = std::max(0, std::min(endframe, linecount - 1));


    std::cout << std::endl;

    for (int i = startframe; i < endframe+1; i++)
    {
        Capture singleframe;
        singleframe.frame = cv::imread(path + "/" + std::to_string(i) + ".png", cv::IMREAD_ANYDEPTH);
        singleframe.position = positions[i];
        scan.push_back(singleframe);

        std::cout << "\r";
        std::cout << "Load Frame " << i + 1 << " of " << linecount << "...";
    }
    std::cout << "finished!" << std::endl;

    file.close();

    return scan;
}


float moveXpos(int x, int y, int depth, float offset_mm, int imagewidth)
{
    float xzFactor = tan(0.7853981634 / 2) * 2;
    float newdx = (((x / imagewidth - 0.5f) * depth * xzFactor) + offset_mm / (depth * xzFactor) + 0.5f) * imagewidth;
    return newdx;
}

float calcWorldX(float px, float depth)
{
    return (px / 480 - 0.5f) * depth * 0.01370799854;
}


cv::Mat createDepthmap(std::vector<Capture> scan, DistanceConverter dc)
{
    // Settings
    int maxIterations = 1;

    // Variables
    cv::Mat map             = cv::Mat::zeros(scan[0].frame.rows, dc.getPixelwidth(), CV_16UC1);
    int     widthMap        = map.cols;
    int     heightMap       = map.rows;
    int     widthImage      = scan[0].frame.cols;
    int     heightImage     = scan[0].frame.rows;
    short*  ptrMap          = map.ptr<short>(0);
    short*  ptrFrame;
    int     grayvalue       = 0;
    double  currentposition = 0;
    int     currentIdx      = 0;
    int     lastXFrame      = 0;
    int     xWorld          = 0;
    int     lastXWorld      = 0;
    cv::Mat mapShow;


    for (int x = 0; x < widthMap; x++)
    {
        currentposition = dc.to_mm(x);

        // Search nearest frame
        double mindist = 0;
        currentIdx = -1;
        for (int i = 0; i < scan.size(); i++)
        {
            mindist = scan[i].position - currentposition;

            if (mindist < 0)
            {
                std::cout << "mindist: " << mindist << ", currentIdx: " << i << std::endl;
                currentIdx = i;

                break;
            }
        }

        if (currentIdx == -1)
            continue;

        for (int y = 0; y < heightMap; y++)
        {
            // Search for a depth information for this position in multiple frames
            for (int i = 0; i < maxIterations; i++)
            {
                int frameIndex = currentIdx + (i * pow(-1, i));
                frameIndex = std::max(0, std::min(frameIndex, static_cast<int>(scan.size()-1)));

                ptrFrame = scan[frameIndex].frame.ptr<short>(y, 0);

                grayvalue = 0;
                lastXWorld = -1;
                lastXFrame = -1;
                // Search in current frame for the depth information
                for (int xframe = 0; xframe < widthImage; xframe++)
                {
                    // If row is black, continue
                    if (cv::sum(scan[frameIndex].frame(cv::Rect(0, y, widthImage, 1)))[0] < 100)
                        continue;

                    if (ptrFrame[xframe] != 0)
                    {
                        xWorld = calcWorldX(xframe, ptrFrame[xframe]);// +scan[frameIndex].position - currentposition;
                      
                        std::cout << "xframe, xworld, depth: " << xframe << ", " << xWorld << ", " << ptrFrame[xframe]<< std::endl;

                        if (lastXWorld != 0)
                        {
                            if (xWorld / lastXWorld < 0)
                                std::cout << "Inside: xworld, lastXworld: " << xWorld << ", " << lastXWorld << std::endl;

                            if (xWorld / lastXWorld < 0 && lastXFrame == xframe - 1 && lastXWorld != -1)
                            {
                                grayvalue = ptrFrame[xframe];
                                std::cout << xWorld << std::endl;

                                break;
                            
                            }
                        }

                        lastXWorld = xWorld;
                        lastXFrame = xframe;
                    }

                }
                std::cout << std::endl << " -- Next Frame --" << std::endl;

            }
            if (grayvalue != 0)
                std::cout << grayvalue << std::endl;
            ptrMap[y*widthMap + x] = grayvalue;
        }

        std::cout << "column " << x << std::endl;

        map.convertTo(mapShow, CV_8UC1, 0.1);
        cv::imshow("map", mapShow);
        cv::waitKey(1);
    }

    return map;
}

void main()
{
    std::vector<Capture> scan = loadScan("scan", 100, 150);

    DistanceConverter dc = DistanceConverter(scan, 0.5);

    cv::Mat depthmap = createDepthmap(scan, dc);

    //cv::Mat frame;
    //for (int i = 0; i < scan.size(); i++)
    //{
    //    frame = scan[i].frame;
    //    frame.convertTo(frame, CV_8UC1, 0.1);
    //    cv::imshow("frame", frame);
    //    cv::waitKey(1);
    //    std::cout << scan[i].position << std::endl;
    //}
}