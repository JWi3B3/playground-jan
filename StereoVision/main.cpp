#include <opencv2\opencv.hpp>

/* NOTE:
You need at least two pairs from the same two cameras.
*/

#include <vector>
#include <string>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

using namespace cv;


std::vector<Point3f> Create3DChessboardCorners(Size boardSize, double squareSize, double originX, double originY, double originZ)
{
    // This function creates the 3D points of your chessboard in its own coordinate system

    std::vector<Point3f> corners;

    for (int i = 0; i < boardSize.height; i++)
    {
        for (int j = 0; j < boardSize.width; j++)
        {
            corners.push_back(Point3f(float(j*squareSize + originX),
                float(i*squareSize + originY), originZ));
        }
    }

    return corners;
}


int main()
{
    Size boardSize(6, 9);
    const double squareSize = 0.02;

    std::string camera1image1FileName = "left.png";
    std::string camera1image2FileName = "left2.png";

    std::string camera2image1FileName = "right.png";
    std::string camera2image2FileName = "right2.png";

    Mat camera1image1 = imread(camera1image1FileName, 0); // 1 is color, 0 is grayscale
    Mat camera1image2 = imread(camera1image2FileName, 0);

    Mat camera2image1 = imread(camera2image1FileName, 0);
    Mat camera2image2 = imread(camera2image2FileName, 0);

    Size imageSize = camera1image1.size();

    if (camera1image2.size() != imageSize || camera1image1.size() != imageSize || camera2image2.size() != imageSize)
    {
        std::cerr << "All images must be the same size!" << std::endl;
        return -1;
    }

    // find corners of the first board
    std::vector<std::vector<Point2f> > camera1ImagePoints(2);
    bool found1_1 = findChessboardCorners(camera1image1, boardSize, camera1ImagePoints[0]);
    if (!found1_1)
    {
        std::cerr << "Checkboard1_1 corners not found!" << std::endl;
        exit(-1);
    }

    bool found1_2 = findChessboardCorners(camera1image2, boardSize, camera1ImagePoints[1]);
    if (!found1_2)
    {
        std::cerr << "Checkboard1_2 corners not found!" << std::endl;
        exit(-1);
    }


    std::vector<std::vector<Point2f> > camera2ImagePoints(2);
    bool found2_1 = findChessboardCorners(camera2image1, boardSize, camera2ImagePoints[0]);
    if (!found2_1)
    {
        std::cerr << "Checkboard2_1 corners not found!" << std::endl;
        exit(-1);
    }
    bool found2_2 = findChessboardCorners(camera2image2, boardSize, camera2ImagePoints[1]);
    if (!found2_2)
    {
        std::cerr << "Checkboard2_2 corners not found!" << std::endl;
        exit(-1);
    }

    cvtColor(camera1image1, camera1image1, COLOR_GRAY2BGR);
    cvtColor(camera1image2, camera1image2, COLOR_GRAY2BGR);
    cvtColor(camera2image1, camera2image1, COLOR_GRAY2BGR);
    cvtColor(camera2image2, camera2image2, COLOR_GRAY2BGR);

    drawChessboardCorners(camera1image1, boardSize, camera1ImagePoints[0], found1_1);
    drawChessboardCorners(camera1image2, boardSize, camera1ImagePoints[1], found1_2); 
    drawChessboardCorners(camera2image1, boardSize, camera2ImagePoints[0], found2_1);
    drawChessboardCorners(camera2image2, boardSize, camera2ImagePoints[1], found2_2);

    imshow("camera1image1", camera1image1);
    imshow("camera1image2", camera1image2);
    imshow("camera2image1", camera2image1);
    imshow("camera2image2", camera2image2);
    waitKey(0);
    destroyAllWindows();


    std::vector<std::vector<Point3f> > objectPoints(2);
    objectPoints[0] = Create3DChessboardCorners(boardSize, squareSize, -0.1, 1.5, 0);
    objectPoints[1] = Create3DChessboardCorners(boardSize, squareSize, - 0.1, 1.5, 0);

    Mat cameraMatrix[2];
    cameraMatrix[0] = Mat::eye(3, 3, CV_64F);
    cameraMatrix[1] = Mat::eye(3, 3, CV_64F);

    Mat distortionCoefficients[2];
    Mat rotationMatrix;
    Mat translationVector;
    Mat essentialMatrix;
    Mat fundamentalMatrix;

    double rms = stereoCalibrate(objectPoints, camera1ImagePoints, camera2ImagePoints,
        cameraMatrix[0], distortionCoefficients[0],
        cameraMatrix[1], distortionCoefficients[1],
        imageSize, rotationMatrix, translationVector, essentialMatrix, fundamentalMatrix,
        CV_CALIB_FIX_ASPECT_RATIO +
        CV_CALIB_ZERO_TANGENT_DIST +
        CV_CALIB_SAME_FOCAL_LENGTH +
        CV_CALIB_RATIONAL_MODEL +
        CV_CALIB_FIX_K3 + CV_CALIB_FIX_K4 + CV_CALIB_FIX_K5,
        TermCriteria(CV_TERMCRIT_ITER + CV_TERMCRIT_EPS, 100, 0.00005)
        );

    std::cout << "rotationMatrix: " << rotationMatrix << std::endl;
    std::cout << "translationVector: " << translationVector << std::endl;
    std::cout << "essentialMatrix: " << essentialMatrix << std::endl;
    std::cout << "fundamentalMatrix: " << fundamentalMatrix << std::endl;


    namedWindow("Ende");
    waitKey();
    destroyAllWindows();

    return 0;
}


