#include <opencv2\opencv.hpp>
#include <math.h>
#include "ImageDisplay.hpp"
#include <string>

cv::Mat gaussimage(int imwidth, int imheight, double centervalue, double strength, double fwhm = -1, double centerposition = -1)
{
    if (centerposition == -1) centerposition = imwidth / 2;
    if (fwhm == -1) fwhm = imwidth *0.7;

    cv::Mat image = cv::Mat::zeros(1, imwidth, CV_32FC1);

    double addition = centervalue - strength;

    float* ptr = image.ptr<float>(0);
    for (int i = 0; i < imwidth; i++)
        ptr[i] = strength * exp(-(4 * log(2) * pow(i - centerposition, 2)) / (fwhm * fwhm)) + addition;

    cv::resize(image, image, cv::Size(imwidth, imheight), 0, 0, cv::INTER_NEAREST);

    return image;
}


void shadingcorrection(cv::Mat & image, double centervalue, double strength, double fwhm = -1, double centerposition = -1)
{
    cv::Mat imageclone = image.clone();

    if (imageclone.channels() == 3)
        cv::cvtColor(imageclone, imageclone, cv::COLOR_BGR2GRAY);

    imageclone.convertTo(imageclone, CV_32FC1);

    cv::Mat correctionimage = gaussimage(imageclone.cols, imageclone.rows, centervalue, strength, fwhm, centerposition);

    cv::multiply(imageclone, correctionimage, imageclone);

    imageclone.convertTo(image, CV_8UC1);
}

void main()
{
    cv::Mat image1 = cv::imread("undistorted.png", cv::IMREAD_GRAYSCALE);


    std::string windowname = "Settings";
    cv::namedWindow(windowname);

    int centervalue = 200;
    int strength = 400;
    int threshOn = 0;
    int threshValue = 127;
    int fwhm = 1000;

    cv::createTrackbar("centervalue", windowname, &centervalue, 400);
    cv::createTrackbar("strength", windowname, &strength, 800);
    cv::createTrackbar("thresh On", windowname, &threshOn, 1);
    cv::createTrackbar("thresh Value", windowname, &threshValue, 255);
    cv::createTrackbar("fwhm", windowname, &fwhm, 2000);



    cv::Mat imageclone;

    while (true)
    {
        imageclone = image1.clone();
        shadingcorrection(imageclone, centervalue / 100., -strength/100., fwhm);

        if (threshOn)
            cv::threshold(imageclone, imageclone, threshValue, 255, cv::THRESH_BINARY);


        cv::imshow("image", imageclone);

        if (cv::waitKey(10) == 27)
            break;
    }

    cv::destroyAllWindows();
}