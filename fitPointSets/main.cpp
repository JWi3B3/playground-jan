
#include <iostream>
#include <vector>
#include <string>
#include "opencv2\opencv.hpp"


struct WeightPoint3d
{
    cv::Point3d point;
    double      weight;
    int         id;
};

std::vector<WeightPoint3d> createTestData1()
{
    std::vector<WeightPoint3d> points1;

    WeightPoint3d point;
    point = { cv::Point3d(0.79859, 2.43626, 0), 10, 0 };
    points1.push_back(point);

    point = { cv::Point3d(2.14227, 3.8883, 0), 10, 1 };
    points1.push_back(point);

    point = { cv::Point3d(3.78935, 4.83465, 0), 100, 2 };
    points1.push_back(point);

    point = { cv::Point3d(7.24245, 4.8491, 0), 100, 3 };
    points1.push_back(point);

    point = { cv::Point3d(8.92566, 2.45921  , 0), 10, 4 };
    points1.push_back(point);

    point = { cv::Point3d(10.84966, 2.32068, 0), 10, 5 };
    points1.push_back(point);

    point = { cv::Point3d(11.46852, 0.74343, 0), 10, 6 };
    points1.push_back(point);

    return points1;
}

std::vector<WeightPoint3d> createTestData2()
{
    std::vector<WeightPoint3d> points2;
    WeightPoint3d point;

    point = { cv::Point3d(8.75487, 5.1473, 0), 10, 0 };
    points2.push_back(point);

    point = { cv::Point3d(10.41271, 6.08315, 0), 10, 1 };
    points2.push_back(point);

    point = { cv::Point3d(12.37718, 6.65698, 0), 10, 2 };
    points2.push_back(point);

    point = { cv::Point3d(15.54461, 5.70304, 0), 10, 3 };
    points2.push_back(point);

    point = { cv::Point3d(16.56196, 3.0721, 0), 10, 4 };
    points2.push_back(point);

    point = { cv::Point3d(18.26883, 2.22227, 0), 10, 5 };
    points2.push_back(point);

    point = { cv::Point3d(18.51112, 0.57166, 0), 10, 6 };
    points2.push_back(point);

    // create ID that is not in points1
    point = { cv::Point3d(23., 11., 0), 10, 8 };
    points2.push_back(point);

    return points2;
}


cv::Mat drawDisplayImage(std::vector<cv::Point2d> points1, std::vector<cv::Point2d> points2, cv::Mat transformMatrix)
{

    int width = 800, height = 600;
    cv::Mat image = cv::Mat::zeros(height, width, CV_8UC3);

    double x, y;

    // Transform points2
    cv::transform(points2, points2, transformMatrix);

    // Get min and max coords
    double  minX = 1000000., maxX = -1000000.,
            minY = 1000000., maxY = -1000000.;

    for (int i = 0; i < points1.size(); i++)
    {
        if (points1[i].x < minX)  minX = points1[i].x;
        if (points2[i].x < minX)  minX = points2[i].x;

        if (points1[i].y < minY)  minY = points1[i].y;
        if (points2[i].y < minY)  minY = points2[i].y;

        if (points1[i].x > maxX)  maxX = points1[i].x;
        if (points2[i].x > maxX)  maxX = points2[i].x;

        if (points1[i].y > maxY)  maxY = points1[i].y;
        if (points2[i].y > maxY)  maxY = points2[i].y;
    }

    double scalefactor = 1;
    if ((maxX - minX) / width > (maxY - minY) / height)
        scalefactor = width / (maxX - minX);
    else
        scalefactor = height / (maxY - minY);

    

    // Transform points for display
    for (int i = 0; i < points1.size(); i++)
    {
        points1[i] -= cv::Point2d(minX, minY);
        points2[i] -= cv::Point2d(minX, minY);

        points1[i] *= scalefactor;
        points2[i] *= scalefactor;
    }

    

    // draw Lines
    for (int i = 1; i < points1.size(); i++)
    {
        cv::line(image, points1[i - 1], points1[i], cv::Scalar(190, 0, 190));
        cv::line(image, points2[i - 1], points2[i], cv::Scalar(0, 190, 0));
    }

    // Draw Connections
    for (int i = 0; i < points1.size(); i++)
    {
        cv::line(image, points1[i], points2[i], cv::Scalar(100, 100, 100));
    }

    // Draw Points
    for (int i = 0; i < points1.size(); i++)
    {
        cv::circle(image, points1[i], 5, cv::Scalar(255, 0, 255));
        cv::circle(image, points2[i], 5, cv::Scalar(0, 255, 0));
    }

   

    return image;
}


cv::Mat calcTransformationBetweenPointsets(std::vector<WeightPoint3d> points1, std::vector<WeightPoint3d> points2)
{
    std::vector<WeightPoint3d>  points1new, points2new;
    std::vector<cv::Point2d>    pts1, pts2, pts2transformed;
    cv::Point2d                 center1, center2;
    cv::Point2d                 translation;
    double                      angle;

    // Find same IDs in both pointsets
    for (int i = 0; i < points1.size(); i++)
    {
        for (int j = 0; j < points2.size(); j++)
        {
            if (points1[i].id == points2[j].id)
            {
                points1new.push_back(points1[i]);
                points2new.push_back(points2[j]);
            }
        }
    }

    // Convert points from 3D to 2D;
    cv::Point2d pt;
    for (int i = 0; i < points1new.size(); i++)
    {
        pt.x = points1new[i].point.x;
        pt.y = points1new[i].point.y;
        pts1.push_back(pt);

        pt.x = points2new[i].point.x;
        pt.y = points2new[i].point.y;
        pts2.push_back(pt);
    }

    // Calculate weight center of pointsets;
    double weight = 0;

    for (int i = 0; i < points1new.size(); i++)
    {
        center1 += pts1[i] * points1new[i].weight * points2new[i].weight;
        center2 += pts2[i] * points1new[i].weight * points2new[i].weight;

        weight += points1new[i].weight * points2new[i].weight;
    }

    center1 /= weight;
    center2 /= weight;

    translation = center1 - center2;

    cv::Mat transformMatrix ;
    double errorvalue = 0;

    // Calculate angle
    double currentangle = 0;
    double minError = 10000000.;
    double bestAngle = 0;
    double minangle = 0, maxangle = 360;

    int stepcount = 50;

    for (int i = 0; i < 3; i++)
    {
        for (int step = 0; step < stepcount; step++)
        {
            currentangle = minangle + step * ((maxangle - minangle) / stepcount);
            transformMatrix = cv::getRotationMatrix2D(center2, currentangle, 1.);
            transformMatrix.at<double>(0, 2) += translation.x;
            transformMatrix.at<double>(1, 2) += translation.y;

            // Transform points2
            cv::transform(pts2, pts2transformed, transformMatrix);

            // Calculate Error Value
            errorvalue = 0;
            for (int k = 0; k < pts1.size(); k++)
            {
                errorvalue += pow(cv::norm(pts1[k] - pts2transformed[k]), 2) * points1new[k].weight * points2new[k].weight;
            }

            if (errorvalue < minError)
            {
                minError = errorvalue;
                bestAngle = currentangle;
            }
        }

        minangle = bestAngle - 360 / pow(10, i+1);
        maxangle = bestAngle + 360 / pow(10, i+1);

        transformMatrix = cv::getRotationMatrix2D(center2, bestAngle, 1.);
        transformMatrix.at<double>(0, 2) += translation.x;
        transformMatrix.at<double>(1, 2) += translation.y;
        cv::imshow("Coords", drawDisplayImage(pts1, pts2, transformMatrix));
        cv::waitKey();

    }

    return transformMatrix;
}


void main()
{
    std::vector<WeightPoint3d> points1, points2;
    
    points1 = createTestData1();
    points2 = createTestData2();



    cv::Mat transformation = calcTransformationBetweenPointsets(points1, points2);

}

