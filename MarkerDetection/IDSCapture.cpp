#include <iostream>

#include "IDSCapture.hpp"

using namespace std;

IDSCapture::IDSCapture()
{
    int driver = is_GetDLLVersion();
    printf("Driver Version: %d.%02d.%04d\n", (driver >> 24), (driver >> 16 & 0xff), (driver & 0xffff));

    _initialize();
}


void IDSCapture::_initialize()
{
    int count;
    is_GetNumberOfCameras(&count);

    cout << "Number of cams connected: " << count << endl;

    for (int i = 0; i < count; i++)
    {
        HIDS cam = static_cast<HIDS>(0);
        int retVal = _open(&cam, nullptr);

        if (retVal == IS_SUCCESS)
        {
            is_FreeImageMem(cam, _imgMem, _memoryID);

            // Disable trigger Mode
            is_SetExternalTrigger(cam, IS_SET_TRIGGER_OFF);

            // Camera info
            CAMINFO camInfo;
            is_GetCameraInfo(cam, &camInfo);
            cout << "ID: " << static_cast<int>(camInfo.Select) << endl;
            cout << "S/N: " << camInfo.SerNo << endl;

            // Sensor info
            SENSORINFO senInfo;
            is_GetSensorInfo(cam, &senInfo);

            // Max img size
            _width = senInfo.nMaxWidth;
            _height = senInfo.nMaxHeight;
            
            is_SetColorMode(cam, IS_CM_MONO8);
            this->_bitsPerPixel = 8;


            // setup the color depth
            //is_GetColorDepth(cam, &_bitsPerPixel, &_colorMode);
            //is_SetColorMode(cam, _colorMode);

            // initialize memory
            is_AllocImageMem(cam, _width, _height, _bitsPerPixel, &_imgMem, &_memoryID);
            is_SetImageMem(cam, _imgMem, _memoryID); // set memory active
            
            // Add cam to vector
            _cams.push_back(cam);

            _cam = cam;
        }
    }

}


int IDSCapture::_open(HIDS* hCam, HWND hWnd)
{
    INT val = is_InitCamera(hCam, hWnd);

    if (val == IS_STARTER_FW_UPLOAD_NEEDED)
    {
        cout << "FW Upload needed" << endl;
    }

    return val;
}


cv::Mat IDSCapture::getFrame()
{
    is_FreezeVideo(_cam, IS_DONT_WAIT);

    return cv::Mat(_height, _width, CV_8UC1, _imgMem);
}

int IDSCapture::getCameraCount()
{
    return static_cast<int>(_cams.size());
}


void IDSCapture::_close()
{
    for (auto const& value : _cams)
    {
        is_FreeImageMem(value, _imgMem, _memoryID);
        is_ExitCamera(value);
    }
}


IDSCapture::~IDSCapture()
{
    _close();
}
