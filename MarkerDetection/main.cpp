#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include "IDSCapture.hpp"
#include "Helpers.h"
#include "MarkerDetection.h"
#include "Registration2d3d.h"

using namespace cv;
using namespace std;

void main()
{
    

    RNG rng(12345);

    vector<Scalar> colortable;
    for (int i = 0; i < 60; i++)
    {
        colortable.push_back(Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255)));
    }

    // 2D-Kamera initialisieren
    IDSCapture cam = IDSCapture();
    Mat frame, dispimage;
    MarkerDetection markerdetect;

    vector<vector<Point2f>> paths;
    vector<vector<Marker>> foundmarkers1_vec, foundmarkers2_vec;

    paths.resize(60);

    /*Mat allmarkers = imread("AllMarkers.png", IMREAD_GRAYSCALE);
    allmarkers /= 255;

    for (int y = 0; y < 5; y++)
    {
        for (int x = 0; x < 12; x++)
        {
            cout << "uchar data" << x + y*12 << "[6][6] = " << endl;

            auto roi = Rect(x * 8 + 1, y * 8 + 1, 6, 6);

            auto markerimage = allmarkers(roi);
            transpose(markerimage, markerimage);

            cout << "{" << endl;
            for (int marker_y = 0; marker_y < 6; marker_y++)
            {
                cout << "    {" ;

                for (int marker_x = 0; marker_x < 6; marker_x++)
                {
                    cout << to_string(markerimage.at<uchar>(marker_x, marker_y)) << ", ";
                }
                cout << "\b\b";
                cout << " }," << endl;
            }
            
            cout << "};" << endl;

            cout << "pattern = Mat(6, 6, CV_8UC1, data" << x + y * 12 << ");" << endl;
            cout << "teached_patterns.push_back(pattern.clone());" << endl << endl;

        }
    }*/



    //// Bilder laden
    //vector<Mat> images;
    //int imagecount = 420;
    //for (int i = 0; i < imagecount; i++)
    //{
    //    images.push_back(imread("scan_marker2/2d_0/" + to_string(i) + ".png", IMREAD_GRAYSCALE));
    //    cout << i << endl;
    //}

    int counter = 0;
    while (true)
    {
        frame = cam.getFrame();
        //frame = images[counter];
        Helpers::rotateimage90(frame, 3);

        resize(frame, frame, Size(), 0.8, 0.8, CV_INTER_AREA);
        
        auto foundmarkers1 = markerdetect.findMarkers(frame, 128, false);
        //auto foundmarkers2 = markerdetect.findMarkers(Helpers::scaleimage(frame, 0.6), 128, false);

        //foundmarkers1_vec.push_back(foundmarkers1);
        //foundmarkers2_vec.push_back(foundmarkers2);

        cvtColor(frame, dispimage, COLOR_GRAY2BGR);

        for (int i = 0; i < foundmarkers1.size(); i++)
        {
            if (paths[foundmarkers1[i].id].size() > 0)
            {
                if (abs(paths[foundmarkers1[i].id].back().x - foundmarkers1[i].xpos) > 10)
                    paths[foundmarkers1[i].id].push_back(Point2f(foundmarkers1[i].xpos, foundmarkers1[i].ypos));
            }
            else
            {
                paths[foundmarkers1[i].id].push_back(Point2f(foundmarkers1[i].xpos, foundmarkers1[i].ypos));
            }
        }

        for (int i = 0; i < paths.size(); i++)
        {
            for (int j = 1; j < paths[i].size(); j++)
            {
                line(dispimage, paths[i][j - 1], paths[i][j], colortable[i], 2);
            }
        }

        dispimage = markerdetect.drawMarkers(dispimage, foundmarkers1);

        imshow("Dispimage", dispimage);
        
        if (waitKey(1) == 27) break;

        //counter++;
        //if (counter >= imagecount)
        //    break;
    }


    Registration2d3d reg2d3d;
    reg2d3d.calcRegistration(foundmarkers1_vec, foundmarkers2_vec);

    while (true)
    {
        frame = cam.getFrame();
        Helpers::rotateimage90(frame, 3);

        resize(frame, frame, Size(), 0.8, 0.8, CV_INTER_AREA);

        imshow("Original", frame);
        imshow("3d warped", reg2d3d.warpDepthImage(Helpers::scaleimage(frame, 0.6), frame.size()));
        if (waitKey(1) == 27) break;
    }

    imwrite("dispimage.png", dispimage);
    destroyAllWindows();

    return;
}