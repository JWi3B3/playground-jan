#pragma once

#include <vector>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include "MarkerDetection.h"


class Registration2d3d
{
public:
    Registration2d3d();
    ~Registration2d3d();

    void calcRegistration(std::vector<std::vector<Marker>> markers2d, std::vector<std::vector<Marker>> markers3d);
    cv::Mat warpDepthImage(cv::Mat depthimage, cv::Size dstsize);

private:
    cv::Mat _warpmatrix;
};

