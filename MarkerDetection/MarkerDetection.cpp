#include <iostream>
#include "MarkerDetection.h"
#include "Helpers.h"

using namespace cv;
using namespace std;

MarkerDetection::MarkerDetection()
{
    Mat pattern;
    uchar data0[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data0);
    teached_patterns.push_back(pattern.clone());

    uchar data1[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data1);
    teached_patterns.push_back(pattern.clone());

    uchar data2[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data2);
    teached_patterns.push_back(pattern.clone());

    uchar data3[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data3);
    teached_patterns.push_back(pattern.clone());

    uchar data4[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data4);
    teached_patterns.push_back(pattern.clone());

    uchar data5[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data5);
    teached_patterns.push_back(pattern.clone());

    uchar data6[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data6);
    teached_patterns.push_back(pattern.clone());

    uchar data7[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data7);
    teached_patterns.push_back(pattern.clone());

    uchar data8[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data8);
    teached_patterns.push_back(pattern.clone());

    uchar data9[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data9);
    teached_patterns.push_back(pattern.clone());

    uchar data10[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data10);
    teached_patterns.push_back(pattern.clone());

    uchar data11[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data11);
    teached_patterns.push_back(pattern.clone());

    uchar data12[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data12);
    teached_patterns.push_back(pattern.clone());

    uchar data13[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data13);
    teached_patterns.push_back(pattern.clone());

    uchar data14[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data14);
    teached_patterns.push_back(pattern.clone());

    uchar data15[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data15);
    teached_patterns.push_back(pattern.clone());

    uchar data16[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data16);
    teached_patterns.push_back(pattern.clone());

    uchar data17[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data17);
    teached_patterns.push_back(pattern.clone());

    uchar data18[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data18);
    teached_patterns.push_back(pattern.clone());

    uchar data19[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data19);
    teached_patterns.push_back(pattern.clone());

    uchar data20[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data20);
    teached_patterns.push_back(pattern.clone());

    uchar data21[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data21);
    teached_patterns.push_back(pattern.clone());

    uchar data22[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data22);
    teached_patterns.push_back(pattern.clone());

    uchar data23[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data23);
    teached_patterns.push_back(pattern.clone());

    uchar data24[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data24);
    teached_patterns.push_back(pattern.clone());

    uchar data25[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data25);
    teached_patterns.push_back(pattern.clone());

    uchar data26[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data26);
    teached_patterns.push_back(pattern.clone());

    uchar data27[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data27);
    teached_patterns.push_back(pattern.clone());

    uchar data28[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data28);
    teached_patterns.push_back(pattern.clone());

    uchar data29[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data29);
    teached_patterns.push_back(pattern.clone());

    uchar data30[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data30);
    teached_patterns.push_back(pattern.clone());

    uchar data31[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data31);
    teached_patterns.push_back(pattern.clone());

    uchar data32[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data32);
    teached_patterns.push_back(pattern.clone());

    uchar data33[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data33);
    teached_patterns.push_back(pattern.clone());

    uchar data34[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data34);
    teached_patterns.push_back(pattern.clone());

    uchar data35[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data35);
    teached_patterns.push_back(pattern.clone());

    uchar data36[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data36);
    teached_patterns.push_back(pattern.clone());

    uchar data37[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data37);
    teached_patterns.push_back(pattern.clone());

    uchar data38[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data38);
    teached_patterns.push_back(pattern.clone());

    uchar data39[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data39);
    teached_patterns.push_back(pattern.clone());

    uchar data40[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data40);
    teached_patterns.push_back(pattern.clone());

    uchar data41[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data41);
    teached_patterns.push_back(pattern.clone());

    uchar data42[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data42);
    teached_patterns.push_back(pattern.clone());

    uchar data43[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data43);
    teached_patterns.push_back(pattern.clone());

    uchar data44[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data44);
    teached_patterns.push_back(pattern.clone());

    uchar data45[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data45);
    teached_patterns.push_back(pattern.clone());

    uchar data46[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data46);
    teached_patterns.push_back(pattern.clone());

    uchar data47[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data47);
    teached_patterns.push_back(pattern.clone());

    uchar data48[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data48);
    teached_patterns.push_back(pattern.clone());

    uchar data49[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data49);
    teached_patterns.push_back(pattern.clone());

    uchar data50[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data50);
    teached_patterns.push_back(pattern.clone());

    uchar data51[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data51);
    teached_patterns.push_back(pattern.clone());

    uchar data52[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data52);
    teached_patterns.push_back(pattern.clone());

    uchar data53[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data53);
    teached_patterns.push_back(pattern.clone());

    uchar data54[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data54);
    teached_patterns.push_back(pattern.clone());

    uchar data55[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data55);
    teached_patterns.push_back(pattern.clone());

    uchar data56[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data56);
    teached_patterns.push_back(pattern.clone());

    uchar data57[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data57);
    teached_patterns.push_back(pattern.clone());

    uchar data58[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data58);
    teached_patterns.push_back(pattern.clone());

    uchar data59[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data59);
    teached_patterns.push_back(pattern.clone());
}

MarkerDetection::~MarkerDetection(){}

vector<Marker> MarkerDetection::findMarkers(Mat grayimage, int thresh, bool debugmode)
{
    Mat dispimage;
    

    // Binarisieren
    Mat binimage;
    
    cv::threshold(grayimage, binimage, thresh, 255, CV_THRESH_BINARY);
    cvtColor(binimage, dispimage, CV_GRAY2BGR);

    // Konturerkennung
    vector<vector<Point>> contours;
    vector<Vec4i> hierarchy;
    binimage = 255 - binimage;
    findContours(binimage, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_TC89_KCOS);
    
    // Alle Konturen mit vier Ecken in einen Vektor schreiben
    vector<vector<Point>> newContours;

    for (int i = 0; i < contours.size(); i++)
    {
        vector<Point> newcurve;
        approxPolyDP(contours[i], newcurve, 6, true);
        contours[i] = newcurve;
        
        if (debugmode)
        {
            if (hierarchy[i][3] == -1)
            {
                if (contours[i].size() == 4)
                {
                    drawContours(dispimage, contours, i, Scalar(0, 150, 0), 2);
                    putText(dispimage, to_string(contours[i].size()), Point(contours[i][0].x, contours[i][0].y), CV_FONT_HERSHEY_PLAIN, 1, Scalar(255, 255, 255), 3);
                    putText(dispimage, to_string(contours[i].size()), Point(contours[i][0].x, contours[i][0].y), CV_FONT_HERSHEY_PLAIN, 1, Scalar(0, 150, 0));
                }
                else
                {
                    drawContours(dispimage, contours, i, Scalar(0, 0, 100), 2);
                    putText(dispimage, to_string(contours[i].size()), Point(contours[i][0].x, contours[i][0].y), CV_FONT_HERSHEY_PLAIN, 1, Scalar(255, 255, 255), 3);
                    putText(dispimage, to_string(contours[i].size()), Point(contours[i][0].x, contours[i][0].y), CV_FONT_HERSHEY_PLAIN, 1, Scalar(0, 0, 100));
                }
            }
        }

        if (contours[i].size() == 4 && hierarchy[i][3] == -1)
        {
            // Pr�fen ob eine der Ecken den Bildrand ber�hrt
            bool cornerHitsBorder = false;
            for (int k = 0; k < 4; k++)
            {
                //cout << contours[i][k].x << " " << contours[i][k].y << endl;
                if (contours[i][k].x < 5 || contours[i][k].x > binimage.cols - 5 
                    || contours[i][k].y < 5 || contours[i][k].y > binimage.rows - 5)
                {
                    cornerHitsBorder = true;
                    break;
                }
            }

            if (cornerHitsBorder == false)
                newContours.push_back(contours[i]);
        }
    }

    // Perspektivische Transformation
    Point2f inputQuad[4];
    Point2f outputQuad[4];
    int patternsize = 60;
    outputQuad[0] = Point2f(0, 0);
    outputQuad[1] = Point2f(0, patternsize);
    outputQuad[2] = Point2f(patternsize, patternsize);
    outputQuad[3] = Point2f(patternsize, 0);

    Mat matrix;
    Mat patternimage = Mat::zeros(patternsize, patternsize, CV_8UC1);

    vector<Mat> patternimages;
    for (int i = 0; i < newContours.size(); i++)
    {
        for (int k = 0; k < 4; k++)
            inputQuad[k] = Point2f(newContours[i][k].x, newContours[i][k].y);
        
        matrix = getPerspectiveTransform(inputQuad, outputQuad);
        warpPerspective(grayimage, patternimage, matrix, patternimage.size());
        threshold(patternimage, patternimage, thresh, 255, CV_THRESH_BINARY);
        patternimages.push_back(patternimage.clone());
    }

    
    // Hauptwert pro Feld bestimmen
    Mat minipatternimage = Mat::zeros(6, 6, CV_8UC1);
    Mat minipatternimage_bin;
    Mat dispimage1;
    vector<Mat> minipatternimages;
    vector<bool> bigerror;
    vector<double> errorvals;
    for (int i = 0; i < patternimages.size(); i++)
    {
        resize(patternimages[i], minipatternimage, minipatternimage.size(), 0, 0, INTER_LANCZOS4);
        cv::threshold(minipatternimage, minipatternimage_bin, thresh, 255, CV_THRESH_BINARY);

        double errorval = Helpers::differenceOfImages(minipatternimage, minipatternimage_bin);
        errorvals.push_back(errorval);

        if (errorval < 3000)
            bigerror.push_back(false);
        else
        {
            cout << errorval << endl;
            bigerror.push_back(true);
        }

        minipatternimages.push_back(minipatternimage_bin.clone());
    }

    // Mit allen Mustern vergleichen und ID bestimmen
    Mat rotimage;
    vector<int> id_vec;
    vector<int> correctIdx;
    vector<Marker> foundmarkers;
    bool found = false;
    for (int i = 0; i < minipatternimages.size(); i++)
    {
        if (bigerror[i] == true)
            continue;

        found = false;
        for (int j = 0; j < teached_patterns.size(); j++)
        {
            for (int angle = 0; angle < 4; angle++)
            {
                rotimage = minipatternimages[i].clone();
                Helpers::rotateimage90(rotimage, angle);

                if (Helpers::differenceOfImages(rotimage / 255, teached_patterns[j]) == 0)
                {
                    id_vec.push_back(j);
                    correctIdx.push_back(i);
                    found = true;

                    break;
                }
            }
            if (found == true) break;
        }
    }

    // X- und Y-Position durch Kreuzmittelpunkt ermitteln
    for (int i = 0; i < correctIdx.size(); i++)
    {
        double a1, a2, b1, b2, center_x, center_y;

        a1 = (1.*newContours[correctIdx[i]][0].y - newContours[correctIdx[i]][2].y)
            / (1.*newContours[correctIdx[i]][0].x - newContours[correctIdx[i]][2].x);
        a2 = (1.*newContours[correctIdx[i]][1].y - newContours[correctIdx[i]][3].y)
            / (1.*newContours[correctIdx[i]][1].x - newContours[correctIdx[i]][3].x);
        b1 = 1.0*newContours[correctIdx[i]][0].y - a1 * newContours[correctIdx[i]][0].x;
        b2 = 1.0*newContours[correctIdx[i]][1].y - a2 * newContours[correctIdx[i]][1].x;

        center_x = (b2 - b1) / (a1 - a2);
        center_y = a1 * center_x + b1;

        if (debugmode)
        {
            putText(dispimage, to_string(static_cast<int>(errorvals[correctIdx[i]])), Point(center_x, center_y+25), CV_FONT_HERSHEY_PLAIN, 1, Scalar(255, 255, 255), 3);
            putText(dispimage, to_string(static_cast<int>(errorvals[correctIdx[i]])), Point(center_x, center_y+25), CV_FONT_HERSHEY_PLAIN, 1, Scalar(255, 0, 0));
        }


        foundmarkers.push_back(Marker(id_vec[i], center_x, center_y));
    }

    dispimage = drawMarkers(dispimage, foundmarkers);

    if (debugmode)
    {
        cv::imshow("DebugMode", dispimage);
        waitKey(1);
    }

    return foundmarkers;
}

cv::Mat MarkerDetection::drawMarkers(cv::Mat& image, std::vector<Marker>& markers)
{
    cv::Mat dispimage;
    if (image.channels() == 1)
        cvtColor(image, dispimage, COLOR_GRAY2BGR);
    else
        dispimage = image.clone();

    for (int i = 0; i < markers.size(); i++)
    {
        circle(dispimage, Point(markers[i].xpos, markers[i].ypos), 5, Scalar(255, 0, 0), 2);
        putText(dispimage, to_string(markers[i].id), Point(markers[i].xpos + 10, markers[i].ypos + 5), CV_FONT_HERSHEY_PLAIN, 1, Scalar(255, 255, 255), 3);
        putText(dispimage, to_string(markers[i].id), Point(markers[i].xpos + 10, markers[i].ypos + 5), CV_FONT_HERSHEY_PLAIN, 1, Scalar(255, 0, 0));

    }

    return dispimage;
}


Marker::Marker(): xpos(-1), ypos(-1), id(-1){}
Marker::Marker(int markerID, double xPos, double yPos) : xpos(xPos), ypos(yPos), id(markerID) {}
Marker::~Marker(){}
