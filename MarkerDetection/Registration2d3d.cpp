#include "Registration2d3d.h"
#include <opencv2/calib3d/calib3d.hpp>

using namespace std;
using namespace cv;

Registration2d3d::Registration2d3d()
{
    _warpmatrix = Mat::eye(Size(3, 3), CV_32F);
}


Registration2d3d::~Registration2d3d()
{
}

void Registration2d3d::calcRegistration(std::vector<std::vector<Marker>> markers2d, std::vector<std::vector<Marker>> markers3d)
{
    vector<Point2f> inputCoords;
    vector<Point2f> outputCoords;

    for (int i = 0; i < markers2d.size(); i++)
    {
        for (int j = 0; j < markers2d[i].size(); j++)
        {
            for (int k = 0; k < markers3d[i].size(); k++)
            {
                if (markers3d[i][k].id == markers2d[i][j].id)
                {
                    inputCoords.push_back(Point2f(markers3d[i][k].xpos, markers3d[i][k].ypos));
                    outputCoords.push_back(Point2f(markers2d[i][j].xpos, markers2d[i][j].ypos));
                }
            }
        }
    }

    _warpmatrix = cv::findHomography(inputCoords, outputCoords);
}

cv::Mat Registration2d3d::warpDepthImage(cv::Mat depthimage, Size dstsize)
{
    Mat warpedimage;
    warpPerspective(depthimage, warpedimage, _warpmatrix, dstsize);
    return warpedimage;
}
