#pragma once

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <vector>

class Marker
{
public:
    Marker();
    Marker(int id, double xpos, double ypos);
    ~Marker();

    double xpos, ypos;
    int id;
};

class MarkerDetection
{
public:
    MarkerDetection();
    ~MarkerDetection();

    std::vector<Marker> findMarkers(cv::Mat grayimage, int thresh = 128, bool debugmode=false);
    cv::Mat drawMarkers(cv::Mat& image, std::vector<Marker>& markers);

private:
    std::vector<cv::Mat> teached_patterns;
};


