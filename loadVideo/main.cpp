#include <string>
#include <stdio.h>
#include <vector>
#include <opencv2\opencv.hpp>


std::vector<cv::Mat> loadVideo(std::string filename)
{
    cv::VideoCapture cap(filename);
    if (!cap.isOpened())
        std::cout << "ERROR: loadVideo: can't find the file " + filename << std::endl;

    std::vector<cv::Mat> video;
    cv::Mat frame;
    while (true)
    {
        if (cap.read(frame))
            video.push_back(frame.clone());
        else
            break;
    }

    return video;
}

void main()
{

    auto frames = loadVideo("video.avi");

    for (int i = 0; i < frames.size(); i++)
    {
        cv::imshow("frame", frames[i]);
        cv::waitKey(10);
    }
    
    cv::destroyAllWindows();
}