#include "ImageDisplay.hpp"
#include <iostream>
#include <string>

void show(cv::Mat image, std::string title)
{
    ImageDisplay imdisp;
    imdisp.run(image, title);
}


ImageDisplay::ImageDisplay() :
    _viewX(0),
    _viewY(0),
    _zoom(1.0),
    _mouseX(0),
    _mouseY(0),
    _isRMouseDown(false),
    _isLMouseDown(false),
    _heightstep(17),
    _nameborder(10),
    _valueborder(140),
    _colorHeader(cv::Scalar(255, 150, 0)),
    _colorName(cv::Scalar(170, 170, 170)),
    _colorValue(cv::Scalar(255, 255, 255))
{
    int maxcount = 30;
    for (int i = 0; i < maxcount; i++)
    {
        _zoomfactors.push_back(std::pow(0.9, maxcount - i));
    }

    _zoomfactors.push_back(1.0);

    for (int i = 1; i < maxcount; i++)
    {
        _zoomfactors.push_back(std::pow(1.1, i));
    }

    _zoom = maxcount;
}


void ImageDisplay::run(cv::Mat image, std::string title)
{

    cv::namedWindow(title);
    std::string titleInfo = title + " -Info-";
    cv::namedWindow(titleInfo);

    int autorangeOn = 0;
    int autorangeOnPrev = 0;
    int minInput = 0;
    int minInputPrev = 0;
    int maxInput = 100;
    int maxInputPrev = 0;
    int zoomPrev = _zoom;

    cv::createTrackbar("AutoRange", titleInfo, &autorangeOn, 1);
    cv::createTrackbar("Min", titleInfo, &minInput, 100);
    cv::createTrackbar("Max", titleInfo, &maxInput, 100);

    auto infoimage = _createInfoImage(image);

    cv::setMouseCallback(title, ImageDisplay::_mouseCallback, this);

    if (image.channels() == 2)
    {
        cv::Mat channels[2];
        cv::split(image, channels);
        std::vector<cv::Mat> channelvec;
        channelvec.push_back(channels[0]);
        channelvec.push_back(channels[0]);
        channelvec.push_back(channels[1]);

        cv::merge(channelvec, image);
    }

    cv::Mat dispimg, zoomimage;
    _autoRange(image, dispimg, autorangeOn, minInput, maxInput);

    int mouseXPrev = _mouseX, mouseYPrev = _mouseY;
    int absX = 0, absY = 0;

    // Calculate window size
    auto viewwidth = (std::min)(image.cols, IMAGEDISPLAY_WINDOWWIDTH);
    auto viewheight = (std::min)(image.rows, IMAGEDISPLAY_WINDOWHEIGHT);

    while (true)
    {
        if (_isRMouseDown)
        {
            _viewX = _viewX + (mouseXPrev - _mouseX) / _zoomfactors[_zoom];
            _viewY = _viewY + (mouseYPrev - _mouseY) / _zoomfactors[_zoom];
        }

        if (_isLMouseDown || zoomPrev != _zoom)
        {
            if (_isLMouseDown)
            {
                int xViewInt = (std::max)((std::min)(image.cols - _roiWidth, static_cast<int>(_viewX)), 0);
                int yViewInt = (std::max)((std::min)(image.rows - _roiHeight, static_cast<int>(_viewY)), 0);

                absX = static_cast<int>(_mouseX / _zoomfactors[_zoom] + xViewInt);
                absY = static_cast<int>(_mouseY / _zoomfactors[_zoom] + yViewInt);

                absX = (std::max)((std::min)(image.cols - 1, absX), 0);
                absY = (std::max)((std::min)(image.rows - 1, absY), 0);
            }

            infoimage = _updateInfoImage(infoimage, absX, absY, _getPixelValue(image, absX, absY));

            zoomPrev = _zoom;
        }

        if (autorangeOnPrev != autorangeOn || minInputPrev != minInput || maxInputPrev != maxInput)
        {
            autorangeOnPrev = autorangeOn;
            minInputPrev = minInput;
            maxInputPrev = maxInput;

            _autoRange(image, dispimg, autorangeOn, minInput, maxInput);
        }

        zoomimage = _resize(dispimg);

        cv::imshow(title, zoomimage);
        cv::imshow(titleInfo, infoimage);
        mouseXPrev = _mouseX;
        mouseYPrev = _mouseY;

        if (cv::waitKey(10) == 27)
            break;
    }

    cv::destroyWindow(title);
    cv::destroyWindow(titleInfo);
}

void ImageDisplay::show(cv::Mat image, std::string title)
{
    ImageDisplay imdisp;
    imdisp.run(image, title);
}


ImageDisplay::~ImageDisplay()
{
}

cv::Mat ImageDisplay::_resize(cv::Mat image)
{
    _roiWidth = (std::min)(image.cols, static_cast<int>(IMAGEDISPLAY_WINDOWWIDTH / _zoomfactors[_zoom]));
    _roiHeight = (std::min)(image.rows, static_cast<int>(IMAGEDISPLAY_WINDOWHEIGHT / _zoomfactors[_zoom]));

    int xViewInt = (std::max)((std::min)(image.cols - _roiWidth + 1, static_cast<int>(_viewX)), -1);
    int yViewInt = (std::max)((std::min)(image.rows - _roiHeight + 1, static_cast<int>(_viewY)), -1);

    if (xViewInt == -1)
        _viewX = 0;
    if (yViewInt == -1)
        _viewY = 0;
    if (xViewInt == image.cols - _roiWidth + 1)
        _viewX = image.cols - _roiWidth;
    if (yViewInt == image.rows - _roiHeight + 1)
        _viewY = image.rows - _roiHeight;

    xViewInt = (std::max)((std::min)(image.cols - _roiWidth, static_cast<int>(_viewX)), 0);
    yViewInt = (std::max)((std::min)(image.rows - _roiHeight, static_cast<int>(_viewY)), 0);

    cv::Rect roi = cv::Rect(xViewInt, yViewInt, _roiWidth, _roiHeight);
    cv::Mat newimage = image(roi).clone();

    if (_zoomfactors[_zoom] > 1)
        cv::resize(newimage, newimage, cv::Size(), _zoomfactors[_zoom], _zoomfactors[_zoom], CV_INTER_NN);
    else if (_zoomfactors[_zoom] < 1)
        cv::resize(newimage, newimage, cv::Size(), _zoomfactors[_zoom], _zoomfactors[_zoom], CV_INTER_CUBIC);

    return newimage;
}

void ImageDisplay::_mouseCallback(int event, int x, int y, int flags, void* userdata)
{
    auto *self = static_cast<ImageDisplay*>(userdata);
    self->_doMouseCallback(event, x, y, flags);
}

void ImageDisplay::_doMouseCallback(int event, int x, int y, int flags)
{
    if (event == cv::EVENT_LBUTTONDOWN)
        _isLMouseDown = true;

    else if (event == cv::EVENT_LBUTTONUP)
        _isLMouseDown = false;

    else if (event == cv::EVENT_RBUTTONDOWN)
        _isRMouseDown = true;

    else if (event == cv::EVENT_RBUTTONUP)
        _isRMouseDown = false;

    else if (event == cv::EVENT_MOUSEMOVE)
    {
        _mouseX = x;
        _mouseY = y;
    }

    else if (event == cv::EVENT_MOUSEWHEEL)
    {
        int zoomPrev = _zoom;
        _zoom += static_cast<int>(cv::getMouseWheelDelta(flags) / 120);

        if (_zoom < 0)
            _zoom = 0;
        else if (_zoom > _zoomfactors.size() - 1)
            _zoom = static_cast<int>(_zoomfactors.size()) - 1;
    }
}

cv::Mat ImageDisplay::_createInfoImage(cv::Mat originalimage)
{
    // Get image statistics
    int width = originalimage.cols;
    int height = originalimage.rows;
    int channels = originalimage.channels();
    int depth = originalimage.type();
    double minval = 0, maxval = 0;
    cv::minMaxLoc(originalimage, &minval, &maxval);

    // Create empty imag
    cv::Mat infoimage = cv::Mat::zeros(450, 300, CV_8UC3);

    // Write statistics
    cv::putText(infoimage, "Image Info", cv::Point(9, 2 * _heightstep), cv::FONT_HERSHEY_DUPLEX, 1, _colorHeader, 1);

    cv::putText(infoimage, "Width, Height:", cv::Point(_nameborder, 4 * _heightstep), cv::FONT_HERSHEY_PLAIN, 1, _colorName, 1);
    cv::putText(infoimage, std::to_string(width) + ", " + std::to_string(height), cv::Point(_valueborder, 4 * _heightstep), cv::FONT_HERSHEY_PLAIN, 1, _colorValue, 1);

    cv::putText(infoimage, "Channels:", cv::Point(10, 5 * _heightstep), cv::FONT_HERSHEY_PLAIN, 1, _colorName, 1);
    cv::putText(infoimage, std::to_string(channels), cv::Point(_valueborder, 5 * _heightstep), cv::FONT_HERSHEY_PLAIN, 1, _colorValue, 1);

    cv::putText(infoimage, "Type:", cv::Point(_nameborder, 6 * _heightstep), cv::FONT_HERSHEY_PLAIN, 1, _colorName, 1);
    cv::putText(infoimage, _getImageType(depth), cv::Point(_valueborder, 6 * _heightstep), cv::FONT_HERSHEY_PLAIN, 1, cv::Scalar(255, 255, 255), 1);

    cv::putText(infoimage, "Minimum:", cv::Point(_nameborder, 8 * _heightstep), cv::FONT_HERSHEY_PLAIN, 1, _colorName, 1);
    cv::putText(infoimage, std::to_string(minval), cv::Point(_valueborder, 8 * _heightstep), cv::FONT_HERSHEY_PLAIN, 1, _colorValue, 1);

    cv::putText(infoimage, "Maximum:", cv::Point(_nameborder, 9 * _heightstep), cv::FONT_HERSHEY_PLAIN, 1, _colorName, 1);
    cv::putText(infoimage, std::to_string(maxval), cv::Point(_valueborder, 9 * _heightstep), cv::FONT_HERSHEY_PLAIN, 1, _colorValue, 1);

    _drawHistogram(infoimage, originalimage);

    return infoimage;
}

cv::Mat ImageDisplay::_updateInfoImage(cv::Mat infoimage, int absX, int absY, std::vector<float> pixelvalue)
{
    cv::rectangle(infoimage, cv::Rect(0, 10 * _heightstep + 4, 300, _heightstep*6), cv::Scalar(0,0,0),-1);

    cv::putText(infoimage, "Pixel:", cv::Point(_nameborder, 11 * _heightstep), cv::FONT_HERSHEY_PLAIN, 1, _colorName, 1);
    cv::putText(infoimage, std::to_string(absX) + ", " + std::to_string(absY), cv::Point(_valueborder, 11 * _heightstep), cv::FONT_HERSHEY_PLAIN, 1, _colorValue, 1);



    cv::putText(infoimage, "Value:", cv::Point(_nameborder, 12 * _heightstep), cv::FONT_HERSHEY_PLAIN, 1, _colorName, 1);
    for (int i = 0; i < pixelvalue.size(); i++)
        cv::putText(infoimage, std::to_string(pixelvalue[i]), cv::Point(_valueborder, (12 + i) * _heightstep), cv::FONT_HERSHEY_PLAIN, 1, _colorValue, 1);

    cv::putText(infoimage, "Zoom:", cv::Point(_nameborder, 16 * _heightstep), cv::FONT_HERSHEY_PLAIN, 1, _colorName, 1);
    cv::putText(infoimage, std::to_string(_zoomfactors[_zoom]), cv::Point(_valueborder, 16 * _heightstep), cv::FONT_HERSHEY_PLAIN, 1, _colorValue, 1);

    return infoimage;
}


std::string ImageDisplay::_getImageType(int number)
{
    // find type
    int imgTypeInt = number % 8;
    std::string imgTypeString;

    switch (imgTypeInt)
    {
    case 0:
        imgTypeString = "8U";
        break;
    case 1:
        imgTypeString = "8S";
        break;
    case 2:
        imgTypeString = "16U";
        break;
    case 3:
        imgTypeString = "16S";
        break;
    case 4:
        imgTypeString = "32S";
        break;
    case 5:
        imgTypeString = "32F";
        break;
    case 6:
        imgTypeString = "64F";
        break;
    default:
        break;
    }

    // find channel
    int channel = (number / 8) + 1;

    std::stringstream type;
    type << "CV_" << imgTypeString << "C" << channel;

    return type.str();
}

void ImageDisplay::_drawHistogram(cv::Mat & infoimage, cv::Mat originalimage)
{
    int histHeight = 150;
    int histLeft = 10;
    int histBottom = 440;
    int histSize = 64;
    int linewidth = 4;

    cv::Mat histImage = cv::Mat::zeros(histHeight, histSize*linewidth, CV_8UC3);
    std::vector<cv::Mat> histImageVec;
    for (int i = 0; i < 3; i++)
        histImageVec.push_back(cv::Mat::zeros(histHeight, histSize*linewidth, CV_8UC3));

    // Create Colors for histogram
    std::vector<std::vector<cv::Scalar>> histColors;
    std::vector<cv::Scalar> onechannel, twochannel, threechannel;

    onechannel.push_back(cv::Scalar(255, 255, 255));
    histColors.push_back(onechannel);

    twochannel.push_back(cv::Scalar(255, 0, 0));
    twochannel.push_back(cv::Scalar(0, 255, 255));
    histColors.push_back(twochannel);

    threechannel.push_back(cv::Scalar(255, 0, 0));
    threechannel.push_back(cv::Scalar(0, 255, 0));
    threechannel.push_back(cv::Scalar(0, 0, 255));
    histColors.push_back(threechannel);


    std::vector<cv::Mat> channelVec;
    std::vector<cv::Mat> histVec;

    double minval = 0, maxval = 0;
    cv::minMaxLoc(originalimage, &minval, &maxval);


    float range[] = { static_cast<float>(minval), static_cast<float>(maxval) + 1};
    const float* histRange = { range };

    if (originalimage.channels() > 1)
    {
        cv::split(originalimage, channelVec);

    }
    else
    {
        channelVec.push_back(originalimage);
    }

    for (int i = 0; i < channelVec.size(); i++)
    {
        channelVec[i].convertTo(channelVec[i], CV_32F);
        histVec.push_back(cv::Mat());

        cv::calcHist(&channelVec[i], 1, 0, cv::Mat(), histVec[i], 1, &histSize, &histRange);
        normalize(histVec[i], histVec[i], 0, histHeight, cv::NORM_MINMAX, -1, cv::Mat());
    }


    for (int ch = 0; ch < channelVec.size(); ch++)
    {
        for (int k = 0; k < histSize; k++)
        {
            int xpos = k * linewidth;
            for (int j = 0; j < linewidth; j++)
            {
                cv::line(histImageVec[ch],
                    cv::Point(xpos+j, histHeight - 1),
                    cv::Point(xpos+j, static_cast<int>(histHeight - 1 - histVec[ch].at<float>(k))),
                    histColors[channelVec.size() - 1][ch], 1);
            }
        }

        cv::add(histImage, histImageVec[ch], histImage);
    }

    cv::Rect histarea = cv::Rect(histLeft, histBottom - histHeight, histSize*linewidth, histHeight);
    histImage.copyTo(infoimage(histarea));
}

std::vector<float> ImageDisplay::_getPixelValue(cv::Mat image, int x, int y)
{
    std::vector<float> pixelvalue;
    cv::Mat newimage;

    image.convertTo(newimage, CV_32F);

    if (image.channels() == 1)
    {
        pixelvalue.push_back(newimage.at<float>(y, x));
    }
    else if (image.channels() == 2)
    {
        pixelvalue.push_back(newimage.at<cv::Vec2f>(y, x).val[0]);
        pixelvalue.push_back(newimage.at<cv::Vec2f>(y, x).val[1]);
    }
    else if (image.channels() == 3)
    {
        pixelvalue.push_back(newimage.at<cv::Vec3f>(y, x).val[0]);
        pixelvalue.push_back(newimage.at<cv::Vec3f>(y, x).val[1]);
        pixelvalue.push_back(newimage.at<cv::Vec3f>(y, x).val[2]);
    }
    else
    {
        pixelvalue.push_back(-1);
    }

    return pixelvalue;
}

void ImageDisplay::_autoRange(cv::Mat& src, cv::Mat& dst, int rangeOn, int minpercent, int maxpercent)
{
    if (rangeOn == 1)
    {
        double minval, maxval;
        cv::minMaxIdx(src, &minval, &maxval);

        double minimum, maximum;
        minimum = minval + (minpercent/100.) * (maxval - minval);
        maximum = minval + (maxpercent/100.) * (maxval - minval);

        double a = 255. / (maximum - minimum);
        double b = -a * minimum;

        src.convertTo(dst, CV_8UC3, a, b);
    }
    else
    {
        dst = src.clone();
    }
}
