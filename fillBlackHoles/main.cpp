#include <opencv2\opencv.hpp>
#include <vector>
#include <iostream>
#include <string>
#include "ImageDisplay.hpp"

void fillBlackHoles(cv::Mat & coordImage, unsigned int filtersize)
{
    filtersize = filtersize * 2 + 1;

    cv::Mat original, result, mask, master, maskClone;
    cv::Mat coordChannels[2];
    cv::split(coordImage, coordChannels);

    cv::threshold(coordChannels[1], mask, 0., 1., CV_THRESH_BINARY);

    maskClone = mask.clone();
    maskClone = 1 - maskClone;
    cv::GaussianBlur(mask, mask, cv::Size(filtersize, filtersize), 0);

    for (int i = 0; i < 2; i++)
    {
        original = coordChannels[i].clone();
        cv::GaussianBlur(coordChannels[i], coordChannels[i], cv::Size(filtersize, filtersize), 0);
        cv::divide(coordChannels[i], mask, result);
        multiply(maskClone, result, master);
        add(master, original, coordChannels[i]);
    }

    cv::merge(coordChannels, 2, coordImage);
}

void main()
{
    cv::Mat image;
    cv::FileStorage storage("coordmap.yml", cv::FileStorage::READ);
    storage["img"] >> image;
    storage.release();

    cv::Mat channels[2];
    cv::split(image, channels);

    //ImageDisplay::show(channels[0]);
    //ImageDisplay::show(channels[1]);

    fillBlackHoles(image, 10);
    cv::split(image, channels);

    ImageDisplay::show(channels[0]);
    ImageDisplay::show(channels[1]);
}