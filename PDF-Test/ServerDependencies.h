#pragma once

enum Component {
    BONNET, ROOF, TRUNK_TOP, TRUNK_BOTTOM, ROOF_SPAR_LEFT,
    ROOF_SPAR_RIGHT, FENDER_FRONT_LEFT, FENDER_FRONT_RIGHT,
    DOOR_FRONT_LEFT, DOOR_FRONT_RIGHT,
    DOOR_REAR_LEFT, DOOR_REAR_RIGHT,
    FENDER_REAR_LEFT, FENDER_REAR_RIGHT
};


class Dent
{
public:
    unsigned getDiameter() const
    {
        return _diameter;
    }
    
    void setDiameter(int diameter)
    {
        _diameter = diameter;
    }

private:
    unsigned int _diameter = 0;
};