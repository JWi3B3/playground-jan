

#include <iostream>
#include <opencv2\opencv.hpp>
#include <libharu\hpdf.h>
#include <string>
#include "PDFReport.hpp"
#include "ServerDependencies.h"


std::pair<cv::Mat, std::vector<Dent>> createDents(cv::Mat image, int dentcount)
{
    int minsize = 10, maxsize = 50;

    std::vector<Dent> dents;
    for (int i = 0; i < dentcount; i++)
    {
        Dent dent;
        dent.setDiameter(rand() % (maxsize - minsize + 1) + minsize);
        dents.push_back(dent);
    }

    return std::make_pair(image, dents);
}

void main()
{
    PDFReport report;

    std::map<Component, std::pair<cv::Mat, std::vector<Dent>>> data;

    data[BONNET] = createDents(cv::imread("images2/bonnet.png"), 47);
    data[ROOF] = createDents(cv::imread("images2/roof.png"), 186);
    data[TRUNK_TOP] = createDents(cv::imread("images2/trunktop.png"), 15);
    data[TRUNK_BOTTOM] = createDents(cv::imread("images2/trunkbottom.png"), 11);
    data[ROOF_SPAR_LEFT] = createDents(cv::imread("images2/roofsparL.png"), 21);
    data[ROOF_SPAR_RIGHT] = createDents(cv::imread("images2/roofsparR.png"), 18);
    data[FENDER_FRONT_LEFT] = createDents(cv::imread("images2/fenderfrontL.png"), 10);
    data[FENDER_FRONT_RIGHT] = createDents(cv::imread("images2/fenderfrontR.png"), 12);
    
    //data[DOOR_FRONT_LEFT] = createDents(cv::Mat::zeros(100,100, CV_8UC1), 0);
    data[DOOR_FRONT_LEFT] = createDents(cv::imread("images2/doorfrontL.png"), 10);

    data[DOOR_FRONT_RIGHT] = createDents(cv::imread("images2/doorfrontR.png"), 17);
    data[DOOR_REAR_LEFT] = createDents(cv::imread("images2/doorrearL.png"), 13);
    data[DOOR_REAR_RIGHT] = createDents(cv::imread("images2/doorrearR.png"), 18);
    data[FENDER_REAR_LEFT] = createDents(cv::imread("images2/fenderrearL.png"), 10);
    data[FENDER_REAR_RIGHT] = createDents(cv::imread("images2/fenderrearR.png"), 12);

    report.createPDF("Report.pdf", "DA-RW 1985", "05.03.2016", data);

    std::cout << "PDF created: Report.pdf" << std::endl;

    //system("pause");
}
