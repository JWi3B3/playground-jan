#pragma once
#include <string>
#include <map>
#include "opencv2\opencv.hpp"
#include <libharu\hpdf.h>
#include "ServerDependencies.h"

class PDFReport
{
public:
    PDFReport();
    ~PDFReport();

    void createPDF(std::string filename, std::string licenseplate, std::string date, std::map<Component, std::pair<cv::Mat, std::vector<Dent>>> data);
    void createPDF(std::string date, std::map<Component, std::pair<cv::Mat, std::vector<Dent>>> result);



private:
    jmp_buf     _env;
    HPDF_Doc    _pdf;

    HPDF_Font   _fontNormal;
    HPDF_Font   _fontBold;
    HPDF_Font   _fontItalic;
    HPDF_Font   _fontLight;

    HPDF_Page   _page;
    char        _filename[256];
    HPDF_Destination dst;

    std::map<Component, std::pair<cv::Mat, std::vector<Dent>>> _data;
    std::map<Component, std::vector<int>> _stats;
    std::string _licenseplate;
    std::string _date;

    std::string _getComponentString(Component comp);
    void        _createStatistics();

    void        _drawImage(cv::Mat cvImage, float x, float y, float width = -1, float height = -1, float scalefactor = 1, bool blackTransparent = false, bool anchorRightBottom = false);
    void        _drawRectangle(double xpos, double ypos, double width, double height, double thickness, bool drawLine = true, bool drawFill = false, HPDF_RGBColor linecolor = { 0,0,0 }, HPDF_RGBColor fillcolor = { 0,0,0 });
    void        _drawText(std::string text, float x, float y, int fontsize, int style = 0, int angle = 0, HPDF_RGBColor color = {0,0,0}, bool aligncenter = false);
    void        _drawLine(float xstart, float ystart, float xend, float yend, float thickness = 1, float r = 0.0, float g = 0.0, float b = 0.0);
    void        _drawColibri(float xpos, float ypos, float size, bool mirror, float red, float green, float blue);

    void        _drawWaterMark();
    void        _drawHeader();
    void        _drawTable();
    void        _drawScan();
    void        _drawFooter();

    cv::Mat     _createErrorImage();

};

