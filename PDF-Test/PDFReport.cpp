﻿#include "PDFReport.hpp"
#include <iostream>
#include <vector>
#include <sys/stat.h>
#include <string>

void rotateRight(cv::Mat &frame)
{
    cv::transpose(frame, frame);
    cv::flip(frame, frame, 1);
}

static bool fileExists(const std::string& name)
{
    std::ifstream f(name.c_str());
    return f.good();
}

void errorHandler(HPDF_STATUS error_no, HPDF_STATUS detail_no, void *user_data)
{
    /* throw exception when an error has occured */
    std::cout << "PDF ERROR: error_no= " << static_cast<unsigned int>(error_no) << "detail_no=" << static_cast<int>(detail_no) << std::endl;
    throw std::exception();
}

PDFReport::PDFReport()
{
    _pdf = HPDF_New(errorHandler, NULL);
    if (!_pdf) {
        printf("error: cannot create PdfDoc object\n");
        return;
    }

    // Error Handling
    if (setjmp(_env)) {
        HPDF_Free(_pdf);
        return;
    }

    HPDF_SetCompressionMode(_pdf, HPDF_COMP_ALL);

    // load fonts
    if (!fileExists("calibri.ttf"))
        std::cout << "PDFReport ERROR: " << "calibri.ttf" << " not found!" << std::endl;
    if (!fileExists("calibrib.ttf"))
        std::cout << "PDFReport ERROR: " << "calibrib.ttf" << " not found!" << std::endl;
    if (!fileExists("calibrii.ttf"))
        std::cout << "PDFReport ERROR: " << "calibrii.ttf" << " not found!" << std::endl;
    if (!fileExists("calibril.ttf"))
        std::cout << "PDFReport ERROR: " << "calibril.ttf" << " not found!" << std::endl;

    auto fontNormal = HPDF_LoadTTFontFromFile(_pdf, "calibri.ttf", HPDF_TRUE);
    _fontNormal = HPDF_GetFont(_pdf, fontNormal, "ISO8859-10"); // Vorher ISO8859-2

    auto fontBold = HPDF_LoadTTFontFromFile(_pdf, "calibrib.ttf", HPDF_TRUE);
    _fontBold = HPDF_GetFont(_pdf, fontBold, "ISO8859-10");

    auto fontItalic = HPDF_LoadTTFontFromFile(_pdf, "calibrii.ttf", HPDF_TRUE);
    _fontItalic = HPDF_GetFont(_pdf, fontItalic, "ISO8859-10");

    auto fontLight = HPDF_LoadTTFontFromFile(_pdf, "calibril.ttf", HPDF_TRUE);
    _fontLight = HPDF_GetFont(_pdf, fontLight, "ISO8859-10");
}


PDFReport::~PDFReport()
{
}

void PDFReport::createPDF(std::string filename, std::string licenseplate, std::string date, std::map<Component, std::pair<cv::Mat, std::vector<Dent>>> data)
{
    _data = data;
    _licenseplate = licenseplate;
    _date = date;

    _createStatistics();

    strcpy(_filename, filename.c_str());

    //HPDF_GetEncoder(_pdf, "FontSpecific");

    // New Page
    _page = HPDF_AddPage(_pdf);

    // Set Size to DinA4
    HPDF_Page_SetSize(_page, HPDF_PAGE_SIZE_A4, HPDF_PAGE_PORTRAIT);

    dst = HPDF_Page_CreateDestination(_page);
    HPDF_Destination_SetXYZ(dst, 0, HPDF_Page_GetHeight(_page), 1);
    HPDF_SetOpenAction(_pdf, dst);

    // Draw all the stuff
    _drawWaterMark();
    _drawHeader();
    _drawTable();
    _drawScan();
    _drawFooter();


    // save the document to a file
    HPDF_SaveToFile(_pdf, _filename);

    // clean up
    HPDF_Free(_pdf);
}

void PDFReport::createPDF(std::string date, std::map<Component, std::pair<cv::Mat, std::vector<Dent>>> result)
{
    _data = result;
    _licenseplate = "DA RW 1985";
    _date = date;

    _createStatistics();

    std::string filename = "Report.pdf";
    strcpy(_filename, filename.c_str());


    // New Page
    _page = HPDF_AddPage(_pdf);

    // Set Size to DinA4
    HPDF_Page_SetSize(_page, HPDF_PAGE_SIZE_A4, HPDF_PAGE_PORTRAIT);

    dst = HPDF_Page_CreateDestination(_page);
    HPDF_Destination_SetXYZ(dst, 0, HPDF_Page_GetHeight(_page), 1);
    HPDF_SetOpenAction(_pdf, dst);

    // Draw all the stuff
    _drawWaterMark();
    _drawHeader();
    _drawTable();
    _drawScan();
    _drawFooter();

    // save the document to a file
    HPDF_SaveToFile(_pdf, _filename);

    // clean up
    HPDF_Free(_pdf);
}


std::string PDFReport::_getComponentString(Component comp)
{
    std::vector<std::string> componentstring;
    componentstring.push_back("Motorhaube");
    componentstring.push_back("Dach");
    componentstring.push_back("Heckklappe oben");
    componentstring.push_back("Heckklappe unten");
    componentstring.push_back("Dachholm");
    componentstring.push_back("Dachholm");
    componentstring.push_back("Kotflügel vorne");
    componentstring.push_back("Kotflügel vorne");
    componentstring.push_back("Tür vorne");
    componentstring.push_back("Tür vorne");
    componentstring.push_back("Tür hinten");
    componentstring.push_back("Tür hinten");
    componentstring.push_back("Kotflügel hinten");
    componentstring.push_back("Kotflügel hinten");

    return componentstring[comp];
}

void PDFReport::_createStatistics()
{
    _stats.clear();

    for (int i = 0; i < 14; i++)
    {
        std::vector<int> componentStats(11, 0);

        if (_data.find(static_cast<Component>(i)) == _data.end())
            componentStats[10] = 2; // 2- Keine Daten vorhanden
        else if (cv::sum(_data[static_cast<Component>(i)].first)[1] == 0)
            componentStats[10] = 1; // 1 - Kein Bild vorhanden
        
        else if (_data[static_cast<Component>(i)].second.size() == 0)
            componentStats[10] = 3; // 3 - Keine Dellen gefunden
        else
            componentStats[10] = 0; // 0 - Daten ok

        if (_data.find(static_cast<Component>(i)) != _data.end())
        {
            std::vector<Dent> dents = _data[static_cast<Component>(i)].second;
            for (int j = 0; j < dents.size(); j++)
            {
                int dentsize = (dents[j].getDiameter() + 5) / 10 * 10;
                componentStats[9] += dentsize;
                componentStats[8]++;

                int dentclass = dentsize / 10 - 1;
                if (dentclass < 0) dentclass = 0;
                if (dentclass > 7) dentclass = 7;
                componentStats[dentclass]++;
            }

            componentStats[9] = static_cast<int>(static_cast<float>(componentStats[9]) / static_cast<float>(componentStats[8]) + 0.5);
            _stats[static_cast<Component>(i)] = componentStats;
        }
        else
        {
            _stats[static_cast<Component>(i)] = componentStats;
        }
    }
}

void PDFReport::_drawImage(cv::Mat cvImage, float x, float y, float width, float height, float scalefactor, bool blackTransparent, bool anchorRightBottom)
{
    if (cvImage.empty())
        cvImage = _createErrorImage();

    cv::Mat cvImageCopy = cvImage.clone();
    if (cvImageCopy.channels() == 1)
        cv::cvtColor(cvImageCopy, cvImageCopy, cv::COLOR_GRAY2BGR);
    else if (cvImageCopy.channels() != 3)
    {
        std::cout << "ERROR: PDFReport::_drawImage: cvImage must have either 3 or 1 channel!" << std::endl;
        return;
    }

    float imwidth, imheight;

    HPDF_Page page = HPDF_GetCurrentPage(_pdf);
    HPDF_Image image;

    std::vector<int> compression_params;
    compression_params.push_back(CV_IMWRITE_JPEG_QUALITY);
    compression_params.push_back(98);
    std::vector<uchar> imgBuffer;

    cv::imencode(".jpg", cvImageCopy, imgBuffer, compression_params);
    image = HPDF_LoadJpegImageFromMem(_pdf, &imgBuffer[0], imgBuffer.size());

    if (blackTransparent)
        HPDF_Image_SetColorMask(image, 0, 30, 0, 30, 0, 30);

    if (width == -1 && height == -1)
    {
        imwidth = HPDF_Image_GetWidth(image) * scalefactor;
        imheight = HPDF_Image_GetHeight(image) * scalefactor;
    }
    else if (width == -1)
    {
        imwidth = height *  HPDF_Image_GetWidth(image) / HPDF_Image_GetHeight(image);
        imheight = height;
    }
    else if (height == -1)
    {
        imwidth = width;
        imheight = width * HPDF_Image_GetHeight(image) / HPDF_Image_GetWidth(image);
    }
    else
    {
        imwidth = width; imheight = height;
    }

    if (anchorRightBottom)
    {
        x -= imwidth;
    }

    HPDF_Page_DrawImage(page, image, x, y, imwidth, imheight);
}

void PDFReport::_drawRectangle(double xpos, double ypos, double width, double height, double thickness, bool drawLine, bool drawFill, HPDF_RGBColor linecolor, HPDF_RGBColor fillcolor)
{
    if (!drawLine &&  !drawFill)
        return;

    HPDF_Page page = HPDF_GetCurrentPage(_pdf);

    HPDF_Page_SetLineWidth(page, thickness);
    HPDF_Page_SetRGBStroke(page, linecolor.r, linecolor.g, linecolor.b);
    HPDF_Page_SetRGBFill(page, fillcolor.r, fillcolor.g, fillcolor.b);

    HPDF_Page_Rectangle(page, xpos, ypos, width, height);
    if (drawFill && drawLine)
        HPDF_Page_FillStroke(page);
    else if (drawLine)
        HPDF_Page_Stroke(page);
    else if (drawFill)
        HPDF_Page_Fill(page);
}

void PDFReport::_drawText(std::string text, float x, float y, int fontsize, int style, int angle, HPDF_RGBColor color, bool aligncenter)
{
    char text_c[256];
    strcpy(text_c, text.c_str());

    HPDF_Font font;
    if (style == 0)
        font = _fontNormal;
    else if (style == 1)
        font = _fontBold;
    else if (style == 2)
        font = _fontItalic;
    else if (style == 3)
        font = _fontLight;

    float radians = float(angle) / 180.f * 3.141592f;
    HPDF_Page_SetRGBFill(_page, color.r, color.g, color.b);
    HPDF_Page_BeginText(_page);

    HPDF_Page_SetFontAndSize(_page, font, static_cast<HPDF_REAL>(fontsize));
    HPDF_Page_SetTextMatrix(_page, cos(radians), sin(radians), -sin(radians), cos(radians), x, y);

    if (aligncenter)
    {
        float centerpos = HPDF_Page_TextWidth(_page, text_c) / 2;
        HPDF_Page_MoveTextPos(_page, -centerpos, 0);
    }
    
    HPDF_Page_ShowText(_page, text_c);
    HPDF_Page_EndText(_page);
}

void PDFReport::_drawLine(float xstart, float ystart, float xend, float yend, float thickness, float r, float g, float b)
{
    HPDF_Page_SetLineWidth(_page, thickness);
    HPDF_RGBColor prevcol = HPDF_Page_GetRGBStroke(_page);
    HPDF_Page_SetRGBStroke(_page, r, g, b);
    HPDF_Page_MoveTo(_page, xstart, ystart);
    HPDF_Page_LineTo(_page, xend, yend);
    HPDF_Page_Stroke(_page);
    HPDF_Page_SetRGBStroke(_page, prevcol.r, prevcol.g, prevcol.b);
}

void PDFReport::_drawWaterMark()
{
    _drawColibri(600, 300, 50, true, 0.97, 0.97, 0.97);
}

void PDFReport::_drawHeader()
{
    float left = 440;
    float top = 752;
    _drawText("Colibri Scanbericht", 20, top + 40, 28, 3);

    _drawText("Kennzeichen:", left, top - 15, 10, 2);
    _drawText(_licenseplate, left, top - 29, 12);

    _drawText("Datum:", left, top - 46, 10, 2);
    _drawText(_date, left, top - 60, 12);

    _drawText("Kunde:", left, top - 77, 10, 2);
    _drawText("Jan Wiebe", left, top - 91, 12);
}

void PDFReport::_drawTable()
{
    float thickness = 0.4f;

    float top = 763;
    float left = 33;

    const int rows = 17;
    const int cols = 10;

    float h = 14;
    float w = 26;

    int heightOffsetBold = 6;

    float rowheight[rows] = { 20, h, h, h, h, h, h, h, h, h, h, h, h, h, h, h, h};
    float colwidth[cols] = { 86, w, w, w, w, w, w, w, w, 80};

    float rowAccu[rows];
    float colAccu[cols];
    float rowsum = 0, colsum = 0;
    for (int i = 0; i < rows; i++)
    {
        rowsum += rowheight[i];
        rowAccu[i] = rowsum;
    }
    for (int i = 0; i < cols; i++)
    {
        colsum += colwidth[i];
        colAccu[i] = colsum;
    }

    float tablewidth = 0;
    for (auto& num : colwidth)
        tablewidth += num;

    float tableheight = 0;
    for (auto& num : rowheight)
        tableheight += num;

    float rowposition = 0;
    float colposition = 0;

    // Alle Linien zeichnen
    _drawLine(left, top - rowAccu[0], left, top - rowAccu[4], thickness, 0.6, 0.6, 0.6);
    _drawLine(left, top - rowAccu[5], left, top - rowAccu[10], thickness, 0.6, 0.6, 0.6);
    _drawLine(left, top - rowAccu[11], left, top - rowAccu[16], thickness, 0.6, 0.6, 0.6);

    for (int y = 0; y < rows; y++)
        _drawLine(left, top - rowAccu[y], left + tablewidth, top - rowAccu[y], thickness, 0.6, 0.6, 0.6);

    for (int x = 0; x < cols; x++)
    {
        _drawLine(left + colAccu[x], top - rowAccu[0], left + colAccu[x], top -rowAccu[4], thickness, 0.6, 0.6, 0.6);
        _drawLine(left + colAccu[x], top - rowAccu[5], left + colAccu[x], top - rowAccu[10], thickness, 0.6, 0.6, 0.6);
        _drawLine(left + colAccu[x], top - rowAccu[11], left + colAccu[x], top - rowAccu[16], thickness, 0.6, 0.6, 0.6);
    }

    int componentTablePos[14] = { 1, 2, 3, 4, 8, 14, 6, 12, 7, 13, 9, 15, 10, 16 };

    for (int i = 0; i < 8; i++)
    {
        _drawText(std::to_string((i + 1) * 10), left + colAccu[i] + 5, top - rowAccu[0] + heightOffsetBold, 10, 1);
    }

    _drawText("Gesamt", left + colAccu[8] + 5, top - rowAccu[0] + heightOffsetBold, 10, 1);

    _drawText("Horizontal", left - 6, top - rowAccu[4] + heightOffsetBold, 10, 1, 90);
    _drawText("Links", left - 6, top - rowAccu[10] + 20 + heightOffsetBold, 10, 1, 90);
    _drawText("Rechts", left - 6, top - rowAccu[15] + heightOffsetBold, 10, 1, 90);

    for (int i = 0; i < 14; i++)
    {
        _drawText(_getComponentString(static_cast<Component>(i)), left + 5, top - rowAccu[componentTablePos[i]] + 4, 10);

        auto componentStatistic = _stats[static_cast<Component>(i)];

        if (componentStatistic[10] == 0)
        {
            for (int j = 0; j < 8; j++)
            {
                if (componentStatistic[j] > 0)
                    _drawText(std::to_string(componentStatistic[j]), left + colAccu[j] + 5, top - rowAccu[componentTablePos[i]] + 4, 10);
            }

            _drawText(std::to_string(componentStatistic[8]), left + colAccu[8] + 5, top - rowAccu[componentTablePos[i]] + 4, 10);
            _drawText("Ø " + std::to_string(componentStatistic[9]) + " mm", left + colAccu[8] + 30, top - rowAccu[componentTablePos[i]] + 4, 10);

        }
        else if (componentStatistic[10] == 1)
            _drawText("    - Kein Bild -", left + colAccu[0] + 5, top - rowAccu[componentTablePos[i]] + 4, 10);
        else if (componentStatistic[10] == 2)
            _drawText("    - Keine Daten -", left + colAccu[0] + 5, top - rowAccu[componentTablePos[i]] + 4, 10);
        else if (componentStatistic[10] == 3)
            _drawText("    - Keine Dellen gefunden -", left + colAccu[0] + 5, top - rowAccu[componentTablePos[i]] + 4, 10);
    }
}

void PDFReport::_drawScan()
{
    float left = 20, top = 500;
    float gap = 8;

    float wSide = 100;
    float wMid = 140;
    float wSpar = 45;

    float hFront = 115;
    float hDoor = 97;
    float hBack1 = 55;
    float hBack2 = 45;

    float xDist[10] = { 0, wSide, gap, wSpar, gap, wMid, gap, wSpar, gap, wSide };
    float yDist[10] = { 0, hFront, gap, hDoor, gap, hDoor, gap, hBack1, gap, hBack2 };

    float Width = 2 * wSide + 4 * gap + 2 * wSpar + wMid;
    float Height = hFront + 4*gap + 2*hDoor + hBack1 + hBack2;

    float xPos[10];
    float yPos[10];
    float ySum = top, xSum = left;

    for (int i = 0; i < 10; i++)
    {
        ySum -= yDist[i];
        yPos[i] = ySum;
        xSum += xDist[i];
        xPos[i] = xSum;
    }

    float xposPart[14] = { xPos[4], xPos[4], xPos[4], xPos[4], xPos[2], xPos[6], xPos[0],
                            xPos[8], xPos[0], xPos[8], xPos[0], xPos[8], xPos[0], xPos[8] };

    float yposPart[14] = { yPos[1], yPos[5], yPos[7], yPos[9], yPos[5], yPos[5], yPos[1],
                           yPos[1], yPos[3], yPos[3], yPos[5], yPos[5], yPos[9], yPos[9] };
    
    float widthPart[14] = { xPos[5] - xPos[4], xPos[5] - xPos[4], xPos[5] - xPos[4],
        xPos[5] - xPos[4], xPos[3] - xPos[2], xPos[7] - xPos[6], xPos[1] - xPos[0],
        xPos[9] - xPos[8], xPos[1] - xPos[0], xPos[9] - xPos[8], xPos[1] - xPos[0],
        xPos[9] - xPos[8], xPos[1] - xPos[0], xPos[9] - xPos[8] };

    float heightPart[14] = { yPos[0] - yPos[1], yPos[2] - yPos[5], yPos[6] - yPos[7],
        yPos[8] - yPos[9], yPos[2] - yPos[5], yPos[2] - yPos[5], yPos[0] - yPos[1],
        yPos[0] - yPos[1], yPos[2] - yPos[3], yPos[2] - yPos[3], yPos[4] - yPos[5],
        yPos[4] - yPos[5], yPos[6] - yPos[9], yPos[6] - yPos[9] };

    for (int i = 0; i < 14; i++)
    {
        _drawRectangle(xposPart[i], yposPart[i], widthPart[i], heightPart[i],
            0.4f, true, true, HPDF_RGBColor{ 0.6f,0.6f,0.6f }, HPDF_RGBColor{ 0.96f,0.96f,0.96f });
        if (_stats[static_cast<Component>(i)][10] != 1 && _stats[static_cast<Component>(i)][10] != 2)
            _drawImage(_data[static_cast<Component>(i)].first, xposPart[i], yposPart[i], widthPart[i], heightPart[i], -1, true);

        _drawText(_getComponentString(static_cast<Component>(i)), xposPart[i] + widthPart[i]/2, yposPart[i] + 3, 8, 0, 0, HPDF_RGBColor{0,0,0.3f}, true);

    }
}

void PDFReport::_drawColibri(float xpos, float ypos, float size, bool mirror, float red, float green, float blue)
{
    // Create coordinates of colibri logo
    std::vector<cv::Point2f> coords;
    coords.push_back(cv::Point2f(2.2413, 0.85570));
    coords.push_back(cv::Point2f(3.41803, 0.0));
    coords.push_back(cv::Point2f(3.96369, 3.00138));
    coords.push_back(cv::Point2f(3.7546, 3.59539));
    coords.push_back(cv::Point2f(-1, -1));
    coords.push_back(cv::Point2f(3.5397, 0.04735));
    coords.push_back(cv::Point2f(5.50734, 1.73639));
    coords.push_back(cv::Point2f(4.49047, 3.38185));
    coords.push_back(cv::Point2f(4.07261, 2.97843));
    coords.push_back(cv::Point2f(-1, -1));
    coords.push_back(cv::Point2f(5.59018, 1.81404));
    coords.push_back(cv::Point2f(6.55325, 2.77117));
    coords.push_back(cv::Point2f(5.04491, 3.91715));
    coords.push_back(cv::Point2f(4.5724, 3.46095));
    coords.push_back(cv::Point2f(-1, -1));
    coords.push_back(cv::Point2f(6.59642, 2.87821));
    coords.push_back(cv::Point2f(6.78824, 4.19276));
    coords.push_back(cv::Point2f(5.57143, 4.42552));
    coords.push_back(cv::Point2f(5.12594, 3.9954));
    coords.push_back(cv::Point2f(-1, -1));
    coords.push_back(cv::Point2f(2.5176, 1.58603));
    coords.push_back(cv::Point2f(3.70512, 3.73593));
    coords.push_back(cv::Point2f(3.53611, 4.21612));
    coords.push_back(cv::Point2f(-1, -1));
    coords.push_back(cv::Point2f(4.04479, 3.10625));
    coords.push_back(cv::Point2f(5.40757, 4.42198));
    coords.push_back(cv::Point2f(3.61574, 4.32515));
    coords.push_back(cv::Point2f(-1, -1));
    coords.push_back(cv::Point2f(3.62104, 4.43687));
    coords.push_back(cv::Point2f(5.53107, 4.54011));
    coords.push_back(cv::Point2f(6.89856, 5.75712));
    coords.push_back(cv::Point2f(5.57909, 7.01498));
    coords.push_back(cv::Point2f(4.15511, 5.7229));
    coords.push_back(cv::Point2f(-1, -1));
    coords.push_back(cv::Point2f(6.9958, 5.81823));
    coords.push_back(cv::Point2f(8.12555, 6.15271));
    coords.push_back(cv::Point2f(5.8034, 6.95496));
    coords.push_back(cv::Point2f(-1, -1));
    coords.push_back(cv::Point2f(10.24875, 5.43184));
    coords.push_back(cv::Point2f(9.00876, 6.27718));
    coords.push_back(cv::Point2f(8.46266, 6.1317));
    coords.push_back(cv::Point2f(-1, -1));
    coords.push_back(cv::Point2f(8.31087, 6.20645));
    coords.push_back(cv::Point2f(8.93924, 6.37385));
    coords.push_back(cv::Point2f(8.30852, 7.66112));
    coords.push_back(cv::Point2f(7.39744, 8.06664));
    coords.push_back(cv::Point2f(5.71407, 7.10354));
    coords.push_back(cv::Point2f(-1, -1));
    coords.push_back(cv::Point2f(5.6627, 7.20243));
    coords.push_back(cv::Point2f(6.69948, 7.79559));
    coords.push_back(cv::Point2f(6.38445, 10.34772));
    coords.push_back(cv::Point2f(-1, -1));
    coords.push_back(cv::Point2f(5.59395, 7.40051));
    coords.push_back(cv::Point2f(6.27759, 10.37964));
    coords.push_back(cv::Point2f(5.37453, 9.00743));
    coords.push_back(cv::Point2f(-1, -1));
    coords.push_back(cv::Point2f(6.70113, 8.69055));
    coords.push_back(cv::Point2f(7.84705, 11.95494));
    coords.push_back(cv::Point2f(6.6624, 11.55899));
    coords.push_back(cv::Point2f(6.45735, 10.66545));
    coords.push_back(cv::Point2f(-1, -1));
    coords.push_back(cv::Point2f(4.12835, 5.84891));
    coords.push_back(cv::Point2f(5.50225, 7.09557));
    coords.push_back(cv::Point2f(3.66251, 9.20812));
    coords.push_back(cv::Point2f(3.31125, 7.70912));
    coords.push_back(cv::Point2f(-1, -1));
    coords.push_back(cv::Point2f(3.99797, 5.86889));
    coords.push_back(cv::Point2f(3.21427, 7.65308));
    coords.push_back(cv::Point2f(1.39421, 7.91604));
    coords.push_back(cv::Point2f(2.29916, 6.66408));
    coords.push_back(cv::Point2f(-1, -1));
    coords.push_back(cv::Point2f(3.08306, 7.78447));
    coords.push_back(cv::Point2f(0.24468, 9.7663));
    coords.push_back(cv::Point2f(1.30589, 8.04125));
    coords.push_back(cv::Point2f(-1, -1));
    coords.push_back(cv::Point2f(3.05762, 7.93796));
    coords.push_back(cv::Point2f(0.03658, 11.68999));
    coords.push_back(cv::Point2f(0.11137, 9.99509));
    coords.push_back(cv::Point2f(-1, -1));
    coords.push_back(cv::Point2f(3.23927, 7.88983));
    coords.push_back(cv::Point2f(3.56103, 9.26287));
    coords.push_back(cv::Point2f(0.26508, 11.5837));
    coords.push_back(cv::Point2f(-1, -1));


    // Reposition and resize coordinates
    for (int i = 0; i < coords.size(); i++)
    {
        if (coords[i].x == -1 && coords[i].y == -1)
            continue;

        if (mirror)
            coords[i].x = coords[i].x * -size + xpos;
        else
            coords[i].x = coords[i].x * size + xpos;

        coords[i].y = coords[i].y * size + ypos;
    }

    HPDF_Page_SetRGBFill(_page, red, green, blue);
    HPDF_Page_SetLineJoin(_page, HPDF_MITER_JOIN);
    HPDF_Page_MoveTo(_page, coords[0].x, coords[0].y);

    for (int i = 1; i < coords.size(); i++)
    {
        if (coords[i].x == -1 && coords[i].y == -1)
        {
            HPDF_Page_Fill(_page);

            if (i < coords.size() - 1)
            {
                i++;
                HPDF_Page_SetLineJoin(_page, HPDF_MITER_JOIN);
                HPDF_Page_MoveTo(_page, coords[i].x, coords[i].y);
            }
        }
        else
        {
            HPDF_Page_LineTo(_page, coords[i].x, coords[i].y);
        }
    }
}

void PDFReport::_drawFooter()
{
    float ypos = 41;
    float xpos = 20;

    float lineOffset = 11;
    int fontsize = 9;

    auto logoAssurance = cv::imread("logoAssurance.png", cv::IMREAD_COLOR);

    float area = 2500;
    float s = static_cast<float>(logoAssurance.cols) / static_cast<float>(logoAssurance.rows);
    float logoheight = sqrt(area / s);

    // Calculate logo width and height, so that the area is allways the same
    _drawImage(logoAssurance, 575, 17, -1, logoheight, -1, false, true);

    _drawText("Colibri Dent Scanner | PDR-Team GmbH", xpos , ypos, fontsize);
    _drawText("Martin-Luther-Weg 1 - 73527 Schwäbisch Gmünd - Deutschland", xpos, ypos - lineOffset, fontsize);
    _drawText("+49 (0) 800 308 1234 | info-de@pdr-team.com | www.pdr-team.com", xpos, ypos - lineOffset * 2, fontsize);
}

cv::Mat PDFReport::_createErrorImage()
{
    cv::Mat errorimage = cv::Mat::ones(300, 300, CV_8UC3);
    errorimage *= 100;
    cv::putText(errorimage, "No IMAGE!", cv::Point(15, 150), cv::FONT_HERSHEY_PLAIN, 3, cv::Scalar(0, 0, 255), 3);

    return errorimage;
}
