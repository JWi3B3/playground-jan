#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>

using namespace cv;
using namespace std;

void main()
{
    Mat image = imread("Marker2.png", IMREAD_GRAYSCALE);

    Mat image_nearest, image_linear, image_area, image_cubic, image_lanczos;
    resize(image, image_nearest, Size(10, 10), 0, 0, INTER_NEAREST);
    resize(image, image_linear, Size(10, 10), 0, 0, INTER_LINEAR);
    resize(image, image_area, Size(10, 10), 0, 0, INTER_AREA);
    resize(image, image_cubic, Size(10, 10), 0, 0, INTER_CUBIC);
    resize(image, image_lanczos, Size(10, 10), 0, 0, INTER_LANCZOS4);

    imwrite("nearest.png", image_nearest);
    imwrite("linear.png", image_linear);
    imwrite("area.png", image_area);
    imwrite("cubic.png", image_cubic);
    imwrite("lanczos.png", image_lanczos);
}