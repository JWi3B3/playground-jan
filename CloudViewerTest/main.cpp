#include <pcl/visualization/cloud_viewer.h>
#include <iostream>
#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>
#include <string>

#include "boost\make_shared.hpp"

int user_data;

pcl::PointCloud<pcl::PointXYZ>::Ptr create_sphere(double diameter, int pointcount)
{
    auto sphere = boost::make_shared<pcl::PointCloud<pcl::PointXYZ>>();

    double radius = diameter / 2.0;

    for (int i = 0; i < pointcount; i++)
    {
        double x = (double) rand() / RAND_MAX * 2. - 1;
        double y = (double) rand() / RAND_MAX * 2. - 1;
        double z = (double) rand() / RAND_MAX * 2. - 1;

        double length = sqrt(x*x + y*y + z*z) * radius;

        pcl::PointXYZ point;
        point.x = x / length;
        point.y = y / length;
        point.z = z / length;
        sphere->push_back(point);
    }

    sphere->width = pointcount;
    sphere->height = 1;
    return sphere;
}


void viewerOneOff(pcl::visualization::PCLVisualizer& viewer)
{
    viewer.setBackgroundColor(0.0, 0.0, 0.0);
    pcl::PointXYZ o;
    o.x = 1.0;
    o.y = 0;
    o.z = 0;
    
    std::cout << "i only run once" << std::endl;
}


void viewerPsycho(pcl::visualization::PCLVisualizer& viewer)
{
    static unsigned count = 0;
    std::stringstream ss;
    ss << "Once per viewer loop: " << count++;
    viewer.removeShape("text", 0);
    viewer.addText(ss.str(), 200, 300, "text", 0);

    if (!viewer.updatePointCloud(create_sphere(1.0, 1000), "Kugel"))
        viewer.addPointCloud(create_sphere(1.0, 1000), "Kugel");
}

int main()
{


    pcl::visualization::CloudViewer viewer("Cloud Viewer");

    //This will only get called once
    viewer.runOnVisualizationThreadOnce(viewerOneOff);

    //This will get called once per visualization iteration
    viewer.runOnVisualizationThread(viewerPsycho);
    while (!viewer.wasStopped())
    {
        user_data++;
    }
    return 0;
}