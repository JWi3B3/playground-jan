#include "opencv2\highgui.hpp"
#include "opencv2\imgproc.hpp"
#include "opencv2\core.hpp"
#include "ImageDisplay.hpp"

void main()
{
    //auto image = cv::imread("image.png");
    //ImageDisplay::show(image, "ImageDisplay Test");

    auto map3d = cv::imread("map3d.png", CV_LOAD_IMAGE_ANYDEPTH);
    ImageDisplay::show(map3d, "ImageDisplay Test");

    //auto map3d2 = cv::imread("map3d.png");
    //ImageDisplay::show(map3d2, "ImageDisplay Test");

    //auto frame3d = cv::imread("frame3d.png", CV_LOAD_IMAGE_ANYDEPTH);
    //ImageDisplay::show(frame3d, "ImageDisplay Test");
}