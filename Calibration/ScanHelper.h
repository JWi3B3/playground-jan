#include <vector>

#pragma once
class ScanHelper
{
public:
	ScanHelper();
	~ScanHelper();
	void set_mindistance(double distance);
	bool addposition(double position);
private:
	std::vector<double> data_position;
	double mindistance;
};

