#include <string>
#include <pcl/common/transforms.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include "DepthConverter.h"
#include "FrameListers.hpp"

#pragma once
class DepthCam
{
public:
	DepthCam(char* devicename);
	~DepthCam();


	void capture();

	void setTransformationMatrix(Eigen::Matrix4f transformation);
	void setResolution(int res);
	void setColor(int red, int green, int blue);

	void getPointCloud(pcl::PointCloud<pcl::PointXYZ>::Ptr pointcloud);
	void getColorPointCloud(pcl::PointCloud<pcl::PointXYZRGB>::Ptr pointcloud);
	pcl::ModelCoefficients getCameraCone();

private:
	astra::StreamSet streamSet;
	astra::StreamReader reader;
	astra::DepthStream depthStream;
	DepthConverter depthconverter;
	pcl::PointCloud<pcl::PointXYZ>::Ptr pointcloud;
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr pointcloud_color;
	Eigen::Matrix4f transformationmatrix;
	int resolution;
	int r, g, b;
	cv::Mat depthimage;
	std::unique_ptr<DepthFrameListener> listener;
};

