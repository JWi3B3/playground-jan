boost::shared_ptr<pcl::visualization::PCLVisualizer> simpleVis(pcl::PointCloud<pcl::PointXYZ>::ConstPtr cloud)
{
	// -------------------------------------------- 
	// -----Open 3D viewer and add point cloud----- 
	// -------------------------------------------- 
	boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer(new pcl::visualization::PCLVisualizer("3D Viewer"));
	viewer->setBackgroundColor(0, 0, 0);
	viewer->addPointCloud<pcl::PointXYZ>(cloud, "sample cloud");
	viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "sample cloud");
	//viewer->addCoordinateSystem (1.0, "global"); 
	viewer->initCameraParameters();
	return (viewer);
}

int main(int argc, char *argv[]) // Don't forget first integral argument 'argc' 
{
	// initialize PointClouds 
	std::string current_exec_name = argv[0]; // Name of the current exec program 
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_sphere(new pcl::PointCloud<pcl::PointXYZ>);

	// populate our PointCloud with points 
	pcl::io::loadPCDFile(argv[1], *cloud);

	//do RANSAC to find the largest sphere from the non plane points 
	pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients);
	pcl::PointIndices::Ptr inliers(new pcl::PointIndices);

	// Create the segmentation object 
	pcl::SACSegmentation<pcl::PointXYZ> seg;
	seg.setOptimizeCoefficients(true);

	seg.setModelType(pcl::SACMODEL_SPHERE);
	seg.setMethodType(pcl::SAC_RANSAC);
	seg.setMaxIterations(10000);
	seg.setDistanceThreshold(0.005);
	seg.setRadiusLimits(.06, .08); // The actual value is 7.25cm, but it even doesn't work with logically larger intervals as well. 
	seg.setProbability(.99);

	seg.setInputCloud(cloud);
	seg.segment(*inliers, *coefficients);

	if (inliers->indices.size() == 0)
	{
		PCL_ERROR("Could not estimate a sphere model for the given dataset.");
		std::cerr << "No sphere found" << std::endl;
		//return (-1); 
	}

	double x = coefficients->values[0];
	double y = coefficients->values[1];
	double z = coefficients->values[2];
	double r = coefficients->values[3];
	std::cerr << "Sphere coefficients: " << x << " " << y << " " << z << " " << r << std::endl;

	// Create the filtering object 
	pcl::ExtractIndices<pcl::PointXYZ> extract;

	// Extract the inliers 
	extract.setInputCloud(cloud);
	extract.setIndices(inliers);
	extract.setNegative(false);
	extract.filter(*cloud_sphere);
	std::cerr << "Original pointCloud: " << cloud->width * cloud->height << " data points." << std::endl;
	std::cerr << "PointCloud representing the sphere component: " << cloud_sphere->width * cloud_sphere->height << " data points." << std::endl;

	// creates the visualization object and adds either our orignial cloud or all of the inliers 
	// depending on the command line arguments specified. 
	boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer1, viewer2;

	viewer1 = simpleVis(cloud_sphere);

	while (!viewer1->wasStopped())
	{
		viewer1->spinOnce(100);
		boost::this_thread::sleep(boost::posix_time::microseconds(100000));
	}

	return 0;
}
