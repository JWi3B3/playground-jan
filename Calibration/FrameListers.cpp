/**
 * \file     FrameListeners.cpp
 *
 * \brief    Implementation of FrameListeners class.
 *
 * \author   Gennadi Eirich (gennadi.eirich@pdr-team.com)
 * \date     13.06.2016
 *
 * \version  0.1.1
 *
 * \note     Copyright (c) 2016, PDR-Team GmbH
 */


//#if defined (_DEBUG)
//    #include "../Debugtools/Timer.hpp"
//#endif
#include "FrameListers.hpp"
#include <iostream>

using namespace std;

DepthFrameListener::DepthFrameListener() : _frame(cv::Mat())
{
}


/**
 * This is called, when a new frame is available.
 */
void DepthFrameListener::on_frame_ready(astra::StreamReader& reader, astra::Frame& frame)
{
	const auto depthFrame = frame.get<astra::DepthFrame>();

	//getDepthFrameStatistics(depthFrame);

	if (depthFrame.is_valid())
	{
		lock_guard<mutex> lock(_frameMu);
		_frame = depthFrameToMat(depthFrame);
	}
}


/**
 * Returns the current frame.
 */
void DepthFrameListener::getFrame(cv::Mat& frame)
{
	lock_guard<mutex> lock(_frameMu);
	frame = _frame.clone();
}

void DepthFrameListener::getDepthFrameStatistics(const astra::DepthFrame& frame)
{
	double meanval = 0;
	int nonzero = 0;
	int w = frame.width();

	for (int y = 0; y < frame.height(); y++)
	{
		for (int x = 0; x < frame.width(); x++)
		{
			int pixelvalue = frame.data()[y*w + x];
			meanval += static_cast<double>(pixelvalue);

			if (pixelvalue != 0) nonzero++;
		}
	}

	meanval /= frame.width() * frame.height();
	cout << "Mean: " << meanval << ", Non-Zero Pixels: " << nonzero << endl;
}

DepthFrameListener::~DepthFrameListener()
{
}
