/**
 * \file     FrameListeners.hpp
 *
 * \brief    This class
 *
 * \details  ...
 *
 * \author   Gennadi Eirich (gennadi.eirich@pdr-team.com)
 * \date     13.06.2016
 *
 * \version  0.1.0
 *
 * \note     Copyright (c) 2016, PDR-Team GmbH
 */


#pragma once


#include <mutex>

//#include "Camera3D.hpp"

#include "astra/astra.hpp"
#include "opencv2/opencv.hpp"


class DepthFrameListener : public astra::FrameListener
{
	public:
		DepthFrameListener();
		~DepthFrameListener();

		void getFrame(cv::Mat& frame);
		void getDepthFrameStatistics(const astra::DepthFrame& frame);

	private:
		std::mutex _frameMu;
		cv::Mat _frame;

		void on_frame_ready(astra::StreamReader& reader, astra::Frame& frame) override;
};



static cv::Mat depthFrameToMat(const astra::DepthFrame& frame)
{
	const int h = frame.height();
	const int w = frame.width();

	cv::Mat image(h, w, CV_16UC1);
	frame.copy_to((short*)image.data);

	return image;
};
