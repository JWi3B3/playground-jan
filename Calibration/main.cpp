
#define _USE_MATH_DEFINES

#include <cstdio>
#include <iostream>
#include <math.h>
#include <chrono>
#include <vector>
#include <conio.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>

#include <astra/astra.hpp>

#include <pcl/common/common_headers.h>
#include <pcl/common/transforms.h>
#include <pcl/conversions.h>
#include <pcl/console/parse.h>
#include <pcl/features/normal_3d.h>
#include <pcl/filters/filter.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/surface/gp3.h>
#include <pcl/surface/organized_fast_mesh.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/visualization/keyboard_event.h>
#include <pcl/PolygonMesh.h>
#include "DepthCam.h"

#include "DepthConverter.h"
#include "CloudHandler.h"
#include "LabJack.h"
//#include "ScanHelper.h"
#include "SphereFinder.h"
#include "RangeVis.h"
#include "FileHandling.h"
#include "Calibration_InteractorStyle.h"

#include <mutex>

using namespace cv;
using namespace std;
using namespace pcl;

const double step_rot = 1;
const double step_trn = 0.01;

string text_rotatemode = "Rotation";
string text_translatemode = "Translation";

// Globale Variablen
int delta_x_rot = 0;
int delta_y_rot = 0;
int delta_z_rot = 0;
int delta_x_pos = 0;
int delta_y_pos = 0;
int delta_z_pos = 0;

int mode_transform = 0; // Modus 0-Translation, 1-Rotation
int cameraselection = 0;
int resolution = 10;

vector<uchar> redvals = {114, 0, 255, 255, 228};
vector<uchar> greenvals = {220, 144, 216, 54, 129};
vector<uchar> bluevals = { 0, 255, 0, 0, 227};


double scalefactor = 0.001;

int cameracount = 3;

void keyboardEventOccurred(const visualization::KeyboardEvent &event,
    void* viewer_void)
{
    //boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer = *static_cast<boost::shared_ptr<pcl::visualization::PCLVisualizer> *> (viewer_void);
    pcl::visualization::PCLVisualizer *viewer = static_cast<pcl::visualization::PCLVisualizer *> (viewer_void);

    if (event.keyDown())
    {
        std::string key = event.getKeySym();
        uchar c = event.getKeyCode();
        cout << key << endl;

        if (key == "Left")
        {
            if (mode_transform == 0) delta_x_pos += 1;
            else delta_x_rot += 1;
        }
            
        else if (key == "Right")
        {
            if (mode_transform == 0) delta_x_pos -= 1;
            else delta_x_rot -= 1;
        }

        else if (key == "Up")
        {
            if (mode_transform == 0) delta_y_pos += 1;
            else delta_y_rot += 1;
        }

        else if (key == "Down")
        {
            if (mode_transform == 0) delta_y_pos -= 1;
            else delta_y_rot -= 1;
        }

        else if (key == "Home")
        {
            if (mode_transform == 0) delta_z_pos += 1;
            else delta_z_rot += 1;
        }
        else if (key == "End")
        {
            if (mode_transform == 0) delta_z_pos -= 1;
            else delta_z_rot -= 1;
        }
        else if (key == "Prior")
        {
            mode_transform = 0;
        }
        else if (key == "Next")
        {
            mode_transform = 1;
        }
        else if (key == "Delete")
        {
            cameraselection -= 1;
            if (cameraselection < 0)
                cameraselection = cameracount - 1;
        }
        else if (key == "Insert")
        {
            cameraselection += 1;
            if (cameraselection > cameracount - 1)
                cameraselection = 0;
        }
        else if (key == "period")
        {
            resolution -= 1;
            if (resolution <  1)
                resolution = 1;
        }
        else if (key == "comma")
        {
            resolution += 1;
        }
    }
}



int main(int argc, char** argv)
{
    astra::initialize();

    double posrange = 10.0;
    int possteps = 2000;
    int res = 5;

    int x_pos= 1174,
        y_pos= 1014,
        z_pos= 1379;
    int x_rot=21,
        y_rot=2,
        z_rot=355;

    // Kameras initialisieren
    DepthCam cam1("device/sensor0");
    DepthCam cam2("device/sensor1");
    DepthCam cam3("device/sensor2");

    vector<DepthCam*> camerapointer;

    camerapointer.push_back(&cam1);
    camerapointer.push_back(&cam2);
    camerapointer.push_back(&cam3);

    for (int i = 0; i < cameracount; i++)
    {
        camerapointer[i]->setColor(redvals[i], greenvals[i], bluevals[i]);
        camerapointer[i]->setResolution(10);
    }

    auto sphere_real = CloudHandler::create_sphere(1, 100);
    PointCloud<PointXYZRGB>::Ptr sphere_color(new PointCloud<PointXYZRGB>);
    PointCloud<PointXYZ>::Ptr sphere(&sphere_real);
    
    CloudHandler::colorizePointCloud(sphere, sphere_color, 255, 255, 255);

    vector<PointCloud<PointXYZRGB>::Ptr> pointclouds_color;
    for (int i = 0; i < cameracount; i++)
    {
        pointclouds_color.push_back(PointCloud<PointXYZRGB>::Ptr(new PointCloud<PointXYZRGB>));
        *pointclouds_color[i] = *sphere_color;
    }

    auto matrix_scale_all = CloudHandler::get_scalematrix(scalefactor, scalefactor, scalefactor);

    vector<Eigen::Matrix4f> matrices_transform;
    for (int i = 0; i < cameracount; i++)
    {
        matrices_transform.push_back(matrix_scale_all);
    }

    // Zeitmessung
    auto time1 = std::chrono::system_clock::now();
    auto time2 = std::chrono::system_clock::now();

    LabJack labjack;
    //ScanHelper scanhelper;
    //scanhelper.set_mindistance(100);

    labjack.set_position(0, 3);
    labjack.set_position(2400, 0);

    double position;
    int timecounter = 0;

    // Viewer erstellen
    boost::shared_ptr<visualization::PCLVisualizer> viewer(new visualization::PCLVisualizer("3D Viewer"));
    viewer->setWindowName("3D Viewer");
    viewer->setBackgroundColor(0, 0, 0);
    viewer->addCoordinateSystem(1.0);
    viewer->initCameraParameters();
    //viewer->registerKeyboardCallback(keyboardEventOccurred, (void*)&viewer);
    viewer->registerKeyboardCallback(keyboardEventOccurred, (void*)viewer.get());

    viewer->addText(text_translatemode, 5, 20, "Mode");
    viewer->addText("Camera " + to_string(cameraselection), 5, 40, redvals[cameraselection] / 255., greenvals[cameraselection] / 255., bluevals[cameraselection] / 255., "Camera");
    viewer->addText("Resolution " + to_string(resolution), 5, 60, "Resolution");
    viewer->setShowFPS(false);

    for (int i = 0; i < cameracount; i++)
    {
        string cloudname = "Cloud " + to_string(i);
        viewer->addPointCloud(pointclouds_color[i], cloudname);
    }


    // Transformationen laden
    for (int i = 0; i < cameracount; i++)
    {
        //matrices_transform[i] = FileHandling::openEigenMatrix4f("transform" + to_string(i));
        //matrices_transform[i] = CloudHandler::get_rotatematrix(0, 0, 0);
    }

    int lastmode = mode_transform;
    int lastcamera = cameraselection;
    int lastresolution = resolution;

    while(!viewer->wasStopped())
    {
        astra_temp_update();

        //Tranformation verarbeiten, wenn sie sich veraendert hat
        if (delta_x_pos != 0 || delta_y_pos != 0 || delta_z_pos != 0 ||
            delta_x_rot != 0 || delta_y_rot != 0 || delta_z_rot != 0)
        {
            auto current_transform = CloudHandler::get_rotatematrix(0, 0, 0);

            current_transform = CloudHandler::get_translatematrix(delta_x_pos*step_trn, delta_y_pos*step_trn, delta_z_pos*step_trn)
                * CloudHandler::get_rotatematrix(delta_x_rot*step_rot, delta_y_rot*step_rot, delta_z_rot*step_rot);

            delta_x_pos = 0; delta_y_pos = 0; delta_z_pos = 0;
            delta_x_rot = 0; delta_y_rot = 0; delta_z_rot = 0;

            matrices_transform[cameraselection] = current_transform * matrices_transform[cameraselection];
        }

        // Text updaten
        if (lastmode != mode_transform || lastcamera != cameraselection || lastresolution != resolution)
        {
            lastmode = mode_transform;
            lastcamera = cameraselection;
            lastresolution = resolution;

            if (mode_transform == 0)
                viewer->updateText(text_translatemode, 5, 20, "Mode");
            else
                viewer->updateText(text_rotatemode, 5, 20, "Mode");

            viewer->updateText("Camera " + to_string(cameraselection), 5, 40, redvals[cameraselection]/255., greenvals[cameraselection] / 255., bluevals[cameraselection] / 255., "Camera");
            viewer->updateText("Resolution " + to_string(resolution), 5, 60, "Resolution");

            for (int i = 0; i < cameracount; i++)
            {
                camerapointer[i]->setResolution(resolution);
            }
        }

        // Alte Punktewolken l�schen
        for (int i = 0; i < cameracount; i++)
        {
            pointclouds_color[i]->clear();
        }

        // Position vom Distanzsensor abfragen
        position = labjack.get_mm();
        auto matrix_distance = CloudHandler::get_translatematrix(0, position / 1000, 0);

        for (int i = 0; i < cameracount; i++)
        {
            camerapointer[i]->capture();

            camerapointer[i]->setTransformationMatrix(matrix_distance * matrices_transform[i]);
            camerapointer[i]->getColorPointCloud(pointclouds_color[i]);

            string shapename = "Camera " + to_string(i);
            viewer->removeShape(shapename);
            viewer->addCone(camerapointer[i]->getCameraCone(), shapename);

            string cloudname = "Cloud " + to_string(i);
            viewer->updatePointCloud(pointclouds_color[i], cloudname);
        }

        viewer->spinOnce();

        timecounter++;
        if (timecounter > 9)
        {
            timecounter = 0;
            time2 = std::chrono::system_clock::now();
            auto elapsedtime = std::chrono::duration_cast<std::chrono::milliseconds>(time2 - time1);
            time1 = time2;
        }

    }

    astra::terminate();
    destroyAllWindows();

    return 0;
}