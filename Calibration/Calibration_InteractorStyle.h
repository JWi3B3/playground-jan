#include <pcl/visualization/cloud_viewer.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/visualization/keyboard_event.h>
#include <iostream>

#pragma once
class Calibration_InteractorStyle : public pcl::visualization::PCLVisualizerInteractorStyle
{
	
	virtual void OnKeyDown()
	{
		std::cout << "OnKeyDown!" << std::endl;
	}
};

