#include "ScanHelper.h"



ScanHelper::ScanHelper()
{
	this->mindistance = 0;
}


ScanHelper::~ScanHelper()
{
}

void ScanHelper::set_mindistance(double distance)
{
	this->mindistance = distance;
}

bool ScanHelper::addposition(double position)
{
	for (int i = 0; i < this->data_position.size(); i++)
	{
		if (abs(position - this->data_position[i]) < this->mindistance)
		{
			return false;
		}
	}

	this->data_position.push_back(position);
	return true;
}