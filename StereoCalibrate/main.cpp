#include <opencv2\opencv.hpp>
#include <vector>
#include <iostream>
#include <string>
#include "ImageDisplay.hpp"

std::vector<std::vector<cv::Mat>> readStereoFrames(std::string pathWithFileName, std::string extension, int startNumber, int endNumber)
{

    std::vector<std::vector<cv::Mat>> frames;
    cv::Mat frame;
    cv::Mat channels[3];
    std::vector<cv::Mat> stereoimages;
    for (int i = startNumber; i < endNumber + 1; i++)
    {
        frame = cv::imread(pathWithFileName + std::to_string(i) + "." + extension, cv::IMREAD_COLOR);
        cv::split(frame, channels);

        stereoimages.push_back(channels[0].clone());
        stereoimages.push_back(channels[2].clone());

        frames.push_back(stereoimages);
        stereoimages.clear();
    }

    return frames;
}

void main()
{
    auto frames = readStereoFrames("stereo", "png", 1, 18);

    auto testimage = cv::imread("TestRender.png", cv::IMREAD_COLOR);
    cv::Mat channels[3];
    cv::split(testimage, channels);
    auto imgL = channels[0].clone();
    auto imgR = channels[2].clone();

    std::vector<cv::Point2f> cornersL, cornersR;
    bool patternfoundL, patternfoundR;

    std::vector<cv::Point3f> patternpoints;
    float patternsize = 50;
    int patternHeight = 6, patternWidth = 9;
    for (int y = 0; y < patternHeight; y++)
    {
        for (int x = 0; x < patternWidth; x++)
        {
            patternpoints.push_back(cv::Point3f(x * patternsize, y * patternsize, 0.f));
        }
    }

    std::vector<std::vector<cv::Point3f>> objectPoints;
    std::vector<std::vector<cv::Point2f>> imagePointsL;
    std::vector<std::vector<cv::Point2f>> imagePointsR;

    for (int i = 0; i < frames.size(); i++)
    {
        patternfoundL = cv::findChessboardCorners(frames[i][0], cv::Size(9, 6), cornersL, cv::CALIB_CB_FAST_CHECK);
        patternfoundR = cv::findChessboardCorners(frames[i][1], cv::Size(9, 6), cornersR, cv::CALIB_CB_FAST_CHECK);

        if (patternfoundL && patternfoundR)
        {
            cv::cornerSubPix(frames[i][0], cornersL, cv::Size(11, 11), cv::Size(-1, -1),
                cv::TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 30, 0.1));
            cv::cornerSubPix(frames[i][1], cornersR, cv::Size(11, 11), cv::Size(-1, -1),
                cv::TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 30, 0.1));

            imagePointsL.push_back(cornersL);
            imagePointsR.push_back(cornersR);
            objectPoints.push_back(patternpoints);

            cv::cvtColor(frames[i][0], frames[i][0], CV_GRAY2BGR);
            cv::cvtColor(frames[i][1], frames[i][1], CV_GRAY2BGR);

            cv::drawChessboardCorners(frames[i][0], cv::Size(patternWidth, patternHeight), cornersL, patternfoundL);
            cv::drawChessboardCorners(frames[i][1], cv::Size(patternWidth, patternHeight), cornersR, patternfoundR);
    
            cv::imshow("Left", frames[i][0]);
            cv::imshow("Right", frames[i][1]);
            cv::waitKey(10);
        } 
    }

    cv::destroyAllWindows();


    cv::Mat cameraMatrixL, cameraMatrixR, distCoeffsL, distCoeffsR, R, T, E, F;

    std::cout << "stereoCalibrate...";

    cv::stereoCalibrate(objectPoints,
                        imagePointsL, imagePointsR,
                        cameraMatrixL, distCoeffsL,
                        cameraMatrixR, distCoeffsR,
                        frames[0][0].size(),
                        R, T, E, F,
                        cv::CALIB_SAME_FOCAL_LENGTH);

    std::cout << "finished!" << std::endl;


    std::cout << "cameraMatrixL:" << std::endl;
    std::cout << cameraMatrixL << std::endl;

    std::cout << "cameraMatrixR:" << std::endl;
    std::cout << cameraMatrixR << std::endl;

    std::cout << "distCoeffsL:" << std::endl;
    std::cout << distCoeffsL << std::endl;

    std::cout << "distCoeffsR:" << std::endl;
    std::cout << distCoeffsR << std::endl;

    std::cout << "R:" << std::endl;
    std::cout << R << std::endl;

    std::cout << "T:" << std::endl;
    std::cout << T << std::endl;

    std::cout << "E:" << std::endl;
    std::cout << E << std::endl;

    std::cout << "F:" << std::endl;
    std::cout << F << std::endl;

    std::cout << std::endl << "stereoRectify...";


    cv::Mat RMatL, RMatR, PMatL, PMatR, QMat;
    cv::stereoRectify(  cameraMatrixL, distCoeffsL,
                        cameraMatrixR, distCoeffsR,
                        frames[0][0].size(),
                        R, T,
                        RMatL, RMatR,
                        PMatL, PMatR,
                        QMat);

    std::cout << "finished!" << std::endl;


    cv::Mat map1L, map1R, map2L, map2R;
    cv::initUndistortRectifyMap(cameraMatrixL, distCoeffsL, RMatL, PMatL, frames[0][0].size(), CV_16SC2, map1L, map2L);
    cv::initUndistortRectifyMap(cameraMatrixR, distCoeffsR, RMatR, PMatR, frames[0][0].size(), CV_16SC2, map1R, map2R);

    cv::Mat imgUndistortL, imgUndistortR;
    cv::remap(imgL, imgUndistortL, map1L, map2L, cv::INTER_CUBIC);
    cv::remap(imgR, imgUndistortR, map1R, map2R, cv::INTER_CUBIC);

    cv::imshow("LeftImage", imgUndistortL);
    cv::imshow("RightImage", imgUndistortR);
    cv::waitKey(10);
    cv::destroyAllWindows();

    cv::Ptr<cv::StereoSGBM> stereo = cv::StereoSGBM::create(
        -256, 512, 11, 100, 1000,
        32, 0, 15, 1000, 16,
        false
    );

    cv::Mat disparityMap;
    stereo->compute(imgUndistortL, imgUndistortR, disparityMap);

    ImageDisplay::show(disparityMap, "Disparity Map");

    cv::Mat image3D;
    cv::reprojectImageTo3D(disparityMap, image3D, QMat);

    cv::viz::writeCloud("image3d.ply", image3D);

}