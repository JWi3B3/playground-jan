#include <opencv2\opencv.hpp>
#include <vector>
#include <string>

void main()
{
    cv::Mat image = cv::imread("image.jpg", cv::IMREAD_COLOR);

    std::vector<cv::Mat> frames;


    for (int i = 1; i < 9; i++)
    {
        frames.push_back(cv::imread("frame" + std::to_string(i) + ".png", cv::IMREAD_GRAYSCALE).clone());
    }

    cv::Mat binary;
    
    int threshold = 128;
    int onoff = 0;

    cv::namedWindow("Bild 1");
    cv::createTrackbar("Threshold", "Bild 1", &threshold, 255);
    cv::createTrackbar("Button", "Bild 1", &onoff, 1);

    int counter = 0;

    while (true)
    {
        cv::threshold(image, binary, threshold, 255, CV_THRESH_BINARY);

        cv::imshow("Bild 1", binary);



        cv::imshow("frames", frames[counter]);

        counter++;
        if (counter > 7)
            counter = 0;

        if (cv::waitKey(30) == 27)
            break;
    }

    cv::destroyAllWindows();
}