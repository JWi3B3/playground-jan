#include <cstdio>
#include <iostream>
#include <math.h>
#include <chrono>
#include <vector>
#include <conio.h>
#include <string.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include "Helpers.h"

using namespace std;
using namespace cv;

int angles = 360;
int stepsize = 4;

void main()
{
    Mat image = imread("image.png", IMREAD_GRAYSCALE);
    Mat rotatedimage, temp, map_flowfilter;


    vector<Mat> images_processed_1;
    vector<Mat> images_processed_2;
    vector<Mat> images_processed_3;
    vector<Mat> images_processed_4;
    vector<Mat> images_processed_5;
    vector<Mat> images_processed_6;


    imshow("original image", image);
    waitKey(1);

    Point center = Point(image.cols / 2, image.rows / 2);

    int newimwidth = 0;
    if (image.rows < image.cols)
        newimwidth = image.rows / sqrt(2) - 10;
    else
        newimwidth = image.cols / sqrt(2) - 10;

    Rect roi = Rect(image.cols / 2 - newimwidth / 2, image.rows / 2 - newimwidth / 2, newimwidth, newimwidth);

    for (int i = 0; i < angles/stepsize; i++)
    {
        Mat rot_mat = getRotationMatrix2D(center, i*stepsize, 1.0);
        warpAffine(image, rotatedimage, rot_mat, image.size());

        // Flowfilter
        vector<Mat> channels(6);
        cv::cornerEigenValsAndVecs(rotatedimage, temp, 15, 3);
        split(temp, channels);

        images_processed_1.push_back(channels[0](roi).clone());
        images_processed_2.push_back(channels[1](roi).clone());
        images_processed_3.push_back(channels[2](roi).clone());
        images_processed_4.push_back(channels[3](roi).clone());
        images_processed_5.push_back(channels[4](roi).clone());
        images_processed_6.push_back(channels[5](roi).clone());

        cout << i << endl;
    }


    int angle = 0;
    while (true)
    {
        angle++;
        //if (angle >= angles/stepsize) angle = 0;
        if (angle >= angles / stepsize) break;


        imshow("Filterimage_1", Helpers::autoscale(images_processed_1[angle]));
        imshow("Filterimage_2", Helpers::autoscale(images_processed_2[angle]));
        imshow("Filterimage_3", Helpers::autoscale(images_processed_3[angle]));
        imshow("Filterimage_4", Helpers::autoscale(images_processed_4[angle]));
        imshow("Filterimage_5", Helpers::autoscale(images_processed_5[angle]));
        imshow("Filterimage_6", Helpers::autoscale(images_processed_6[angle]));

        double minval, maxval;
        minMaxIdx(images_processed_2[angle], &minval, &maxval);

        imwrite("Filterimage_" + Helpers::to_string_leadingZeros(angle, 4) + ".png", Helpers::autoscale(images_processed_2[angle], 0, -1, maxval*0.2));

        if (waitKey(100) == 27)
            break;
    }
}