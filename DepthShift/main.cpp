#define _USE_MATH_DEFINES

#include "opencv2\opencv.hpp"
#include "ImageDisplay.hpp"
#include <chrono>
#include <iostream>
#include <vector>
#include <string>
#include <thread>
#include <math.h>
#include "Timer.hpp"

cv::Mat autoRange(cv::Mat& src, int minpercent=0, int maxpercent=100)
{
    cv::Mat dst;

    double minval, maxval;
    cv::minMaxIdx(src, &minval, &maxval);

    double minimum, maximum;
    minimum = minval + (minpercent / 100.) * (maxval - minval);
    maximum = minval + (maxpercent / 100.) * (maxval - minval);

    double a = 255. / (maximum - minimum);
    double b = -a * minimum;

    src.convertTo(dst, CV_8UC3, a, b);

    return dst;
}

// take number image type number (from cv::Mat.type()), get OpenCV's enum string.
std::string getImgType(int imgTypeInt)
{
    int numImgTypes = 35; // 7 base types, with five channel options each (none or C1, ..., C4)

    int enum_ints[] = { CV_8U,  CV_8UC1,  CV_8UC2,  CV_8UC3,  CV_8UC4,
        CV_8S,  CV_8SC1,  CV_8SC2,  CV_8SC3,  CV_8SC4,
        CV_16U, CV_16UC1, CV_16UC2, CV_16UC3, CV_16UC4,
        CV_16S, CV_16SC1, CV_16SC2, CV_16SC3, CV_16SC4,
        CV_32S, CV_32SC1, CV_32SC2, CV_32SC3, CV_32SC4,
        CV_32F, CV_32FC1, CV_32FC2, CV_32FC3, CV_32FC4,
        CV_64F, CV_64FC1, CV_64FC2, CV_64FC3, CV_64FC4 };

    std::string enum_strings[] = { "CV_8U",  "CV_8UC1",  "CV_8UC2",  "CV_8UC3",  "CV_8UC4",
        "CV_8S",  "CV_8SC1",  "CV_8SC2",  "CV_8SC3",  "CV_8SC4",
        "CV_16U", "CV_16UC1", "CV_16UC2", "CV_16UC3", "CV_16UC4",
        "CV_16S", "CV_16SC1", "CV_16SC2", "CV_16SC3", "CV_16SC4",
        "CV_32S", "CV_32SC1", "CV_32SC2", "CV_32SC3", "CV_32SC4",
        "CV_32F", "CV_32FC1", "CV_32FC2", "CV_32FC3", "CV_32FC4",
        "CV_64F", "CV_64FC1", "CV_64FC2", "CV_64FC3", "CV_64FC4" };

    for (int i = 0; i<numImgTypes; i++)
    {
        if (imgTypeInt == enum_ints[i]) return enum_strings[i];
    }
    return "unknown image type";
}

void shiftDepthFrame(cv::Mat& inputOutputImage, double offset, double maxViewAngleHoriz, int roiX = 0, int roiWidth = 0)
{
    cv::Mat srcimage = inputOutputImage.clone();

    int width = srcimage.cols;
    int height = srcimage.rows;
    int widthhalf = width / 2;

    // ROI bearbeiten damit sie zuverl�ssig funktioniert
    if (roiX == 0 && roiWidth == 0)
    {
        roiX = 0;
        roiWidth = width;
    }

    if (roiWidth < 1)
        roiWidth = 1;

    if (roiX > width - 1)
        roiX = width - 1;

    if (roiWidth > width - roiX)
        roiWidth = width - roiX;
    
    
    // Keine Bearbeitung wenn offset gleich null
    if (offset == 0)
    {
        inputOutputImage = inputOutputImage(cv::Rect(roiX, 0, roiWidth, height)).clone();
        return;
    }

    // Spiegeln, wenn offset negativ
    if (offset < 0) 
    {
        cv::flip(srcimage, srcimage, 1);
        roiX = width - roiX - roiWidth;
    }
  
    // Brennweite berechnen
    double f = width / std::tan(maxViewAngleHoriz * M_PI / 180.);
    double f2 = f*f;
    inputOutputImage = inputOutputImage(cv::Rect(roiX, 0, roiWidth, height)).clone();

    // Damit der linke Rand richtig funktioniert
    srcimage(cv::Rect(0, 0, 1, height)) = 0;

    double  xDiff = 0;
    double  xDiffPre = 0;
    double  lastDepth = 0;
    int     lastXPos = 1;
    double  xpos = 0;

    // Zwischenergebnisse berechnen, damit es sp�ter nicht so lange dauert
    double xc[2000];
    double interim[2000];

    for (int i = 0; i < width; i++)
    {
        xc[i] = i - widthhalf;
        interim[i] = abs(offset) * std::sqrt(xc[i] * xc[i] + f2);
    }

    // �ber alle Zeilen
    for (int y = 0; y < height; y++)
    {
        float* ptrDst = inputOutputImage.ptr<float>(y);
        float* ptrSrc = srcimage.ptr<float>(y);

        lastXPos = 1;

        // �ber alle Spalten
        for (int x = 0 ; x < roiWidth; x++)
        {
            xDiffPre = 0;

            // �ber alle Pixel der Spalte
            for (int x2 = lastXPos; x2 < width; x2++)
            {
                if (ptrSrc[x2] < 1.0)
                    lastDepth = 20000;
                else
                    lastDepth = ptrSrc[x2];

                // Berechnung der neuen Position
                xpos = xc[x2] + interim[x2] / lastDepth + widthhalf;
                xDiff = xpos - (x + roiX);

                // Wenn neue Position mit dem aktuellen Pixel �bereinstimmt
                if (xDiff > 0)
                {
                    // Nearest Neighbour
                    if (xDiffPre < xDiff)
                        lastXPos = x2 - 1;
                    else
                        lastXPos = x2;

                    // Damit nicht au�erhalb der Zeile zugegriffen wird
                    if (lastXPos < 0)
                        lastXPos = 0;

                    break;
                }

                xDiffPre = xDiff;
            }
            
            ptrDst[x] = ptrSrc[lastXPos];
        }
    }

    if (offset < 0)
        cv::flip(inputOutputImage, inputOutputImage, 1);
}

void main()
{
    int offset = 250;
    int minpercent = 0;
    int maxpercent = 100;
    int xpos = 0;
    int width = 1000;

    cv::namedWindow("Settings");
    
    cv::createTrackbar("offset", "Settings", &offset, 500);
    cv::createTrackbar("min", "Settings", &minpercent, 100);
    cv::createTrackbar("max", "Settings", &maxpercent, 100);
    cv::createTrackbar("xpos", "Settings", &xpos, 1000);
    cv::createTrackbar("width", "Settings", &width, 1000);

    auto frame = cv::imread("533.png", CV_LOAD_IMAGE_ANYDEPTH);
    //auto frame = cv::imread("394.png", CV_LOAD_IMAGE_ANYDEPTH);


    //auto frame = cv::imread("depthmap.png", CV_LOAD_IMAGE_GRAYSCALE);
    //frame.convertTo(frame, CV_16U, 20);

    cv::Mat shiftedFrame = cv::Mat::zeros(100,100, CV_16U);

    frame.convertTo(frame, CV_32F);

    int offsetAnimation = -200;
    int stepsize = 12;
    int counter = 0;
    while (true)
    {
        Timer shifttime;
        shiftedFrame = frame.clone();

        shifttime.start();
        shiftDepthFrame(shiftedFrame, double(offsetAnimation), 50, xpos, width);
        shifttime.stop();

        cv::imshow("shiftedframe", autoRange(shiftedFrame, minpercent, maxpercent));

        cv::imwrite("frame" + std::to_string(counter) + ".png", autoRange(shiftedFrame, minpercent, maxpercent));

        offsetAnimation += stepsize;
        counter++;

        if (offsetAnimation > 200)
        {
            offsetAnimation = 200;
            stepsize *= -1;
        }
        if (offsetAnimation < -200)
        {
            offsetAnimation = -200;
            stepsize *= -1;
            break;
        }

        if (cv::waitKey(1) == 27)
            break;
    }
}