#include <iostream>
#include "opencv2\opencv.hpp"
#include "ComponentSegmentation.h"
#include <vector>

void main()
{
    std::vector<cv::Mat> images;

    images.push_back(cv::imread("left.png", cv::IMREAD_GRAYSCALE));
    images.push_back(cv::imread("top.png", cv::IMREAD_GRAYSCALE));
    images.push_back(cv::imread("right.png", cv::IMREAD_GRAYSCALE));

    ComponentSegmentation comp;
    auto components = comp.run(images);
    for (int i = 0; i < 14; i++)
    {
        cv::imshow(std::to_string(i), components[static_cast<Component>(i)]);
        cv::imwrite(std::to_string(i) + ".png", components[static_cast<Component>(i)]);
    }
    cv::waitKey();
    cv::destroyAllWindows();
}