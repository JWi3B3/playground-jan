#pragma once

#include <vector>
#include <string>
#include "opencv2\opencv.hpp"


enum Component {
    BONNET, ROOF, TRUNK_TOP, TRUNK_BOTTOM, ROOF_SPAR_LEFT,
    ROOF_SPAR_RIGHT, FENDER_FRONT_LEFT, FENDER_FRONT_RIGHT, DOOR_FRONT_LEFT,
    DOOR_FRONT_RIGHT, DOOR_REAR_LEFT, DOOR_REAR_RIGHT, FENDER_REAR_LEFT, FENDER_REAR_RIGHT
};


struct Button {
    int state = 0;
    int xpos = 0;
    int ypos = 0;
    int width = 100;
    int height = 50;
    std::string title = "Button";
};

class ComponentSegmentation
{
public:
    ComponentSegmentation();
    ~ComponentSegmentation();

    std::map<Component, cv::Mat> run(std::vector<cv::Mat> images);


private:
    // Constants
    const int       _viewWidth = 1500;
    const int       _viewHeight = 1000;
    const int       _sidebarWidth = 400;
    const int       _buttonViewXPos = 200;
    const int       _buttonViewYPos = 870;
    const int       _buttonViewRadiusOut = 100;
    const int       _buttonViewRadiusIn = 50;
    const int       _speedView = 10;
    const double    _scalefactor = 0.6;
    const cv::Scalar colorActive = cv::Scalar(0, 255, 0);
    const cv::Scalar colorIdle = cv::Scalar(190, 190, 190);
    const cv::Scalar colorBackground = cv::Scalar(80, 80, 80);
    const cv::Scalar colorContourActive = cv::Scalar(0, 255, 0);
    const cv::Scalar colorContourIdle = cv::Scalar(0, 100, 0);




    // Variables
    std::vector<std::vector<cv::Point>> _componentButtons;
    cv::Mat _combinedimage;
    int     _selectedComponent;

    std::vector<std::vector<cv::Point>> _componentSelections;

    // Mouse and button states
    bool _mouseLDown = false;
    bool _mouseRDown = false;
    int _mouseX = 0, _mouseY = 0;
    int _mouseXOld = 0, _mouseYOld = 0;

    int _viewX = 0, _viewY = 0;
    bool _buttonViewPressed = false;
    bool _updateOverlay = true;
    bool _updateSidebar = true;
    bool _finish = false;
    bool _cancel = false;

    std::vector<Button> buttons;
   

    // Functions
    void        _combineImages(std::vector<cv::Mat> images);
    cv::Mat     _drawSidebar();
    void        _drawSelections(cv::Mat & image);
    bool        _checkMouseOverButton(Button button, int mouseX, int mouseY);
    static void _rotateRight(cv::Mat & frame);

    static void CallBackFunc(int event, int x, int y, int flags, void * userdata);
    void        _doMouseCallback(int event, int x, int y, int flags);
};

