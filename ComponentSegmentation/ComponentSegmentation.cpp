#define _USE_MATH_DEFINES

#include "ComponentSegmentation.h"
#include "ImageDisplay.hpp"
#include <math.h>


ComponentSegmentation::ComponentSegmentation()
{
    // Buttons
    int xpos = 65;
    int ypos = 500;
    int distY = 60;
    Button button = { 0, xpos, ypos, 130, 50, "Clear" };
    buttons.push_back(button);

    button = { 0, xpos, ypos + distY, 130, 50, "Clear All" };
    buttons.push_back(button);

    button = { 0, xpos + 140, ypos, 130, 50, "Cancel" };
    buttons.push_back(button);

    button = { 0, xpos + 140, ypos + distY, 130, 50, "Finish" };
    buttons.push_back(button);

    _selectedComponent = 0;

    std::vector<cv::Point> emptyContour;
    for (int i = 0; i < 14; i++)
        _componentSelections.push_back(emptyContour);

    // Create component contours
    std::vector<cv::Point> cont;


    // Bonnet
    cont.push_back(cv::Point(152, 32));
    cont.push_back(cv::Point(248, 32));
    cont.push_back(cv::Point(279, 152));
    cont.push_back(cv::Point(120, 152));

    _componentButtons.push_back(cont);
    cont.clear();

    // Roof
    cont.push_back(cv::Point(168, 160));
    cont.push_back(cv::Point(232, 160));
    cont.push_back(cv::Point(232, 312));
    cont.push_back(cv::Point(168, 312));

    _componentButtons.push_back(cont);
    cont.clear();

    // Trunk Top
    cont.push_back(cv::Point(120, 320));
    cont.push_back(cv::Point(280, 320));
    cont.push_back(cv::Point(280, 360));
    cont.push_back(cv::Point(120, 360));

    _componentButtons.push_back(cont);
    cont.clear();       

    // Trunk Bottom
    cont.push_back(cv::Point(120, 368));
    cont.push_back(cv::Point(280, 368));
    cont.push_back(cv::Point(280, 416));
    cont.push_back(cv::Point(120, 416));

    _componentButtons.push_back(cont);
    cont.clear();
        
    // Roof Spar Left
    cont.push_back(cv::Point(120, 160));
    cont.push_back(cv::Point(160, 160));
    cont.push_back(cv::Point(160, 312));
    cont.push_back(cv::Point(120, 312));

    _componentButtons.push_back(cont);
    cont.clear();

    // Roof Spar Right
    cont.push_back(cv::Point(240, 160));
    cont.push_back(cv::Point(280, 160));
    cont.push_back(cv::Point(280, 312));
    cont.push_back(cv::Point(240, 312));

    _componentButtons.push_back(cont);
    cont.clear();

    // Fender Front Left
    cont.push_back(cv::Point(32, 32));
    cont.push_back(cv::Point(80, 32));
    cont.push_back(cv::Point(112, 152));
    cont.push_back(cv::Point(32, 152));
    cont.push_back(cv::Point(32, 128));
    cont.push_back(cv::Point(56, 128));
    cont.push_back(cv::Point(72, 112));
    cont.push_back(cv::Point(72, 88));
    cont.push_back(cv::Point(56, 72));
    cont.push_back(cv::Point(32, 72));

    _componentButtons.push_back(cont);
    cont.clear();

    // Fender Front Right
    cont.push_back(cv::Point(320, 32));
    cont.push_back(cv::Point(368, 32));
    cont.push_back(cv::Point(368, 72));
    cont.push_back(cv::Point(344, 72));
    cont.push_back(cv::Point(328, 88));
    cont.push_back(cv::Point(328, 112));
    cont.push_back(cv::Point(344, 128));
    cont.push_back(cv::Point(368, 128));
    cont.push_back(cv::Point(368, 152));
    cont.push_back(cv::Point(288, 152));

    _componentButtons.push_back(cont);
    cont.clear();

    // Door Front Left
    cont.push_back(cv::Point(32, 160));
    cont.push_back(cv::Point(112, 160));
    cont.push_back(cv::Point(112, 232));
    cont.push_back(cv::Point(32, 232));

    _componentButtons.push_back(cont);
    cont.clear();

    // Door Front Right
    cont.push_back(cv::Point(288, 160));
    cont.push_back(cv::Point(368, 160));
    cont.push_back(cv::Point(368, 232));
    cont.push_back(cv::Point(288, 232));

    _componentButtons.push_back(cont);
    cont.clear();

    // Door Rear Left
    cont.push_back(cv::Point(32, 240));
    cont.push_back(cv::Point(112, 240));
    cont.push_back(cv::Point(112, 312));
    cont.push_back(cv::Point(32, 312));

    _componentButtons.push_back(cont);
    cont.clear();

    // Door Rear Right
    cont.push_back(cv::Point(288, 240));
    cont.push_back(cv::Point(368, 240));
    cont.push_back(cv::Point(368, 312));
    cont.push_back(cv::Point(288, 312));

    _componentButtons.push_back(cont);
    cont.clear();

    // Fender Rear Left
    cont.push_back(cv::Point(32, 320));
    cont.push_back(cv::Point(112, 320));
    cont.push_back(cv::Point(112, 416));
    cont.push_back(cv::Point(32, 416));
    cont.push_back(cv::Point(32, 392));
    cont.push_back(cv::Point(56, 392));
    cont.push_back(cv::Point(72, 376));
    cont.push_back(cv::Point(72, 352));
    cont.push_back(cv::Point(56, 336));
    cont.push_back(cv::Point(32, 336));

    _componentButtons.push_back(cont);
    cont.clear();

    // Fender Rear Right
    cont.push_back(cv::Point(368, 320));
    cont.push_back(cv::Point(288, 320));
    cont.push_back(cv::Point(288, 416));
    cont.push_back(cv::Point(368, 416));
    cont.push_back(cv::Point(368, 392));
    cont.push_back(cv::Point(344, 392));
    cont.push_back(cv::Point(328, 376));
    cont.push_back(cv::Point(328, 352));
    cont.push_back(cv::Point(344, 336));
    cont.push_back(cv::Point(368, 336));

    _componentButtons.push_back(cont);
    cont.clear();
}


ComponentSegmentation::~ComponentSegmentation()
{
}

std::map<Component, cv::Mat> ComponentSegmentation::run(std::vector<cv::Mat> images)
{
    cv::namedWindow("Segmentation");
    cv::setMouseCallback("Segmentation", CallBackFunc, this);

    _combineImages(images);

    
    
    cv::Mat combined_lowres;
    cv::resize(_combinedimage, combined_lowres, cv::Size(_combinedimage.cols*_scalefactor, _combinedimage.rows*_scalefactor), 0.0, 0.0, cv::INTER_CUBIC);

    // Extend image if it is too small
    if (combined_lowres.cols < _viewWidth)
        cv::copyMakeBorder(combined_lowres, combined_lowres, 0, 0, 0, _viewWidth - combined_lowres.cols, cv::BORDER_CONSTANT, 0);
    if (combined_lowres.rows < _viewHeight)
        cv::copyMakeBorder(combined_lowres, combined_lowres, 0, _viewHeight - combined_lowres.rows, 0, 0, cv::BORDER_CONSTANT, 0);

    cv::cvtColor(combined_lowres, combined_lowres, CV_GRAY2BGR);
    cv::Mat combined_draw = combined_lowres.clone();
    cv::Mat overlay = combined_lowres.clone() * 0;

    cv::Mat img_sidebar;
    cv::Mat displayimage;
    cv::Mat completeimage = cv::Mat::zeros(_viewHeight, _viewWidth + _sidebarWidth, CV_8UC3);

    while (true)
    {
        if (_mouseRDown)
        {
            _viewX -= _mouseX - _mouseXOld;
            _viewY -= _mouseY - _mouseYOld;
        }

        if (_mouseLDown)
        {
            if (_mouseX < _viewWidth)
            {
                cv::Point newpoint(_mouseX + _viewX, _mouseY + _viewY);
                if (_componentSelections[_selectedComponent].size() > 0)
                {
                    if (cv::norm(newpoint - _componentSelections[_selectedComponent].back()) > 5)
                    {
                        _componentSelections[_selectedComponent].push_back(newpoint);
                    }
                }
                else
                {
                    _componentSelections[_selectedComponent].push_back(newpoint);
                }
                _updateOverlay = true;
            }
            else
            {
                _updateSidebar = true;
                // Check View Circle Button
                int dist = cv::norm(cv::Point(_mouseX, _mouseY) - cv::Point(_buttonViewXPos + _viewWidth, _buttonViewYPos));
                if (dist >= _buttonViewRadiusIn && dist <= _buttonViewRadiusOut)
                {
                    _buttonViewPressed = true;
                    double angle = atan2(_mouseX - (_buttonViewXPos + _viewWidth), _mouseY - _buttonViewYPos);
                    _viewX += sin(angle) * _speedView;
                    _viewY += cos(angle) * _speedView;
                }
            }

        }
        else
            _buttonViewPressed = false;

        if (_updateOverlay)
        {
            combined_draw = combined_lowres.clone();
            _drawSelections(combined_draw);
            _updateOverlay = false;
        }

        _mouseXOld = _mouseX;
        _mouseYOld = _mouseY;
        
        _viewX = std::max(0, std::min(_viewX, combined_draw.cols - _viewWidth));
        _viewY = std::max(0, std::min(_viewY, combined_draw.rows - _viewHeight));

        if (_updateSidebar)
        {
            img_sidebar = _drawSidebar();
            _updateSidebar = false;
        }

        displayimage = combined_draw(cv::Rect(_viewX, _viewY, _viewWidth, _viewHeight));

        img_sidebar.copyTo(completeimage(cv::Rect(_viewWidth, 0, _sidebarWidth, _viewHeight)));
        displayimage.copyTo(completeimage(cv::Rect(0, 0, _viewWidth, _viewHeight)));

        cv::imshow("Segmentation", completeimage);

        if (cv::waitKey(1) == 27)
        {
            _finish = true;
            break;
        }

        if (_cancel || _finish)
            break;
    }

    std::map<Component, cv::Mat> output;

    if (_cancel)
    {
        for (int i = 0; i < images.size(); i++)
        {
            output[static_cast<Component>(i)] = images[i].clone();
        }
        for (int i = images.size(); i < 14; i++)
        {
            output[static_cast<Component>(i)] = cv::Mat::zeros(200, 200, CV_8UC1);
        }

    }
    else if (_finish)
    {
        for (int i = 0; i < _componentSelections.size(); i++)
        {
            if (_componentSelections[i].size() > 1)
            {
                // Correct all selections to real size
                for (int k = 0; k < _componentSelections[i].size(); k++)
                {
                    _componentSelections[i][k].x /= _scalefactor;
                    _componentSelections[i][k].y /= _scalefactor;

                }
              
                cv::Rect roi = cv::boundingRect(_componentSelections[i]);
                cv::Mat mask = cv::Mat::zeros(roi.height, roi.width, CV_8UC1);
                cv::drawContours(mask, _componentSelections, i,
                    cv::Scalar(1), -1, cv::LINE_8, cv::noArray(), 2146483647, cv::Point(-roi.x, -roi.y));
                cv::multiply(mask, _combinedimage(roi).clone(), mask);
                output[static_cast<Component>(i)] = mask.clone();

            }
            else
            {
                output[static_cast<Component>(i)] = cv::Mat::zeros(200, 200, CV_8UC1);
            }
        }
    }

    cv::destroyWindow("Segmentation");

    return output;
}

void ComponentSegmentation::_drawSelections(cv::Mat & image)
{
    cv::Mat overlay = cv::Mat::zeros(image.size(), CV_8UC3);

    // Draw Overlay
    for (int i = 0; i < 14; i++)
    {
        if (_componentSelections[i].size() > 1)
            if (i == _selectedComponent)
                cv::drawContours(overlay, _componentSelections, i, cv::Scalar(0, 50, 0), -1, cv::LINE_8);
            else
                cv::drawContours(overlay, _componentSelections, i, cv::Scalar(0, 25, 0), -1, cv::LINE_8);
    }

    image += overlay;

    // Draw Contours
    for (int i = 0; i < 14; i++)
    {
        if (_componentSelections[i].size() > 1)
            if (i == _selectedComponent)
            {
                cv::drawContours(image, _componentSelections, i, colorContourActive, 2, cv::LINE_AA);
            }
            else
            {
                cv::drawContours(image, _componentSelections, i, colorContourIdle, 2, cv::LINE_AA);
            }
    }
}

bool ComponentSegmentation::_checkMouseOverButton(Button button, int mouseX, int mouseY)
{
    if (mouseX >= button.xpos && mouseX <= button.xpos + button.width
        && mouseY >= button.ypos && mouseY <= button.ypos + button.height)
        return true;
    
    else
        return false;
}

void ComponentSegmentation::_combineImages(std::vector<cv::Mat> images)
{
    std::vector<cv::Mat> imagesCopy;

    // Copy and rotate all images and get maximum height
    int maxheight = 0;
    int width = 0;
    for (int i = 0; i < images.size(); i++)
    {
        imagesCopy.push_back(images[i].clone());
        _rotateRight(imagesCopy[i]);
        _rotateRight(imagesCopy[i]);
        _rotateRight(imagesCopy[i]);

        if (imagesCopy[i].rows > maxheight)
            maxheight = imagesCopy[i].rows;

        width += imagesCopy[i].cols;
    }

    // Create combined image
    _combinedimage = cv::Mat::zeros(maxheight, width, CV_8UC1);
    int xpos = 0;
    for (int i = 0; i < imagesCopy.size(); i++)
    {
        if (imagesCopy[i].rows < maxheight)
            cv::copyMakeBorder(imagesCopy[i], imagesCopy[i], 0, maxheight - imagesCopy[i].rows, 0, 0, cv::BORDER_CONSTANT, 0);

        imagesCopy[i].copyTo(_combinedimage(cv::Rect(xpos, 0, imagesCopy[i].cols, maxheight)));

        xpos += imagesCopy[i].cols;
    }
}

cv::Mat ComponentSegmentation::_drawSidebar()
{
    cv::Mat image = cv::Mat(_viewHeight, _sidebarWidth, CV_8UC3, colorBackground);
    

    // Draw Components
    cv::Scalar color;
    for (int i = 0; i < _componentButtons.size(); i++)
    {
        if (i == _selectedComponent)
            color = colorActive;
        else
            color = colorIdle;
        cv::drawContours(image, _componentButtons, i, color, -1, cv::LINE_AA);

        if (_componentSelections[i].size() > 0)
            cv::drawContours(image, _componentButtons, i, cv::Scalar(255,255,255), 2, cv::LINE_AA);

    }

    // Draw View-Button
    if (_buttonViewPressed)
        cv::circle(image, cv::Point(_buttonViewXPos, _buttonViewYPos), _buttonViewRadiusOut, colorActive, -1, cv::LINE_AA);
    else
        cv::circle(image, cv::Point(_buttonViewXPos, _buttonViewYPos), _buttonViewRadiusOut, colorIdle, -1, cv::LINE_AA);

    cv::circle(image, cv::Point(_buttonViewXPos, _buttonViewYPos), _buttonViewRadiusIn, colorBackground, -1, cv::LINE_AA);

    // Draw Buttons
    for (int i = 0; i < buttons.size(); i++)
    {
        cv::Scalar color;
        if (buttons[i].state == 1)
            color = colorActive;
        else
            color = colorIdle;
        cv::rectangle(image, cv::Rect(buttons[i].xpos, buttons[i].ypos, buttons[i].width, buttons[i].height), color, -1);
        cv::putText(image, buttons[i].title, cv::Point(buttons[i].xpos + 10, buttons[i].ypos + 31), cv::FONT_HERSHEY_DUPLEX, 0.7, cv::Scalar(0,0,0), 1, cv::LINE_AA);

    }

    return image;
}

void ComponentSegmentation::_rotateRight(cv::Mat &frame)
{
    cv::transpose(frame, frame);
    cv::flip(frame, frame, 1);
}

void ComponentSegmentation::CallBackFunc(int event, int x, int y, int flags, void* userdata)
{
    auto *self = static_cast<ComponentSegmentation*>(userdata);
    self->_doMouseCallback(event, x, y, flags);
}

void ComponentSegmentation::_doMouseCallback(int event, int x, int y, int flags)
{
    if (event == cv::EVENT_MOUSEMOVE)
    {
        _mouseX = x;
        _mouseY = y;
    }

    if (event == cv::EVENT_LBUTTONDOWN)
    {
        _mouseLDown = true;
        _updateSidebar = true;
        
        
        // Check if segment contour is clicked
        int result = 0;
        for (int i = 0; i < _componentButtons.size(); i++)
        {
            result = cv::pointPolygonTest(_componentButtons[i], cv::Point2f(x - 1500, y), false);

            if (result == 1)
            {
                _selectedComponent = i;
                _updateOverlay = true;
                _updateSidebar = true;
                break;
            }
        }

        for (int i = 0; i < buttons.size(); i++)
        {
            if (_checkMouseOverButton(buttons[i], x - _viewWidth, y))
            {
                buttons[i].state = 1;
                _updateSidebar = true;
            }
        }
        
    }
    else if (event == cv::EVENT_LBUTTONUP)
    {
        for (int i = 0; i < buttons.size(); i++)
        {
            if (_checkMouseOverButton(buttons[i], x - _viewWidth, y))
                buttons[i].state++;

            if (buttons[i].state == 2)
            {
                
                // Button Clear
                if (i == 0)
                {
                    _componentSelections[_selectedComponent].clear();
                }

                // Button Clear All
                else if (i == 1)
                {
                    for (int k = 0; k < _componentSelections.size(); k++)
                    {
                        _componentSelections[k].clear();
                    }

                }

                // Button Cancel
                else if (i == 2)
                {
                    _cancel = true;
                }

                // Button Finish
                else if (i == 3)
                {
                    _finish = true;
                }


                // Reset all Buttons
                for (int i = 0; i < buttons.size(); i++)
                {
                    buttons[i].state = 0;
                }
                _updateSidebar = true;
                _updateOverlay = true;

            }
        }

        _mouseLDown = false;
        _updateSidebar = true;
    }

    if (event == cv::EVENT_RBUTTONDOWN)
    {
        _mouseRDown = true;
    }
    else if (event == cv::EVENT_RBUTTONUP)
    {
        _mouseRDown = false;
    }
}
