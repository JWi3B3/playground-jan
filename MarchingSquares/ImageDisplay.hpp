#pragma once
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include <string>
#include <vector>

#define IMAGEDISPLAY_WINDOWHEIGHT 1440
#define IMAGEDISPLAY_WINDOWWIDTH  2560

class ImageDisplay
{
public:
    ImageDisplay();
    ~ImageDisplay();

    void                run(cv::Mat image, std::string title);
    static void         show(cv::Mat image, std::string title = "image");

private:
    cv::Mat             _resize(cv::Mat image);
    static void         _mouseCallback(int event, int x, int y, int mousewheelevent, void* userdata);
    void                _doMouseCallback(int event, int x, int y, int mousewheelevent);
    cv::Mat             _createInfoImage(cv::Mat originalimage);
    cv::Mat             _updateInfoImage(cv::Mat infoimage, int absX, int absY, std::vector<float> pixelvalue);
    std::string         _getImageType(int number);
    void                _drawHistogram(cv::Mat& infoimage, cv::Mat originalimage);
    std::vector<float>  _getPixelValue(cv::Mat image, int x, int y);
    void                _autoRange(cv::Mat& src, cv::Mat& dst, int rangeOn, int minpercent, int maxpercent);

    double              _viewX, _viewY, viewwidth, viewheight;
    int                 _zoom, _roiWidth, _roiHeight;
    int                 _heightstep, _nameborder, _valueborder;
    cv::Scalar          _colorHeader, _colorName, _colorValue;
    bool                _isLMouseDown, _isRMouseDown;
    int                 _mouseX, _mouseY;
    std::vector<double> _zoomfactors;
};

