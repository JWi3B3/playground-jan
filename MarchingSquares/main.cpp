#include <opencv2\opencv.hpp>
#include <iostream>
#include <vector>

#include "ImageDisplay.hpp"
#include "Timer.hpp"

void compareImages(cv::Mat image1, cv::Mat image2)
{
    cv::namedWindow("Slider");
    int blendvalue = 0;
    cv::createTrackbar("Blend", "Slider", &blendvalue, 1);

    cv::Mat blendimage;

    while (true)
    {

        cv::addWeighted(image1, 1- blendvalue, image2, blendvalue, 0.0, blendimage);

        cv::resize(blendimage, blendimage, blendimage.size() * 14, 0.0, 0.0, cv::INTER_NEAREST);

        cv::imshow("Image", blendimage);
        if (cv::waitKey(1) == 27)
            break;

    }
}

void fillBlackHoles(cv::Mat & image, unsigned int filtersize)
{
    filtersize = filtersize * 2 + 1;

    cv::Mat original = image.clone();
    cv::Mat result, mask, master, maskClone;

    cv::threshold(image, mask, 0.001, 1.0, CV_THRESH_BINARY);
    maskClone = mask.clone();
    mask.convertTo(mask, CV_32FC1);

    cv::GaussianBlur(image, image, cv::Size(filtersize, filtersize), 0);
    cv::GaussianBlur(mask, mask, cv::Size(filtersize, filtersize), 0);

    image.convertTo(image, CV_32FC1);
    cv::divide(image, mask, result);
    result.convertTo(result, CV_8UC1);

    maskClone = 1 - maskClone;
    multiply(maskClone, result, master);
    add(master, original, image);
}

void edgeSmooth(cv::Mat & image)
{
    cv::Mat kernel = cv::Mat::ones(3, 3, CV_8UC1);

    cv::Mat mask, mask2;

    cv::threshold(image, mask, 0, 1, CV_THRESH_BINARY);

    // Use maximum range of grayvalues
    image.convertTo(image, CV_64FC1);
    mask.convertTo(mask, CV_64FC1);
    image *= 63.7;

    cv::filter2D(image, image, -1, kernel);
    cv::filter2D(mask, mask2, -1, kernel);;

    image /= mask2;
    cv::multiply(image, mask, image);
    image.convertTo(image, CV_8UC1);
}


void edgeFilter(cv::Mat & image)
{
    cv::threshold(image, image, 0, 1, CV_THRESH_BINARY);

    cv::Mat kernel = cv::Mat::zeros(2, 2, CV_8UC1);
    kernel.at<uchar>(0, 0) = 1;
    kernel.at<uchar>(0, 1) = 2;
    kernel.at<uchar>(1, 0) = 4;
    kernel.at<uchar>(1, 1) = 8;

    cv::filter2D(image, image, -1, kernel, cv::Point(0, 0));

    cv::Mat lut = cv::Mat::zeros(1, 256, CV_8UC1);

    lut.at<uchar>(14) = 2;
    lut.at<uchar>(6) = 2;
    lut.at<uchar>(7) = 2;

    lut.at<uchar>(11) = 4;
    lut.at<uchar>(9) = 4;
    lut.at<uchar>(13) = 4;

    lut.at<uchar>(3) = 1;
    lut.at<uchar>(12) = 1;

    lut.at<uchar>(5) = 3;
    lut.at<uchar>(10) = 3;

    cv::LUT(image, lut, image);

    edgeSmooth(image);
}


cv::Mat shiftFrame(cv::Mat frame, int pixels, int direction)
{
    //create a same sized temporary Mat with all the pixels flagged as invalid (-1)
    cv::Mat temp = cv::Mat::zeros(frame.size(), frame.type());

    switch (direction)
    {
    case(0): // Shift up
        frame(cv::Rect(0, pixels, frame.cols, frame.rows - pixels)).copyTo(temp(cv::Rect(0, 0, temp.cols, temp.rows - pixels)));
        break;
    case(1): // Shift right
        frame(cv::Rect(0, 0, frame.cols - pixels, frame.rows)).copyTo(temp(cv::Rect(pixels, 0, frame.cols - pixels, frame.rows)));
        break;
    case(2): // Shift down
        frame(cv::Rect(0, 0, frame.cols, frame.rows - pixels)).copyTo(temp(cv::Rect(0, pixels, frame.cols, frame.rows - pixels)));
        break;
    case(3): // Shift left
        frame(cv::Rect(pixels, 0, frame.cols - pixels, frame.rows)).copyTo(temp(cv::Rect(0, 0, frame.cols - pixels, frame.rows)));
        break;
    default:
        std::cout << "Shift direction is not set properly" << std::endl;
    }

    return temp;
}

void closefilter(cv::Mat & image, int iterations = 1000)
{
    cv::Mat imageClone, mask;

    cv::imshow("image", image);
    cv::waitKey();

    int counter = 0;

    for (int i = 0; i < iterations; i++)
    {

        imageClone = shiftFrame(image, 1, counter);

        counter++;

        if (counter > 3)
            counter = 0;

        cv::threshold(image, mask, 0, 1, CV_THRESH_BINARY_INV);
        cv::multiply(imageClone, mask, imageClone);

        image += imageClone;
        
        cv::imshow("image", image);
        cv::waitKey(100);
    }
}


void blobFilter(cv::Mat & inOutImage, int minsize = 0)
{
    std::vector<std::vector<cv::Point> > contours;
    std::vector<cv::Vec4i> hierarchy;

    cv::Mat inImage = inOutImage.clone();

    cv::findContours(inImage, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

    for (int i = 0; i < contours.size(); i++)
    {
        // Loecher aussortieren
        if (hierarchy[i][3] < 0)
            continue;

        if (cv::contourArea(contours[i]) < minsize)
        {
            std::cout << "draw ";
            cv::drawContours(inOutImage, contours, i, 255, -1);
        }
    }
   
}

void rot90cw(cv::Mat & image, int count = 0)
{
    for (int i = 0; i < count; i++)
    {
        cv::transpose(image, image);
        cv::flip(image, image, 1);
    }
}

void printMarchingSquareNumbers(cv::Mat pattern)
{
    if (pattern.rows != 3 || pattern.cols != 3)
    {
        std::cout << "printMarchingSquareNumbers: wrong pattern size!" << std::endl;
        return;
    }

    if (pattern.type() != CV_16UC1)
        pattern.convertTo(pattern, CV_16UC1);

    cv::Mat pattern_mirror;
    cv::Mat multiplier = cv::Mat::zeros(3, 3, CV_16UC1);
    cv::Mat result;

    cv::flip(pattern, pattern_mirror, 0);

    multiplier.at<short>(0, 0) = 1;
    multiplier.at<short>(0, 1) = 2;
    multiplier.at<short>(0, 2) = 4;

    multiplier.at<short>(1, 0) = 8;
    multiplier.at<short>(1, 1) = 16;
    multiplier.at<short>(1, 2) = 32;

    multiplier.at<short>(2, 0) = 64;
    multiplier.at<short>(2, 1) = 128;
    multiplier.at<short>(2, 2) = 256;

    for (int i = 0; i < 4; i++)
    {
        cv::multiply(pattern, multiplier, result);
        std::cout << cv::sum(result)[0] << std::endl;

        cv::multiply(pattern_mirror, multiplier, result);
        std::cout << cv::sum(result)[0] << std::endl;

        rot90cw(pattern_mirror, 1);
        rot90cw(pattern, 1);
    }

    std::cout << std::endl;
}

cv::Mat curveFilter(cv::Mat & edgeImage)
{
    cv::threshold(edgeImage, edgeImage, 0, 1, CV_THRESH_BINARY);

    cv::Mat curveImage = cv::Mat::zeros(edgeImage.size(), CV_8UC1);

    uchar b;
    int imwidth = edgeImage.cols;
    int imheight = edgeImage.rows;
    int counter = 0;
    int grayval = 0;

    for (int y = 1; y < imheight - 1; y++)
    {
        uchar* pixel = edgeImage.ptr<uchar>(y, 0);
        for (int x = 1; x < imwidth - 1; x++)
        {
            if (int(*(pixel + x)) > 0)
            {

                counter = 16;
                counter += *(pixel + x - 1 - imwidth)   * 1;
                counter += *(pixel + x - imwidth)       * 2;
                counter += *(pixel + x + 1 - imwidth)   * 4;
                counter += *(pixel + x - 1)             * 8;
                counter += *(pixel + x + 1)             * 32;
                counter += *(pixel + x - 1 + imwidth)   * 64;
                counter += *(pixel + x + imwidth)       * 128;
                counter += *(pixel + x + 1 + imwidth)   * 256;

                switch (counter)
                {
                    case 146:   grayval = 1; break;
                    case 56:    grayval = 1; break;
                    case 273:   grayval = 1; break;
                    case 84:    grayval = 1; break;

                    case 274:   grayval = 2; break;
                    case 148:   grayval = 2; break;
                    case 112:   grayval = 2; break;
                    case 280:   grayval = 2; break;
                    case 145:   grayval = 2; break;
                    case 82:    grayval = 2; break;
                    case 28:    grayval = 2; break;
                    case 49:    grayval = 2; break;

                    case 50:    grayval = 3; break;
                    case 176:   grayval = 3; break;
                    case 152:   grayval = 3; break;
                    case 26:    grayval = 3; break;
                    case 21:    grayval = 3; break;
                    case 336:   grayval = 3; break;
                    case 276:   grayval = 3; break;
                    case 81:    grayval = 3; break;

                    case 22:    grayval = 4; break;
                    case 400:   grayval = 4; break;
                    case 304:   grayval = 4; break;
                    case 88:    grayval = 4; break;
                    case 208:   grayval = 4; break;
                    case 19:    grayval = 4; break;
                    case 25:    grayval = 4; break;
                    case 52:    grayval = 4; break;

                    default:    grayval = 0; break;
                }

                curveImage.at<char>(y, x) = grayval;
            }
        }
    }

    edgeSmooth(curveImage);
    return curveImage;
}

void removeSinglePixels(cv::Mat & image)
{
    cv::Mat binimage, mask;
    cv::threshold(image, binimage, 0, 1, CV_THRESH_BINARY);
    cv::Mat kernel = cv::Mat::zeros(3, 3, CV_8UC1);
    kernel.at<char>(1, 1) = 1;
    cv::matchTemplate(binimage, kernel, mask, CV_TM_SQDIFF_NORMED);
    mask.convertTo(mask, CV_8UC1, 255);
    cv::threshold(mask, mask, 1, 1, CV_THRESH_BINARY);
    cv::copyMakeBorder(mask, mask, 1, 1, 1, 1, CV_HAL_BORDER_CONSTANT, 1);
    cv::multiply(image, mask, image);
}



void main()
{
    cv::Mat image = cv::imread("img2.png", cv::IMREAD_GRAYSCALE);

    cv::Mat edgeimage = image.clone();

    edgeFilter(edgeimage);

    ImageDisplay::show(edgeimage);



    //calctime.stop();
}