
/**
* \file     MarkerDetection.hpp
*
* \brief    This class...
*
* \details  ...
*
* \author   Jan Wiebe (jan.wiebe@pdr-team.com)
* \date     00.00.2016
*
* \version  0.1.0
*
* \note     Copyright (c) 2016, PDR-Team GmbH
*/

#pragma once

#include <opencv2/opencv.hpp>
#include <vector>
#include <string>

#define TEACHED_PATTERNS 60

class Marker
{
public:
    Marker();
    Marker(int id, double xpos, double ypos);
    ~Marker();

    double xpos, ypos;
    int id;
    double depth;
};

class MarkerDetection
{
public:
    MarkerDetection();
    ~MarkerDetection();

    std::vector<Marker> findMarkers(cv::Mat grayimage, double thresh, bool debugmode, const bool mirror, bool useAdaptiveThreshold=false);
    cv::Mat getDebugImage();
    cv::Mat drawMarkers(cv::Mat& image, std::vector<Marker>& markers);

private:


    std::vector<cv::Mat> teached_patterns;
    void rotateimage90(cv::Mat& image, int angle = 1);
    cv::Mat scaleimage(cv::Mat image, double factor);
    cv::Mat autoscale(cv::Mat& image, int opencvtype = CV_8U, double minimum = -1, double maximum = -1);
    void showimage(cv::Mat& image, std::string title = "ImageViewer");
    std::string cvtype2str(int type);

    double differenceOfImages(cv::Mat image1, cv::Mat image2);

    cv::Mat _displayimage;
};


