#include <opencv2\opencv.hpp>
#include <opencv2\viz.hpp>
#include <opencv2\calib3d.hpp>
#include <chrono>
#include <thread>
#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <pcl\common\transforms.h>
#include <Eigen\src\Core\Array.h>

#include "OrbbecDevice.hpp"
#include "MarkerDetection.hpp"
#include "ImageDisplay.hpp"


std::vector<std::vector<cv::Point2f>> getMarkers(std::vector<std::shared_ptr<OrbbecDevice>> camlist)
{
    auto md = MarkerDetection();

    // IR-Streams starten und Projektor ausschalten
    for (int i = 0; i < camlist.size(); i++)
    {
        camlist[i]->startStream(OrbbecDevice::STREAMTYPE_IR);
        camlist[i]->setEmitter(false);
    }

    std::vector<std::vector<cv::Point2f>> markerpoints;
    std::vector<std::vector<cv::Point2f>> markerpointsTrusted;

    std::vector<std::vector<Marker>> markers;

    std::vector<cv::Mat> images;

    while (true)
    {
        markers.clear();
        images.clear();

        for (int i = 0; i < camlist.size(); i++)
        {
            images.push_back(cv::Mat().clone());
            camlist[i]->getFrame(images[i], 100);

            cv::imshow("original" + std::to_string(i), images[i]);


            // Markerdetection
            markers.push_back(md.findMarkers(images[i], 128, false, false, false));
            images[i] = md.getDebugImage();

            cv::imshow("debugimage" + std::to_string(i), images[i]);

        }

        std::map <int, std::vector<cv::Point2f>> sortedIDs;

        // Nach IDs sortieren
        // Ueber alle Kameras
        for (int i = 0; i < markers.size(); i++)
        {
            // Ueber alle gefundenen Marker
            for (int j = 0; j < markers[i].size(); j++)
            {
                // Neue ID anlegen
                if (sortedIDs.find(markers[i][j].id) == sortedIDs.end())
                {
                    std::vector<cv::Point2f> newvec {cv::Point2f(markers[i][j].xpos, markers[i][j].ypos)};
                    sortedIDs[markers[i][j].id] = newvec;
                }
                // ID erweitern
                else
                    sortedIDs[markers[i][j].id].push_back(cv::Point2f(markers[i][j].xpos, markers[i][j].ypos));
            }
        }
  
        markerpoints.clear();
        for (auto const& id : sortedIDs)
        {
            if (id.second.size() == camlist.size())
            {
                markerpoints.push_back(id.second);
            }
        }

        if (markerpoints.size() >= 3)
            markerpointsTrusted = markerpoints;

        if (cv::waitKey(1) == 27)
            break;
    }

    // Streams stoppen
    for (int i = 0; i < camlist.size(); i++)
    {
        camlist[i]->setEmitter(true);
        camlist[i]->stopStream();
    }

    return markerpointsTrusted;
}

std::vector<std::vector<cv::Point3f>> getMarkerPosition3D(std::vector<std::shared_ptr<OrbbecDevice>> camlist, std::vector<std::vector<cv::Point2f>> markerpositions)
{
    std::vector<cv::Mat> images;
    std::vector<std::vector<cv::Point3f>> marker3DPosition;
    int roiSize = 10;
    double meanval = 0;
    cv::Mat markerimage, mask;
    cv::Mat displayimage;

    cv::Point3f point3d;

    // Depth-Streams starten und Projektor ausschalten
    for (int i = 0; i < camlist.size(); i++)
    {
        camlist[i]->startStream(OrbbecDevice::STREAMTYPE_DEPTH);
        camlist[i]->setEmitter(true);
    }

    while (true)
    {
        images.clear();

        marker3DPosition.clear();

        for (int i = 0; i < camlist.size(); i++)
        {

            std::vector<cv::Point3f> campoints;

            images.push_back(cv::Mat().clone());
            camlist[i]->getFrame(images[i], 100);

            images[i].convertTo(displayimage, CV_8UC1, 255. / 5000);


            cv::cvtColor(displayimage, displayimage, cv::COLOR_GRAY2BGR);

            for (int j = 0; j < markerpositions.size(); j++)
            {
                auto markerRoi = cv::Rect(markerpositions[j][i].x - roiSize / 2, markerpositions[j][i].y - roiSize / 2, roiSize, roiSize);
                markerimage = images[i](markerRoi).clone();
                mask = markerimage > 0;
                mask.convertTo(mask, CV_8UC1);
                meanval = cv::mean(markerimage, mask)[0];

                point3d = camlist[i]->convert2DPointTo3DPoint(markerpositions[j][i].x, markerpositions[j][i].y, meanval);

                campoints.push_back(point3d);

                cv::rectangle(displayimage, markerRoi, cv::Scalar(0, 255, 0));
                cv::putText(displayimage, std::to_string(meanval), cv::Point(markerRoi.x + markerRoi.width + 5, markerRoi.y), CV_FONT_HERSHEY_PLAIN, 1, cv::Scalar(0, 255, 0));
            }

            marker3DPosition.push_back(campoints);

            cv::imshow("image " + std::to_string(i), displayimage);

        }

        if (cv::waitKey(1) == 27)
            break;
    }

    // Streams stoppen
    for (int i = 0; i < camlist.size(); i++)
    {
        camlist[i]->setEmitter(true);
        camlist[i]->stopStream();
    }

    return marker3DPosition;
}

cv::Point3f calcCenterPoint(std::vector<cv::Point3f> points)
{
    cv::Point3f center(0,0,0);

    for (int i = 0; i < points.size(); i++)
        center += points[i];

    center /= static_cast<int>(points.size());

    return center;
}

Eigen::Matrix4f calcRotationMatrixTwoVectors(cv::Point3f vec1, cv::Point3f vec2)
{
    auto rotationAxis = vec1.cross(vec2);
    rotationAxis /= cv::norm(rotationAxis);

    Eigen::Vector3f rotationAxisEigen;
    rotationAxisEigen(0) = rotationAxis.x;
    rotationAxisEigen(1) = rotationAxis.y;
    rotationAxisEigen(2) = rotationAxis.z;

    float rotationAngle = std::acos(vec1.dot(vec2) / (cv::norm(vec1) * cv::norm(vec2)));

    Eigen::Matrix3f rotationMatrix;
    rotationMatrix = Eigen::AngleAxisf(rotationAngle, rotationAxisEigen);
  
    Eigen::Matrix4f rotationMatrix4f = Eigen::Matrix4f::Identity();
    
    for (int x = 0; x < 3; x++)
    {
        for (int y = 0; y < 3; y++)
        {
            rotationMatrix4f(x, y) = rotationMatrix(x, y);
        }
    }

    return rotationMatrix4f;
}

void transformPoints(std::vector<cv::Point3f> & markerPositions, Eigen::Matrix4f transformationMatrix)
{
    Eigen::Vector4f oldpoint, newpoint;
    
    for (int i = 0; i < markerPositions.size(); i++)
    {
        oldpoint(0) = markerPositions[i].x;
        oldpoint(1) = markerPositions[i].y;
        oldpoint(2) = markerPositions[i].z;
        oldpoint(3) = 1;

        newpoint = transformationMatrix * oldpoint;
        
        markerPositions[i].x = newpoint(0);
        markerPositions[i].y = newpoint(1);
        markerPositions[i].z = newpoint(2);
    }
}

std::vector<Eigen::Matrix4f> fitCameras(std::vector<std::vector<cv::Point3f>> markerPositions)
{
    std::vector<Eigen::Matrix4f> cameratransforms;


    // Erste Kamera als Basiskamera verwenden
    auto centerCam1 = calcCenterPoint(markerPositions[0]);
    auto directionCam1 = markerPositions[0][0] - centerCam1;


    // Ueber alle Kameras, von der zweiten an
    for (int i = 1; i < markerPositions.size(); i++)
    {
        cameratransforms.push_back(Eigen::Matrix4f::Identity());

        // Schwerpunkt berechnen
        auto centerCurrentCam = calcCenterPoint(markerPositions[i]);

        // Verschiebung berechnen
        cameratransforms[i](0, 3) = centerCurrentCam.x - centerCam1.x;
        cameratransforms[i](1, 3) = centerCurrentCam.y - centerCam1.y;
        cameratransforms[i](2, 3) = centerCurrentCam.z - centerCam1.z;

        // Richtungswinkel für ein Punkt bestimmen
        auto direction = markerPositions[i][0] - centerCurrentCam;
        direction /= cv::norm(direction);

        // Erste Rotation
        auto rot1 = calcRotationMatrixTwoVectors(direction, directionCam1);

        // Translation und Rotation auf die Markerpositionen anwenden


        // Rotation um die Achse der ersten Punktewolke
    }

    return cameratransforms;
}

void saveMarkerPositions(std::vector<std::vector<cv::Point3f>> markerPositions)
{
    cv::FileStorage fs("markerpositions.yml", cv::FileStorage::WRITE);

    fs << "cameracount" << static_cast<int>(markerPositions.size());
    fs << "markercount" << static_cast<int>(markerPositions[0].size());

    for (int i = 0; i < markerPositions.size(); i++)
    {
        for (int j = 0; j < markerPositions[i].size(); j++)
        {
            fs << "point" + std::to_string(i) + std::to_string(j) << markerPositions[i][j];
        }
    }

    fs.release();
}

std::vector<std::vector<cv::Point3f>> loadMarkerPositions()
{
    std::vector<std::vector<cv::Point3f>> markerPositions;
    cv::FileStorage fs("markerpositions.yml", cv::FileStorage::READ);

    int cameracount = 0, markercount = 0;
    fs["cameracount"] >> cameracount;
    fs["markercount"] >> markercount;

    std::vector<cv::Point3f> oneCam;
    for (int i = 0; i < cameracount; i++)
    {
        oneCam.clear();

        for (int j = 0; j < markercount; j++)
        {
            oneCam.push_back(cv::Point3f());
            fs["point" + std::to_string(i) + std::to_string(j)] >> oneCam[j];
        }

        markerPositions.push_back(oneCam);
    }

    return markerPositions;
}

void main()
{
    int cameraCount = 2;

    //// 3D Kameras initialisieren
    //OrbbecDevice::initialize();
    //std::vector<std::shared_ptr<OrbbecDevice>> camlist;

    //for (int i = 0; i < cameraCount; i++)
    //{
    //    camlist.push_back(OrbbecDevice::create());
    //    std::cout << "Cam " << i << ":" << camlist[i]->isOpen() << std::endl;
    //}

    //// Markerdetection im Infrarotbild
    //auto markerpositions = getMarkers(camlist);

    //// Bestimmung der 3D-Position der Marker
    //auto marker3Dposition = getMarkerPosition3D(camlist, markerpositions);

    //saveMarkerPositions(marker3Dposition);

    auto markerPositions = loadMarkerPositions();

    for (int i = 0; i < markerPositions.size(); i++)
    {
        std::cout << "Camera " << i << std::endl;

        for (int j = 0; j < markerPositions[i].size(); j++)
        {
            std::cout << markerPositions[i][j].x << ", " << markerPositions[i][j].y << ", " << markerPositions[i][j].z << ", " << std::endl;
        }
    }

    // Berechnung der Transformationen der verschiedenen Kameras

    std::cin.ignore();
    // Test-Livebild der zusammengefügten Punktewolken
}