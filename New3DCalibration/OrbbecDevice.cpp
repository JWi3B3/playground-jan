#pragma once

/**
* \file     OrbbecDevice.cpp
*
* \brief    Implementation of OrbbecDevice.hpp
*
* \author   Timo Blust (timo.blust@pdr-team.com)
* \date     23.09.2016
*
* \version  0.1.0
*
* \note     Copyright (c) 2016, PDR-Team GmbH
*/


#include "OrbbecDevice.hpp"

std::vector<std::weak_ptr<OrbbecDevice>> OrbbecDevice::openDevices = std::vector<std::weak_ptr<OrbbecDevice>>();

OrbbecDevice::OrbbecDevice()
{
    _usbHandle = nullptr;
    _camTag = 0;
    _stream = nullptr;
    _uri = "";
    _latestFrameTime = clock();
    _readingFrame = false;
    _hasNewFrame = false;
    _emitter = true;
    _hardwareDisconnected = false;
}

OrbbecDevice::OrbbecDevice(const char* uri) : OrbbecDevice()
{
    _device = std::make_shared<openni::Device>();
    _device->open(uri);
    xnUSBOpenDeviceByPath(uri, &_usbHandle);

    _uri = uri;
    _serial = OrbbecDevice::getSerialNumber();

    //openni::OpenNI::addDeviceConnectedListener(this);
    openni::OpenNI::addDeviceDisconnectedListener(this);

    // Set to mirror
    this->setMirror(false);
}


OrbbecDevice::~OrbbecDevice()
{
    if (!OrbbecDevice::isOpen())
        return;

    if (isStreamOpen())
        stopStream();

    //openni::OpenNI::removeDeviceConnectedListener(this);
    openni::OpenNI::removeDeviceDisconnectedListener(this);

    xnUSBCloseDevice(_usbHandle);
    _device->close();
}


void OrbbecDevice::initialize()
{
    openni::OpenNI::initialize();
    xnUSBInit();
}


void OrbbecDevice::shutdown()
{
    xnUSBShutdown();
    openni::OpenNI::shutdown();
}


std::string OrbbecDevice::getSerialNumber() const
{
    if (!_isValid())
        return "";

    char data[12];
    int size = sizeof data;
    _device->getProperty(3, &data, &size);
    return data;
}

std::string OrbbecDevice::getUri() const
{
    return _uri;
}

bool OrbbecDevice::isOpen() const
{
    return !_hardwareDisconnected && _isValid();
}

bool OrbbecDevice::startStream(StreamType streamType)
{
    if (_stream)
        stopStream();

    if (!_isValid() || streamType == STREAMTYPE_NONE)
        return false;

    _stream = std::make_shared<openni::VideoStream>();
    _stream->create(*_device, static_cast<openni::SensorType>(streamType));

    if (streamType == ONI_SENSOR_IR)
    {
        openni::VideoMode vm;
        vm.setResolution(640, 480);
        vm.setFps(30);
        vm.setPixelFormat(openni::PIXEL_FORMAT_GRAY16);
        _stream->setVideoMode(vm);
    }

    _stream->start();
    _stream->addNewFrameListener(this);
    return true;
}

void OrbbecDevice::stopStream()
{
    if (!_isValid())
        return;

    _stream->stop();
    _stream->removeNewFrameListener(this);
    _stream.reset();
}

openni::VideoFrameRef OrbbecDevice::getFrame(int timeout)
{
    openni::VideoFrameRef out;

    if (_hardwareDisconnected || !_isValid())
        return out;

    clock_t startTime = clock();

    while ((_readingFrame || !_hasNewFrame) &&
        (clock() - startTime < timeout * CLOCKS_PER_SEC / 1000 || timeout == -1))
    { 	}

    if (_hasNewFrame)
    {
        _hasNewFrame = false;
        return _latestFrame;
    }

    //checkHardwareConnection();
    return out;
}

bool OrbbecDevice::getFrame(cv::Mat& mat, int timeout)
{
    auto frame = getFrame(timeout);
    if (frame.isValid())
    {
        convertFrameToMat(frame, mat);
        return true;
    }

    return false;
}

bool OrbbecDevice::hasFrame() const
{
    return _hasNewFrame;
}

void OrbbecDevice::setEmitter(bool emitterEnabled)
{
    if (!_isValid())
        return;

    char sendBuf[10];
    char recBuf[10];
    *reinterpret_cast<uint16_t*>(sendBuf) = emitterEnabled ? 1 : 0;

    sendCmd(85, sendBuf, 2, recBuf, 2);
    _emitterSetTime = clock();
    _emitter = emitterEnabled;
}

bool OrbbecDevice::getEmitter() const
{
    if (!_isValid())
        return false;

    return _emitter;
}

void OrbbecDevice::setEmitterFrameDelay(int delay)
{
    _frameDelay = max(delay * CLOCKS_PER_SEC / 1000, 0L);
}

int OrbbecDevice::getEmitterFrameDelay() const
{
    return _frameDelay * 1000 / CLOCKS_PER_SEC;
}

void OrbbecDevice::setMirror(bool mirrorEnabled)
{
    if (!_isValid())
        return;

    char data = mirrorEnabled ? 1 : 0;
    _device->setProperty(276885506, &data, sizeof(data));
}

bool OrbbecDevice::getMirror() const
{
    if (!_isValid())
        return false;

    char data;
    int dataSize = 1;
    _device->getProperty(276885506, &data, &dataSize);
    return data;
}

bool OrbbecDevice::isStreamOpen() const
{
    return isOpen() && _stream && _stream->isValid();
}

//bool OrbbecDevice::checkHardwareConnection()
//{
//	if (_hardwareDisconnected)
//		return false;
//
//	openni::Array<openni::DeviceInfo> devices;
//	openni::OpenNI::enumerateDevices(&devices);
//
//	for (auto i = 0; i < devices.getSize(); i++)
//		if (devices[i].getUri() == _uri)
//			return true;
//
//	_hardwareDisconnected = true;
//	return !_hardwareDisconnected;
//}

OrbbecDevice::StreamType OrbbecDevice::getStreamType() const
{
    if (!_stream)
        return STREAMTYPE_NONE;

    return static_cast<StreamType>(_stream->getSensorInfo().getSensorType());
}

//bool OrbbecDevice::reconnect()
//{
//	_device.reset();
//
//	openni::Array<openni::DeviceInfo> devices;
//	openni::OpenNI::enumerateDevices(&devices);
//
//	for (auto i = 0; i < devices.getSize(); i++)
//	{
//		char data[12];
//		int size = sizeof data;
//		_device->getProperty(3, &data, &size);
//
//		if (_serial == data)
//		{
//			_device = std::make_shared<openni::Device>();
//			_uri = devices[i].getUri();
//			_device->open(_uri.c_str());
//
//			bool running = _stream->isValid();
//			auto type = _stream->getSensorInfo().getSensorType();
//			_stream.reset();
//
//			if (running)
//				startStream(type);
//
//			return true;
//		}
//	}
//
//	return false;
//}


std::shared_ptr<OrbbecDevice> OrbbecDevice::create()
{
    openni::Array<openni::DeviceInfo> devices;
    openni::OpenNI::enumerateDevices(&devices);
    int idx;
    bool isFree = false;

    for (idx = 0; idx < devices.getSize(); idx++)
    {
        isFree = true;

        for (auto j = 0; j < openDevices.size(); j++)
        {
            if (auto ptr = openDevices[j].lock())
            {
                // check if URI is already used
                isFree &= devices[idx].getUri() != ptr->getUri();
            }
            else
            {
                // delete weak ref if expired
                openDevices.erase(openDevices.begin() + j);
                j--;
            }
        }

        if (isFree)
            break;
    }

    if (!isFree)
    {
        return std::make_shared<OrbbecDevice>();
    }

    auto instance = std::make_shared<OrbbecDevice>(devices[idx].getUri());
    openDevices.push_back(instance);
    return instance;
}

void OrbbecDevice::convertFrameToMat(openni::VideoFrameRef& frame, cv::Mat& mat)
{
    auto h = frame.getHeight();
    auto w = frame.getWidth();
    auto s = frame.getStrideInBytes();
    auto frameData = frame.getData();

    switch (frame.getSensorType())
    {
        case openni::SENSOR_IR:
        {
            mat.create(h, w, CV_16UC1);
            memcpy(mat.data, frameData, h * s);
            mat.convertTo(mat, CV_8UC1, 255.0 / 1024);
            break;
        }

        case openni::SENSOR_DEPTH:
        {
            mat.create(h, w, CV_16UC1);
            memcpy(mat.data, frameData, h * s);
            break;
        }

        //todo untested experimental code
        case openni::SENSOR_COLOR:
        {
            mat.create(h, w, CV_8UC3);
            memcpy(mat.data, frameData, h * s);
            break;
        }
    }
}

void OrbbecDevice::convertMatToWorld(cv::Mat & depthframe, cv::Mat & cloud, int stepsize)
{
    cloud = cv::Mat::zeros(depthframe.rows * depthframe.cols, 1, CV_32FC3);

    for (int y = 0; y < depthframe.rows; y += stepsize)
    {
        for (int x = 0; x < depthframe.cols; x += stepsize)
        {
            if (depthframe.at<ushort>(y, x) != 0)
            {
                openni::CoordinateConverter::convertDepthToWorld(*_stream.get(),
                    float(x), float(y), float(depthframe.at<short>(y, x)),
                    &cloud.at<cv::Vec3f>(y * depthframe.cols + x)[0],
                    &cloud.at<cv::Vec3f>(y * depthframe.cols + x)[1],
                    &cloud.at<cv::Vec3f>(y * depthframe.cols + x)[2]);
            }
        }
    }
}

cv::Point3f OrbbecDevice::convert2DPointTo3DPoint(double xpos, double ypos, double depth)
{
    auto point3d = cv::Point3f();

    openni::CoordinateConverter::convertDepthToWorld(*_stream.get(),
        float(xpos), float(ypos), float(depth),
        &point3d.x,
        &point3d.y, 
        &point3d.z);

    return point3d;
}

void OrbbecDevice::onNewFrame(openni::VideoStream& stream)
{
    _readingFrame = true;
    _hasNewFrame = false;
    _latestFrameTime = clock();
    stream.readFrame(&_latestFrame);
    _hasNewFrame = (_latestFrameTime - _emitterSetTime) > _frameDelay;
    _readingFrame = false;
}

//void OrbbecDevice::onDeviceConnected(const openni::DeviceInfo* device)
//{
//	if (!_hardwareDisconnected)
//		return;
//
//	std::cout << "device connected... ";
//
//	auto uri = device->getUri();
//
//	char data[12];
//	int size = sizeof data;
//	openni::Device cam;
//
//	if (cam.open(uri) == openni::STATUS_OK)
//	{
//		cam.getProperty(3, &data, &size);
//		cam.close();
//		std::cout << "serial is " << data << "... ";
//	}
//	else
//	{
//		std::cout << "couldn't open." << std::endl;
//		return;
//	}
//
//	if (_serial == data)
//	{
//		std::cout << "serial matches... ";
//
//		_device = std::make_shared<openni::Device>();
//		_uri = uri;
//		_device->open(uri);
//		xnUSBOpenDeviceByPath(uri, &_usbHandle);
//
//		bool running = _stream->isValid();
//		auto type = _stream->getSensorInfo().getSensorType();
//		_stream.reset();
//
//		if (running)
//			startStream(type);
//
//		_hardwareDisconnected = false;
//		std::cout << "successfully connected." <<std::endl;
//	}
//}

void OrbbecDevice::onDeviceDisconnected(const openni::DeviceInfo* device)
{
    if (device->getUri() == _uri)
    {
        _hardwareDisconnected = true;

        if (isStreamOpen())
            stopStream();

        xnUSBCloseDevice(_usbHandle);
        //_device->close();
        //_device.reset();

        std::cout << "device (s.n. " << _serial << ") disconnected." << std::endl;
    }
}

openni::Device* OrbbecDevice::getDevice() const
{
    return _device.get();
}

int OrbbecDevice::sendCmd(uint16_t cmd, void* cmdbuf, uint16_t cmd_len, void* replybuf, uint16_t reply_len)
{
    int res;
    unsigned int actual_len;
    uint8_t obuf[0x400];
    uint8_t ibuf[0x200];

    cam_hdr *chdr = reinterpret_cast<cam_hdr*>(obuf);
    cam_hdr *rhdr = reinterpret_cast<cam_hdr*>(ibuf);

    if (_usbHandle == nullptr)
        return -1;

    if (cmd_len & 1 || cmd_len > (0x400 - sizeof(*chdr)))
        return -1;


    chdr->magic[0] = 0x47;
    chdr->magic[1] = 0x4d;
    chdr->cmd = cmd;
    chdr->tag = _camTag;
    chdr->len = cmd_len / 2;
    //chdr->aa = 0x0010;

    //copy the cmdbuf
    memcpy(obuf + sizeof(*chdr), cmdbuf, cmd_len);


    res = xnUSBSendControl(_usbHandle, XN_USB_CONTROL_TYPE_VENDOR, 0x00, 0x0000, 0x0000, (XnUChar*)obuf, cmd_len + sizeof(*chdr), 5000);
    if (res < 0)
    {
        printf_s("send_cmd: Output control transfer failed (%d)\n", res);
        return res;
    }

    do
    {
        xnUSBReceiveControl(_usbHandle, XN_USB_CONTROL_TYPE_VENDOR, 0x00, 0x0000, 0x0000, (XnUChar *)ibuf, 0x200, &actual_len, 5000);
        //print_dbg("send_cmd: actual length = %d\n", actual_len);

    } while ((actual_len == 0) || (actual_len == 0x200));

    //print_dbg("Control reply: %d\n", res);
    if (actual_len < (int)sizeof(*rhdr)) {
        printf_s("send_cmd: Input control transfer failed (%d)\n", res);
        return res;
    }
    actual_len -= sizeof(*rhdr);

    if (rhdr->magic[0] != 0x52 || rhdr->magic[1] != 0x42) {
        printf_s("send_cmd: Bad magic %02x %02x\n", rhdr->magic[0], rhdr->magic[1]);
        return -1;
    }
    if (rhdr->cmd != chdr->cmd) {
        printf_s("send_cmd: Bad cmd %02x != %02x\n", rhdr->cmd, chdr->cmd);
        return -1;
    }
    if (rhdr->tag != chdr->tag) {
        printf_s("send_cmd: Bad tag %04x != %04x\n", rhdr->tag, chdr->tag);
        return -1;
    }
    if (rhdr->len != (actual_len / 2)) {
        printf_s("send_cmd: Bad len %04x != %04x\n", rhdr->len, (int)(actual_len / 2));
        return -1;
    }

    if (actual_len > reply_len) {
        printf_s("send_cmd: Data buffer is %d bytes long, but got %d bytes\n", reply_len, actual_len);
        memcpy(replybuf, ibuf + sizeof(*rhdr), reply_len);
    }
    else {
        memcpy(replybuf, ibuf + sizeof(*rhdr), actual_len);
    }

    _camTag++;

    return actual_len;
}

bool OrbbecDevice::_isValid() const
{
    return _device && _device->isValid();
}
