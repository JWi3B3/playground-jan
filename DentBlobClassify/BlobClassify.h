#pragma once
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <vector>

class BlobClassify
{
public:
    BlobClassify();
    ~BlobClassify();

    void setRoundness(double minval, double maxval);
    void setBoundingSize(double width_min, double width_max, double height_min, double height_max);
    void setArea(double minarea, double maxarea);
    std::vector<cv::Point> classifyBlobs(cv::Mat binimage);

private:
    double _roundness_min, _roundness_max,
        _area_min, _area_max,
        _width_min, _width_max,
        _height_min, _height_max;
};

