#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <Helpers.h>
#include <Slider.h>
#include "BlobClassify.h"

using namespace cv;
using namespace std;


void main()
{
    Slider slider_thresh = Slider("Settings", "Threshold", 0, 1, 0.5, 500);
    Slider slider_closing = Slider("Settings", "Closing", 0, 99, 0, 100);

    Slider slider_minarea = Slider("Settings2", "Min Area", 0, 15000, 0, 1000);
    Slider slider_maxarea = Slider("Settings2", "Max Area", 0, 15000, 15000, 1000);
    Slider slider_mincirc = Slider("Settings2", "Min Circ", 0, 1, 0, 500);
    Slider slider_maxcirc = Slider("Settings2", "Max Circ", 0, 1, 1, 500);
    Slider slider_minwidth = Slider("Settings2", "Min Width", 0, 199, 0, 200);
    Slider slider_maxwidth = Slider("Settings2", "Max Width", 0, 199, 199, 200);
    Slider slider_minheight = Slider("Settings2", "Min Height", 0, 199, 0, 200);
    Slider slider_maxheight = Slider("Settings2", "Max Height", 0, 199, 199, 200);



    Mat image = imread("image.png", IMREAD_GRAYSCALE);

    // Flowfilter
    Mat temp;
    vector<Mat> channels(6);
    cv::cornerEigenValsAndVecs(image, temp, 25, 3);
    split(temp, channels);

    Mat filterimage = channels[1];
    Mat binimage, dispimage;

    BlobClassify blob;

    while (true)
    {
        double minval, maxval;
        minMaxIdx(filterimage, &minval, &maxval);

        // Binarisieren
        threshold(filterimage, binimage, slider_thresh.getValue() * maxval * 0.1, 255, CV_THRESH_BINARY);
        binimage.convertTo(binimage, CV_8UC1);

        // Morphologie
        dilate(binimage, binimage, Mat::ones(3, 3, CV_8UC1), Point(-1, -1), static_cast<int>(slider_closing.getValue()));
        erode(binimage, binimage, Mat::ones(3, 3, CV_8UC1), Point(-1, -1), static_cast<int>(slider_closing.getValue()));

        blob.setArea(slider_minarea.getValue(), slider_maxarea.getValue());
        blob.setBoundingSize(slider_minwidth.getValue(), slider_maxwidth.getValue(), slider_minheight.getValue(), slider_maxheight.getValue());
        blob.setRoundness(slider_mincirc.getValue(), slider_maxcirc.getValue());
        //Blob-Analyse
        blob.classifyBlobs(binimage);

        // Darstellung
        dispimage = Helpers::scaleimage(binimage, 0.5);
        imshow("filterimage", dispimage);
        if (waitKey(1) == 27) break;
    }
}