#define _USE_MATH_DEFINES

#include "BlobClassify.h"
#include <math.h>

using namespace cv;
using namespace std;

BlobClassify::BlobClassify() :
    _area_min(0), _area_max(100000),
    _roundness_min(0), _roundness_max(1),
    _width_min(1), _width_max(10000),
    _height_min(1), _height_max(10000)
{
}

BlobClassify::~BlobClassify()
{
}

void BlobClassify::setRoundness(double minval, double maxval)
{
    _roundness_min = minval;
    _roundness_max = maxval;
}

void BlobClassify::setBoundingSize(double width_min, double width_max, double height_min, double height_max)
{
    _width_min = width_min;
    _width_max = width_max;
    _height_min = height_min;
    _height_max = height_max;
}

void BlobClassify::setArea(double minarea, double maxarea)
{
    _area_min = minarea;
    _area_max = maxarea;
}

std::vector<cv::Point> BlobClassify::classifyBlobs(cv::Mat binimage)
{
    Mat dispimage;
    cvtColor(binimage, dispimage, COLOR_GRAY2BGR);

    vector<vector<Point>> contours;
    vector<Vec4i> hierarchy;
    findContours(binimage, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_TC89_KCOS);

    double area = 0;
    Rect boundingbox;
    double perimeter;
    double roundness;
    Moments mu;
    Point masscenter;

    int textstep = 14;

    for (int i = 0; i < contours.size(); i++)
    {
        // Blob-Eigenschaften ermitteln
        if (hierarchy[i][3] == -1)
        {
            area = contourArea(contours[i]);
            boundingbox = boundingRect(contours[i]);
            perimeter = arcLength(contours[i], true);
            roundness = area / (perimeter*perimeter / (2 * M_PI));

            mu = moments(contours[i], true);
            masscenter = Point2f(static_cast<float>(mu.m10 / mu.m00), static_cast<float>(mu.m01 / mu.m00));

            // Abfragen, ob Werte innerhalb der Toleranz sind
            if (area >= _area_min && area <= _area_max
                && boundingbox.width >= _width_min && boundingbox.width <= _width_max
                && boundingbox.height >= _height_min && boundingbox.height <= _height_max
                && roundness >= _roundness_min && roundness <= _roundness_max)
            {
                drawContours(dispimage, contours, i, Scalar(0, 255, 0));
                putText(dispimage, "A " + to_string(static_cast<int>(area)), masscenter + Point(10, -2 * textstep), CV_FONT_HERSHEY_PLAIN, 1, Scalar(0, 255, 0));
                putText(dispimage, "w " + to_string(static_cast<int>(boundingbox.width)), masscenter + Point(10, -1 * textstep), CV_FONT_HERSHEY_PLAIN, 1, Scalar(0, 255, 0));
                putText(dispimage, "h " + to_string(static_cast<int>(boundingbox.height)), masscenter + Point(10, 0), CV_FONT_HERSHEY_PLAIN, 1, Scalar(0, 255, 0));
                putText(dispimage, "c " + to_string(roundness), masscenter + Point(10, textstep), CV_FONT_HERSHEY_PLAIN, 1, Scalar(0, 255, 0));
            }
        }
    }

    Scalar textcolor = Scalar(255, 255, 0);
    putText(dispimage, "min A " + to_string(static_cast<int>(_area_min)), Point(10, textstep), CV_FONT_HERSHEY_PLAIN, 1, textcolor);
    putText(dispimage, "max A " + to_string(static_cast<int>(_area_max)), Point(10, 2 * textstep), CV_FONT_HERSHEY_PLAIN, 1, textcolor);
    putText(dispimage, "min w " + to_string(static_cast<int>(_width_min)), Point(10, 3 * textstep), CV_FONT_HERSHEY_PLAIN, 1, textcolor);
    putText(dispimage, "max w " + to_string(static_cast<int>(_width_max)), Point(10, 4 * textstep), CV_FONT_HERSHEY_PLAIN, 1, textcolor);
    putText(dispimage, "min h " + to_string(static_cast<int>(_height_min)), Point(10, 5 * textstep), CV_FONT_HERSHEY_PLAIN, 1, textcolor);
    putText(dispimage, "max h " + to_string(static_cast<int>(_height_max)), Point(10, 6 * textstep), CV_FONT_HERSHEY_PLAIN, 1, textcolor);
    putText(dispimage, "min c " + to_string(_roundness_min), Point(10, 7 * textstep), CV_FONT_HERSHEY_PLAIN, 1, textcolor);
    putText(dispimage, "max c " + to_string(_roundness_max), Point(10, 8 * textstep), CV_FONT_HERSHEY_PLAIN, 1, textcolor);

    imshow("blob analysis", dispimage);

    return std::vector<cv::Point>();
}
