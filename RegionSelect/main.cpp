#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <vector>
#include <iostream>
#include <map>

#define WINDOWHEIGHT 1080      
#define WINDOWWIDTH 1920

#define RED 185
#define GREEN 251
#define BLUE 146

using namespace cv;
using namespace std;


bool isLMouseDown = false;
bool isRMouseDown = false;
int mouse_x = 0, mouse_y = 0;
int xView = 0, yView = 0;

void mouseCallback(int event, int x, int y, int flags, void* userdata)
{
    if (event == EVENT_LBUTTONDOWN)
    {
        isLMouseDown = true;
    }
    else if (event == EVENT_LBUTTONUP)
    {
        isLMouseDown = false;
    }
    if (event == EVENT_RBUTTONDOWN)
    {
        isRMouseDown = true;
    }
    else if (event == EVENT_RBUTTONUP)
    {
        isRMouseDown = false;
    }
    
    else if (event == EVENT_MOUSEMOVE)
    {
        mouse_x = x;
        mouse_y = y;
    }
    
}


map<int, int> findDents(Mat image, Mat mask)
{
    Mat binimage;
    map<int, int> dentlist;
    dentlist[0] = 0;
    dentlist[1] = 0;
    dentlist[2] = 0;
    dentlist[3] = 0;
    dentlist[4] = 0;
    dentlist[5] = 0;
    dentlist[6] = 0;
    dentlist[7] = 0;
    dentlist[8] = 0;

    inRange(image, Scalar(BLUE, GREEN, RED), Scalar(BLUE, GREEN, RED), binimage);

    multiply(binimage, mask, binimage);

    vector<vector<Point>> contours;
    vector<Vec4i> hierarchy;
    findContours(binimage, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_TC89_KCOS);

    for (int i = 0; i < contours.size(); i++)
    {
        // Blob-Eigenschaften ermitteln
        if (hierarchy[i][3] == -1)
        {
            double area = contourArea(contours[i]);
            if (area == 4)
            {
                int grayval = image.at<Vec3b>(contours[i][0].y+1, contours[i][0].x+1)[0];
                int dentclass = grayval / 10;
                if (dentclass > 8)
                    dentclass = 8;
                    
                dentlist[grayval / 10] += 1;
            }
        }
    }
    
    return dentlist;
}

Mat showDentList(map<int, int> dentlist)
{
    int cellheight = 25;
    int cellwidth = 100;

    Mat image = Mat::zeros(cellheight * 10, cellwidth * 2, CV_8UC3);

    //Title 
    putText(image, "Dent List", Point(35, 17), FONT_HERSHEY_PLAIN, 1, Scalar(255, 200, 100));

    // Horizontal lines
    for (int i = 1; i < 10; i++)
    {
        line(image, Point(0, cellheight*i), Point(cellwidth * 2, cellheight*i), Scalar(100,100,100));
    }

    for (int i = 1; i < 10; i++)
    {
        putText(image, to_string((i-1)*10) + "-" + to_string(i * 10), Point(5, 17 + i*cellheight), FONT_HERSHEY_PLAIN, 1, Scalar(255, 255, 255));
        putText(image, to_string(dentlist[i]), Point(cellwidth+cellwidth/2, 17 + i*cellheight), FONT_HERSHEY_PLAIN, 1, Scalar(255, 255, 255));
    }

    return image;
}

void main()
{
    int imselect = 0;
    int drawdelete = 0;
    int radius = 20;
    int clearSelect = 0;
    int mouseXPrev = mouse_x, mouseYPrev = mouse_y;

    vector<Mat> bluredgeImages;
    bluredgeImages.push_back(imread("recognitionImage.png", IMREAD_COLOR));
    //bluredgeImages.push_back(imread("bluredgeImage1.png", IMREAD_COLOR));
    //bluredgeImages.push_back(imread("bluredgeImage2.png", IMREAD_COLOR));

    vector<Mat> selectImages;

    vector<map<int, int>> dentlist;

    for (int i = 0; i < bluredgeImages.size(); i++)
    {
        selectImages.push_back(Mat::zeros(bluredgeImages[0].size(), CV_8UC1));
        map<int, int> dl;
        dentlist.push_back(dl);
    }

    int viewwidth = (std::min)(bluredgeImages[0].cols, WINDOWWIDTH);
    int viewheight = (std::min)(bluredgeImages[0].rows, WINDOWHEIGHT);

    namedWindow("Settings");
    createTrackbar("Image", "Settings", &imselect, bluredgeImages.size()-1);
    createTrackbar("Draw/Delete", "Settings", &drawdelete, 1);
    createTrackbar("Radius", "Settings", &radius, 200);
    createTrackbar("Clear", "Settings", &clearSelect, 1);

    namedWindow("dispimage");
    setMouseCallback("dispimage", mouseCallback, NULL);

    while (true)
    {
        Rect viewRoi = Rect(xView, yView, viewwidth, viewheight);

        if (isLMouseDown)
        {
            if (drawdelete == 0)
                circle(selectImages[imselect], Point(mouse_x + xView, mouse_y + yView), radius, 1, -1);
            else
                circle(selectImages[imselect], Point(mouse_x + xView, mouse_y + yView), radius, 0, -1);
        }

        if (isRMouseDown)
        {
            xView += mouseXPrev - mouse_x;
            yView += mouseYPrev - mouse_y;
 
            xView = (std::min)((std::max)(xView, 0), bluredgeImages[0].cols - viewwidth);
            yView = (std::min)((std::max)(yView, 0), bluredgeImages[0].rows - viewheight);
        }

        if (clearSelect == 1)
        {
            setTrackbarPos("Clear", "Settings", 0);
            selectImages[imselect] *= 0;
        }

        dentlist[imselect] = findDents(bluredgeImages[imselect], selectImages[imselect]);
        
        map<int, int> superdentlist;

        for (int i = 0; i < 9; i++)
        {
            for (int j = 0; j < dentlist.size(); j++)
            {
                superdentlist[i] += dentlist[j][i];
            }
            
        }

        Mat dentlistimage = showDentList(superdentlist);

        Mat dispimage = bluredgeImages[imselect].clone();
        
        // Combine BlurEdge Image with Selectimage
        vector<Mat> channels(3);
        split(dispimage, channels);
        multiply(1-selectImages[imselect], channels[2], channels[2]);
        merge(channels, dispimage);

        Mat dispimageClipped = dispimage(viewRoi).clone();
        imshow("dispimage", dispimageClipped);
        imshow("Dent List", dentlistimage);
        //imshow("colorfilterimage", colorfilterimage);

        mouseXPrev = mouse_x;
        mouseYPrev = mouse_y;

        if (waitKey(1) == 27) break;
    }
}