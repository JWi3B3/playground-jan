#include <iostream>

#include "IDSTest.hpp"

using namespace std;

IDSTest::IDSTest()
{
	int driver = is_GetDLLVersion();
	printf("Driver Version: %d.%02d.%04d\n", (driver >> 24), (driver >> 16 & 0xff), (driver & 0xffff));

	_initialize();
}


void IDSTest::_initialize()
{
	int count;
	is_GetNumberOfCameras(&count);

	cout << "Number of cams connected: " << count << endl;

	for (int i = 0; i < count; i++)
	{
		HIDS cam = static_cast<HIDS>(0);
		int retVal = _open(&cam, nullptr);

		if (retVal == IS_SUCCESS)
		{
			is_FreeImageMem(cam, _imgMem, _memoryID);

			// Disable trigger Mode
			is_SetExternalTrigger(cam, IS_SET_TRIGGER_OFF);

			// Camera info
			CAMINFO camInfo;
			is_GetCameraInfo(cam, &camInfo);
			cout << "ID: " << static_cast<int>(camInfo.Select) << endl;
			cout << "S/N: " << camInfo.SerNo << endl;

			// Sensor info
			SENSORINFO senInfo;
			is_GetSensorInfo(cam, &senInfo);

			// Max img size
			_width = senInfo.nMaxWidth;
			_height = senInfo.nMaxHeight;
			
			is_SetColorMode(cam, IS_CM_MONO8);

			switch (is_SetColorMode(cam, IS_GET_COLOR_MODE))
			{
				case IS_CM_RGBA12_UNPACKED:
				case IS_CM_BGRA12_UNPACKED:
					_bitsPerPixel = 64;
					break;

				case IS_CM_RGB12_UNPACKED:
				case IS_CM_BGR12_UNPACKED:
				case IS_CM_RGB10_UNPACKED:
				case IS_CM_BGR10_UNPACKED:
					_bitsPerPixel = 48;
					break;

				case IS_CM_RGBA8_PACKED:
				case IS_CM_BGRA8_PACKED:
				case IS_CM_RGB10_PACKED:
				case IS_CM_BGR10_PACKED:
				case IS_CM_RGBY8_PACKED:
				case IS_CM_BGRY8_PACKED:
					_bitsPerPixel = 32;
					break;

				case IS_CM_RGB8_PACKED:
				case IS_CM_BGR8_PACKED:
					_bitsPerPixel = 24;
					break;

				case IS_CM_BGR565_PACKED:
				case IS_CM_UYVY_PACKED:
				case IS_CM_CBYCRY_PACKED:
					_bitsPerPixel = 16;
					break;

				case IS_CM_BGR5_PACKED:
					_bitsPerPixel = 15;
					break;

				case IS_CM_MONO16:
				case IS_CM_SENSOR_RAW16:
				case IS_CM_MONO12:
				case IS_CM_SENSOR_RAW12:
				case IS_CM_MONO10:
				case IS_CM_SENSOR_RAW10:
					_bitsPerPixel = 16;
					break;

				case IS_CM_RGB8_PLANAR:
					_bitsPerPixel = 24;
					break;

				case IS_CM_MONO8:
				case IS_CM_SENSOR_RAW8:
				default:
					_bitsPerPixel = 8;
					break;
			}

			// setup the color depth
			//is_GetColorDepth(cam, &_bitsPerPixel, &_colorMode);
			//is_SetColorMode(cam, _colorMode);

			// initialize memory
			is_AllocImageMem(cam, _width, _height, _bitsPerPixel, &_imgMem, &_memoryID);
			is_SetImageMem(cam, _imgMem, _memoryID); // set memory active
			
			// Add cam to vector
			_cams.push_back(cam);

			_cam = cam;
		}
	}

}


int IDSTest::_open(HIDS* hCam, HWND hWnd)
{
	INT val = is_InitCamera(hCam, hWnd);

	if (val == IS_STARTER_FW_UPLOAD_NEEDED)
	{
		cout << "FW Upload needed" << endl;
	}

	return val;
}


cv::Mat IDSTest::getFrame()
{
	is_FreezeVideo(_cam, IS_DONT_WAIT);

	return cv::Mat(_height, _width, CV_8UC1, _imgMem);
}

int IDSTest::getCameraCount()
{
	return static_cast<int>(_cams.size());
}


void IDSTest::_close()
{
	for (auto const& value : _cams)
	{
		is_FreeImageMem(value, _imgMem, _memoryID);
		is_ExitCamera(value);
	}
}


IDSTest::~IDSTest()
{
	_close();
}
