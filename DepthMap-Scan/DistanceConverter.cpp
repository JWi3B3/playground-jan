#include "DistanceConverter.h"
#include <iostream>
#include <algorithm>

using namespace std;


DistanceConverter::DistanceConverter(vector<double>& distances_mm, double pixel_per_mm) :
	_slope(pixel_per_mm),
	_offset(0),
	_width_mm(0),
	_width_pixel(0)
{
	double dist_min = *min_element(distances_mm.begin(), distances_mm.end());
	double dist_max = *max_element(distances_mm.begin(), distances_mm.end());

	if (pixel_per_mm < 0)
		_offset = -_slope * dist_max;
	else
		_offset = -_slope * dist_min;

	_width_mm = dist_max - dist_min;
	_width_pixel = abs(to_pixel(dist_max) - to_pixel(dist_min));
	_minimum_mm = dist_min;
	_maximum_mm = dist_max;
}

double DistanceConverter::to_mm(double pixelposition)
{
	return (pixelposition - _offset) / _slope;
}

double DistanceConverter::to_pixel(double distance)
{
	return distance * _slope + _offset;
}

double DistanceConverter::get_pixelwidth()
{
	return _width_pixel;
}

double DistanceConverter::get_mmwidth()
{
	return _width_mm;
}

double DistanceConverter::get_minimum_mm()
{
	return _minimum_mm;
}

double DistanceConverter::get_maximum_mm()
{
	return _maximum_mm;
}

DistanceConverter::~DistanceConverter()
{
}
