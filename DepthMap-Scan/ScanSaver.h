#pragma once
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <vector>
#include <string>

namespace ScanSaver
{
	void saveScan(std::string path, std::vector<double>& distances, std::vector<cv::Mat>& images1, std::vector<cv::Mat>& images2);
	void openScan(std::string path, std::vector<double>& distances, std::vector<cv::Mat>& images1, std::vector<cv::Mat>& images2);
};

