#include "ScanProcess.h"
#include "DistanceConverter.h"
#include "Helpers.h"
#include <cstdio>
#include <iostream>
#include <math.h>
#include <chrono>
#include <vector>
#include <conio.h>
#include <string.h>
#include <boost/algorithm/clamp.hpp>

//#include <boost/thread/thread.hpp>
//#include <boost/thread/mutex.hpp>

using namespace std;
using namespace cv;

typedef chrono::high_resolution_clock Time;
typedef chrono::milliseconds ms;
typedef chrono::duration<int64> fsec;


Mat ScanProcess::calculateDepthMap(vector<Mat> images3d, vector<double> distances, int windowsize, double pixel_per_mm)
{
    DistanceConverter dc = DistanceConverter(distances, pixel_per_mm);

    Mat map3d = Mat::zeros(images3d[0].rows, windowsize + static_cast<int>(dc.get_pixelwidth()) + 1, CV_32F);
    Mat map3d_clipped, map_weight_clipped;
    Mat map_weight = map3d.clone();
    Mat frame3d, frame3d_bin, weight_1, weight_2;

    int cut_x_pos = images3d[0].cols / 2 - windowsize / 2;
    int xpos = 0;

    int rows = images3d[0].rows;
    Rect roi_cam(cut_x_pos, 0, windowsize, rows);

    for (int i = 0; i < images3d.size(); i++)
    {
        xpos = static_cast<int>(dc.to_pixel(distances[i]));

        if (xpos < 0)
        {
            cout << "Error in calculateDepthMap: xpos is " << xpos << endl;
            continue;
        }

        Rect roi_map(xpos, 0, windowsize, rows);

        frame3d = images3d[i](roi_cam).clone();
        frame3d.convertTo(frame3d_bin, CV_8UC1, 255);
        frame3d_bin /= 255;
        frame3d_bin.convertTo(frame3d_bin, CV_32F);

        map_weight_clipped = map_weight(roi_map).clone();

        weight_1 = map_weight_clipped.clone();

        map3d_clipped = map3d(roi_map).clone();

        frame3d.convertTo(frame3d, CV_32F);

        divide(weight_1, weight_1 + frame3d_bin, weight_1);
        subtract(1, weight_1, weight_2);

        // Gewichtetes Addieren
        multiply(map3d_clipped, weight_1, map3d_clipped);
        multiply(frame3d, weight_2, frame3d);
        add(frame3d, map3d_clipped, map3d_clipped);

        map_weight_clipped += frame3d_bin;

        map_weight_clipped.copyTo(map_weight(Rect(xpos, 0, windowsize, frame3d.rows)));
        map3d_clipped.copyTo(map3d(Rect(xpos, 0, windowsize, frame3d.rows)));
    }

    return map3d;
}

cv::Mat ScanProcess::calculatePreview(std::vector<cv::Mat> images2d, std::vector<double> distances, double pixel_per_mm, double correction)
{
    DistanceConverter dc = DistanceConverter(distances, pixel_per_mm);

    int imwidth = images2d[0].cols;

    //Combine images
    Mat map = Mat::zeros(images2d[0].rows, static_cast<int>(dc.get_pixelwidth()) + 1, CV_8UC1);

    int grayval = 0;
    double offset_px;

    for (int x = 0; x < map.cols; x++)
    {
        // Bestes Bild herausfinden
        int bestindex = 0;
        double bestdiff = 10000;
        for (int i = 0; i < distances.size(); i++)
        {
            double diff = abs(dc.to_mm(x) - distances[i]);
            if (diff < bestdiff)
            {
                bestdiff = diff;
                bestindex = i;
            }
        }

        offset_px = (static_cast<double>(x) - dc.to_pixel(distances[bestindex])) * correction;

        int xpos = imwidth / 2 + static_cast<int>(offset_px);
        if (xpos > 0 && xpos < imwidth)
        {
            for (int y = 0; y < map.rows; y++)
                map.at<uchar>(y, x) = images2d[bestindex].at<uchar>(y, xpos);
        }
        else
            continue;
    }

    return map;
}

cv::Mat ScanProcess::adjust2DMapping(cv::Mat image2d, cv::Mat image3d)
{
    image3d = Helpers::autoscale(image3d);


    int angle = 180, xpos = 300, ypos = 300, xscale = 100, yscale = 100, dispscale = 50;

    Mat mat_affine, mat_rot;
    Mat transformedimage = Mat::zeros(image2d.rows, image2d.cols, CV_8UC1);
    Mat dispimage;

    Point2f srcTri[3];
    Point2f dstTri[3];

    srcTri[0] = Point2f(static_cast<float>(image3d.cols / 2), static_cast<float>(image3d.rows / 2));
    srcTri[1] = Point2f(static_cast<float>(image3d.cols / 2. + 10), static_cast<float>(image3d.rows / 2.));
    srcTri[2] = Point2f(static_cast<float>(image3d.cols / 2.), static_cast<float>(image3d.rows / 2. + 10));

    dstTri[0] = srcTri[0];
    dstTri[1] = srcTri[1];
    dstTri[2] = srcTri[2];

    // Fenster und Slider erzeugen
    namedWindow("Settings");
    createTrackbar("Angle", "Settings", &angle, 359);
    createTrackbar("X Pos", "Settings", &xpos, 600);
    createTrackbar("Y Pos", "Settings", &ypos, 600);
    createTrackbar("X Scale", "Settings", &xscale, 400);
    createTrackbar("Y Scale", "Settings", &yscale, 400);
    createTrackbar("Display Scale", "Settings", &dispscale, 200);

    Point center = Point(image2d.cols / 2, image2d.rows / 2);
    Point center3d = Point(0, 0);


    int xoff = image2d.cols / 2 - image3d.cols / 2;
    int yoff = image2d.rows / 2 - image3d.rows / 2;

    while (true)
    {
        dstTri[0] = srcTri[0];
        dstTri[1] = srcTri[1];
        dstTri[2] = srcTri[2];

        // Skalierung
        dstTri[1].x = (dstTri[1].x - dstTri[0].x) * static_cast<float>(xscale) / 100.f + dstTri[0].x;
        dstTri[2].y = (dstTri[2].y - dstTri[0].y) * static_cast<float>(yscale) / 100.f + dstTri[0].y;

        // Verschiebung
        for (int i = 0; i < 3; i++)
        {
            dstTri[i].x += (xpos - 300) * 3 + xoff;
            dstTri[i].y += (ypos - 300) * 3 + yoff;
        }
        // Rotation
        for (int i = 0; i < 3; i++)
        {
            dstTri[i] = Helpers::rotatePoint(dstTri[i], dstTri[0], angle - 180);
        }

        mat_affine = getAffineTransform(srcTri, dstTri);

        dispimage = show2DMapping(image2d, image3d, mat_affine, static_cast<double>(dispscale) / 100.);

        imshow("2D Mapping", dispimage);

        if (waitKey(1) == 27)
            break;
    }
    destroyWindow("2D Mapping");

    return mat_affine;
}

Mat ScanProcess::show2DMapping(cv::Mat image2d, cv::Mat image3d, cv::Mat mapping, double scale)
{
    Mat transformedimage, dispimage, dispimage2;
    warpAffine(Helpers::autoscale(image3d), transformedimage, mapping, image2d.size(), INTER_NEAREST);
    
    transformedimage.convertTo(transformedimage, CV_8UC1);
    transformedimage = 255 - transformedimage;

    vector<Mat> channels = { transformedimage, transformedimage, image2d };

    merge(channels, dispimage);
    resize(dispimage, dispimage2, Size(0, 0), scale, scale, INTER_NEAREST);
    return dispimage2;
}

Mat ScanProcess::calculateBlurEdge(vector<Mat> images2d, vector<double> distances, Mat depthmap, Mat mapping, double pixeldepth, double referencedepth, int thresh, int blursize, int windowsize)
{
    double pixel_per_mm = pixeldepth / referencedepth;
    DistanceConverter dc = DistanceConverter(distances, pixel_per_mm);

    vector<Mat> images_processed;

    int imwidth = images2d[0].cols;
    int imwidthhalf = imwidth / 2;
    Mat processimage, grad_x, grad_y;

    if (blursize > 0)
        blursize = blursize * 2 + 1;

    // Richtige Frames raussuchen
    vector<int> ind;
    double stepsize_mm = 10;
    int last_index = 0;
    double current_diff = 1000;
    double last_diff = 1000;

    for (int i = 0; i < dc.get_mmwidth() / stepsize_mm; i++)
    {
        last_diff = 1000;
        for (int j = last_index; j < distances.size(); j++)
        {
            current_diff = abs(abs(distances[j] - dc.get_minimum_mm()) - stepsize_mm * i);
            if (current_diff < last_diff)
            {
                last_diff = current_diff;
                last_index = j;
            }
        }

        ind.push_back(last_index);
    }

    auto time1 = Time::now();

    //Preprocessing
    for (int i = 0; i < ind.size(); i++)
    {
        processimage = images2d[ind[i]].clone();
        threshold(processimage, processimage, thresh, 255, CV_THRESH_BINARY);

        Sobel(processimage, grad_x, CV_32F, 1, 0, 3, 1, 0, cv::BORDER_DEFAULT);
        Sobel(processimage, grad_y, CV_32F, 0, 1, 3, 1, 0, cv::BORDER_DEFAULT);
        convertScaleAbs(grad_x, grad_x);
        convertScaleAbs(grad_y, grad_y);
        add(grad_x, grad_y, processimage);

        if (blursize > 0)
            GaussianBlur(processimage, processimage, cv::Size(blursize, blursize), 0, 0, cv::BORDER_DEFAULT);

        images_processed.push_back(processimage.clone());
        cout << "\r";
        cout << i * 100 / ind.size() << "%...";
    }

    auto time2 = Time::now();
    auto timediff = time2 - time1;
    cout << "Preprocess time: " << chrono::duration_cast<chrono::seconds> (timediff).count() << " s" << endl;

    //Map berechnen
    Mat map = Mat::zeros(images2d[0].rows, static_cast<int>(dc.get_pixelwidth()), CV_8UC1);
    Mat transformedDepth, correctionMap;
    warpAffine(depthmap, transformedDepth, mapping, map.size(), INTER_NEAREST);
    divide(referencedepth, transformedDepth, correctionMap);
    imwrite("correctionmap.exr", correctionMap);
    int grayval = 0, bestindex = -1, offset_px;
    double dist_diff, best_diff;
    time1 = Time::now();

    vector<double> distances_pixel;
    for (int i = 0; i < ind.size(); i++)
    {
        distances_pixel.push_back(dc.to_pixel(distances[ind[i]]));
    }


    dist_diff = windowsize / abs(dc.to_pixel(0) - dc.to_pixel(stepsize_mm));

    int x, y, min_index, max_index, indexcount;
    for (x = 0; x < map.cols; x++)
    {
        best_diff = 10000;
        for (int k = 0; k < distances_pixel.size(); k++)
        {
            if (abs(distances_pixel[k] - x) < best_diff)
            {
                best_diff = abs(distances_pixel[k] - x);
                bestindex = k;
            }
            else
                break;
        }

        min_index = bestindex - static_cast<int>(dist_diff) / 2;
        max_index = bestindex + static_cast<int>(dist_diff) / 2;

        min_index = boost::algorithm::clamp(min_index, 0, static_cast<int>(distances_pixel.size()) - 1);
        max_index = boost::algorithm::clamp(max_index, 0, static_cast<int>(distances_pixel.size()) - 1);
        indexcount = abs(min_index - max_index);

        //cout << "minindex, maxindex: " << min_index << " " << max_index << endl;

        for (y = 0; y < map.rows; y++)
        {
            grayval = 0;
            for (int j = min_index; j < max_index; j++)
            {
                if (correctionMap.at<float>(y, x) > 0.7)
                {
                    offset_px = static_cast<int>((x - distances_pixel[j]) * correctionMap.at<float>(y, x) + imwidthhalf);
                    //offset_px = static_cast<int>((x - distances_pixel[j]) * 1.0 + imwidthhalf);

                    if (offset_px > 0 && offset_px < imwidth)
                        grayval += images_processed[j].at<uchar>(y, offset_px);
                }
            }

            if (indexcount > 0)
                grayval /= indexcount;

            map.at<uchar>(y, x) = grayval;
        }

        if (x % 20 == 0)
        {
            cout << "\r";
            cout << x * 100 / map.cols << "%...";
            imshow("bluredge", Helpers::autoscale(map));
            waitKey(1);
        }
    }

    // Flowfilter
    Mat temp, map_flowfilter;
    vector<Mat> channels(6);
    cv::cornerEigenValsAndVecs(map, temp, 15, 3);
    split(temp, channels);
    map_flowfilter = channels[5];




    for (int i = 0; i < channels.size(); i++)
    {
        double minval, maxval;
        minMaxIdx(channels[i], &minval, &maxval);

        imwrite("channel" + to_string(i) + ".png", Helpers::autoscale(channels[i], 2, minval, maxval*0.1));
    }


    time2 = Time::now();
    timediff = time2 - time1;
    cout << "Addition time: " << chrono::duration_cast<chrono::seconds> (timediff).count() << " s" << endl;

    cv::destroyWindow("2D Map");
    return map;
}