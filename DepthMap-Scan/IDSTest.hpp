#pragma once


#include <vector>

#include "IDS/uEye.h"
#include "opencv2/opencv.hpp"


class IDSTest
{
	public:
		IDSTest();
		~IDSTest();

		cv::Mat getFrame();
		int getCameraCount();

	private:
		std::vector<HIDS> _cams;
		HIDS _cam;

		INT _width; // width of frame
		INT _height; // height of frame
		INT _bitsPerPixel; // number of bits needed store one pixel
		INT _colorMode; // color mode (Y8/RGB16/RGB24/REG32)

		INT _memoryID; // buffer ID
		char* _imgMem; // buffer

		void _initialize();
		int _open(HIDS* hCam, HWND hWnd);
		void _close();
};

