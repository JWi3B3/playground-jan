#pragma once
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <vector>



namespace ScanProcess
{
    cv::Mat calculateBlurEdge(std::vector<cv::Mat> images2d, std::vector<double> distances, cv::Mat depthmap, cv::Mat mapping, double pixel_per_mm, double referencedepth, int thresh, int blursize, int windowsize);
    cv::Mat calculateDepthMap(std::vector<cv::Mat> images3d, std::vector<double> distances, int windowsize, double pixel_per_mm);
    cv::Mat calculatePreview(std::vector<cv::Mat> images2d, std::vector<double> distances, double pixel_per_mm, double correction = 1.0);

    cv::Mat adjust2DMapping(cv::Mat image2d, cv::Mat image3d);
    cv::Mat show2DMapping(cv::Mat image2d, cv::Mat image3d, cv::Mat mapping, double scale=1.0);
};

