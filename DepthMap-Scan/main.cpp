
#define _USE_MATH_DEFINES

#include <cstdio>
#include <iostream>
#include <math.h>
#include <chrono>
#include <vector>
#include <conio.h>
#include <string.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>

#include <astra/astra.hpp>

#include <pcl/common/common_headers.h>
#include <pcl/common/transforms.h>
#include <pcl/conversions.h>
#include <pcl/console/parse.h>
#include <pcl/features/normal_3d.h>
#include <pcl/filters/filter.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/surface/gp3.h>
#include <pcl/surface/organized_fast_mesh.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/visualization/keyboard_event.h>
#include <pcl/PolygonMesh.h>
#include "DepthCam.h"

#include "DepthConverter.h"
#include "CloudHandler.h"
#include "LabJack.h"
#include "IDSTest.hpp"
#include "DistanceConverter.h"
#include "ScanSaver.h"
#include "Helpers.h"
#include "ScanProcess.h"

//#include "ScanHelper.h"
//#include "SphereFinder.h"
//#include "RangeVis.h"
//#include "FileHandling.h"
//#include "Calibration_InteractorStyle.h"

using namespace cv;
using namespace std;
using namespace pcl;

bool do_scan = false;
bool do_saving = true;
bool do_mapping = true;

const int WINDOWSIZE = 10;
const double PIXEL_PER_MM_3D = -0.55;
const double PIXEL_PER_MM_2D = -1.44;

void main()
{
    vector<Mat> images2d;
    vector<Mat> images3d;
    vector<double> distances;

    if (do_scan)
    {
        // 3D-Kamera initialisieren
        astra::initialize();

        // 2D-Kamera initialisieren
        IDSTest cam2d = IDSTest();
        if (cam2d.getCameraCount() < 1)
        {
            cout << "No camera connected! Programm closed!" << endl;
            system("PAUSE");
            return;
        }

        namedWindow("DepthImage");

        double posrange = 10.0;
        int possteps = 2000;
        int res = 5;

        Mat frame3d, frame2d;

        // Kamera initialisieren
        DepthCam cam3d("device/sensor0");

        // Zeitmessung
        auto time1 = std::chrono::system_clock::now();
        auto time2 = std::chrono::system_clock::now();

        LabJack labjack;

        labjack.set_position(0, 3);
        labjack.set_position(2400, 0);

        double position;
        int timecounter = 0;
        double lastposition = -20000;
        while (true)
        {
            astra_temp_update();

            // Position vom Distanzsensor abfragen
            position = labjack.get_mm();
            if (position > lastposition)
            {
                lastposition = position;
                auto matrix_distance = CloudHandler::get_translatematrix(0, position / 1000, 0);
                cam3d.capture();
                frame2d = cam2d.getFrame();
                Helpers::rotateimage90(frame2d, 1);
                images2d.push_back(frame2d.clone());

                cam3d.getDepthImage(frame3d);
                transpose(frame3d, frame3d);
                Helpers::rotateimage90(frame3d, 2);
                images3d.push_back(frame3d.clone());
                distances.push_back(position);

                cv::imshow("DepthImage", Helpers::autoscale(frame3d));
                cv::imshow("Colorimage", frame2d);
            }
            if (waitKey(1) == 27) break;
        }
        astra::terminate();

        cv::destroyWindow("DepthImage");
        cv::destroyWindow("Colorimage");

        if (do_saving)
            ScanSaver::saveScan("Scan", distances, images2d, images3d);

        // Distances plotten
        Helpers::simple_plot(distances);
    }
    else
    {
        ScanSaver::openScan("Scan", distances, images2d, images3d);
    }

    cout << "Frames: " << images3d.size() << endl;
    
    // Tiefenkarte erstellen
    Mat map3d = ScanProcess::calculateDepthMap(images3d, distances, WINDOWSIZE, PIXEL_PER_MM_3D);
    imwrite("map3d_1.exr", map3d);
    imwrite("map3d_2.png", Helpers::autoscale(map3d));

    imshow("3D Map", Helpers::autoscale(map3d));
    waitKey(10);

    // Vorschaukarte erstellen
    Mat map_preview;
    map_preview = ScanProcess::calculatePreview(images2d, distances, PIXEL_PER_MM_2D, 1.0);

    // Mapping durchführen oder laden
    Mat mapping;

    //Writing
    cv::FileStorage fs;
    

    //Reading
    
    string filename = "mapping.yml";

    if (do_mapping)
    {
        mapping = ScanProcess::adjust2DMapping(map_preview, map3d);
        fs.open(filename, cv::FileStorage::WRITE);
        fs << "mapping" << mapping;
    }
    else
    {
        fs.open(filename, cv::FileStorage::READ);
        fs["mapping"] >> mapping;
 
        imshow("2D Mapping", ScanProcess::show2DMapping(map_preview, map3d, mapping, 0.5));
        waitKey(1);
    }
    fs.release(); //Very Important


    // BlurEdge berechnen
    Mat map2d;
    map2d = ScanProcess::calculateBlurEdge(images2d, distances, map3d, mapping, -1584., 1100, 70, 1, 1000);
    imwrite("map2d.png", Helpers::autoscale(map2d));
    imshow("2D Map", Helpers::autoscale(map2d));
    waitKey();

    destroyAllWindows();
    images2d.clear();
    images3d.clear();
    

    return;
}