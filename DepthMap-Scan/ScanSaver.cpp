#include "ScanSaver.h"
#include <boost\filesystem.hpp>
#include <iostream>
#include <sstream>
#include <iomanip>

using namespace std;
using namespace cv;

void ScanSaver::saveScan(string path, vector<double>& distances, vector<Mat>& images1, vector<Mat>& images2)
{
	
	string path_images1, path_images2, path_distances;

	path_images1 = path + "/images1";
	path_images2 = path + "/images2";
	path_distances = path + "/distances.txt";


	boost::filesystem::path dir(path.c_str());
	boost::filesystem::path dir_images1(path_images1.c_str());
	boost::filesystem::path dir_images2(path_images2.c_str());

	if (boost::filesystem::exists(dir))
	{
		boost::filesystem::remove_all(dir);
	}

	if (!(boost::filesystem::exists(dir)))
	{
		boost::filesystem::create_directory(dir);
		boost::filesystem::create_directory(dir_images1);
		boost::filesystem::create_directory(dir_images2);
	}

	cout << "Saving " << images1.size() - 1 << " images..." << endl;

	for (int i = 1; i < images1.size(); i++)
	{
		string filename;
		stringstream ss;
		ss << setw(4) << setfill('0') << i;
		filename = path_images1 + "/image" + ss.str() + ".png";
		imwrite(filename, images1[i]);
		cout << "\r";
		cout << i * 100 / images1.size() << " %...";
	}
	cout << endl;

	cout << "Saving " << images2.size() - 1 << " images..." << endl;

	for (int i = 1; i < images2.size(); i++)
	{
		string filename;
		stringstream ss;
		ss << setw(4) << setfill('0') << i;
		filename = path_images2 + "/image" + ss.str() + ".png";
		imwrite(filename, images2[i]);
		cout << "\r";
		cout << i * 100 / images2.size() << " %...";
	}
	cout << endl;

	ofstream myfile;
	myfile.open(path_distances);

	for (int i = 1; i < distances.size(); i++)
	{
		myfile << distances[i] << endl;
	}
	myfile.close();

	cout << " finished! " << endl;
}

void ScanSaver::openScan(std::string path, std::vector<double>& distances, std::vector<Mat>& images1, std::vector<Mat>& images2)
{
	cout << "Open scan..." << endl;
	string path_images1, path_images2, path_distances;

	path_images1 = path + "/images1";
	path_images2 = path + "/images2";
	path_distances = path + "/distances.txt";

	boost::filesystem::path dir(path.c_str());
	boost::filesystem::path dir_images1(path_images1.c_str());
	boost::filesystem::path dir_images2(path_images2.c_str());

	if (boost::filesystem::exists(dir) && boost::filesystem::exists(dir_images1) && boost::filesystem::exists(dir_images2))
	{
		distances.clear();
		images1.clear();
		images2.clear();

		ifstream myfile(path_distances);
		string line;
		if (myfile.is_open())
		{
			while (getline(myfile, line))
			{
				distances.push_back(stod(line));
			}
		}

		int imagecount = static_cast<int>(distances.size());
		
		Mat image;
		for (int i = 0; i < imagecount; i++)
		{
			string filename;
			stringstream ss;
			ss << setw(4) << setfill('0') << i+1;
			filename = path_images1 + "/image" + ss.str() + ".png";
			image = imread(filename, CV_LOAD_IMAGE_ANYDEPTH);
			images1.push_back(image.clone());

			cout << "\r";
			cout << i * 100 / (imagecount*2) << "%...";
		}

		for (int i = 0; i < imagecount; i++)
		{
			string filename;
			stringstream ss;
			ss << setw(4) << setfill('0') << i+1;
			filename = path_images2 + "/image" + ss.str() + ".png";
			image = imread(filename, CV_LOAD_IMAGE_ANYDEPTH);
			images2.push_back(image.clone());

			cout << "\r";
			cout << (i + imagecount) * 100 / (imagecount * 2) << "%...";
		}
		cout << endl;
	}
	else
	{
		cout << "No path: " << path << endl;
		return;
	}
}
