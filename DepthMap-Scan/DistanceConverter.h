#include <vector>

#pragma once
class DistanceConverter
{
public:
	DistanceConverter(std::vector<double>& distances_mm, double pixel_per_mm);
	double to_mm(double pixelposition);
	double to_pixel(double distance);
	double get_pixelwidth();
	double get_mmwidth();
	double get_minimum_mm();
	double get_maximum_mm();

	~DistanceConverter();
private:
	double _slope;
	double _offset;
	double _width_mm;
	double _width_pixel;
	double _minimum_mm;
	double _maximum_mm;
};

