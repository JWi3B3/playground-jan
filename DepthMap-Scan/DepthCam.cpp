#include "DepthCam.h"
#include <astra/astra.hpp>
#include "CloudHandler.h"

using namespace pcl;
using namespace std;

DepthCam::DepthCam(char* devicename) :
	listener(std::make_unique<DepthFrameListener>()),
	streamSet(devicename),
	reader(streamSet.create_reader()),
	depthStream(reader.stream<astra::DepthStream>()),
	depthconverter(DepthConverter(depthStream)),
	pointcloud(new PointCloud<PointXYZ>),
	pointcloud_color(new PointCloud<PointXYZRGB>),
	transformationmatrix(Eigen::Matrix4f::Identity()),
	resolution(1),
	r(255), g(255), b(),
	depthimage(cv::Mat(480, 640, CV_16U))
	
{
	depthStream.start();
	reader.add_listener(*listener);
}

void DepthCam::setTransformationMatrix(Eigen::Matrix4f transformation)
{
	transformationmatrix = transformation;
}

void DepthCam::setResolution(int res)
{
	if (res < 1)
	{
		cout << "DepthCam::setResolution Error: res < 1. Resolution is set to 1.";
		resolution = 1;
	}
	else
	{
		resolution = res;
	}
}

void DepthCam::setColor(int red, int green, int blue)
{
	r = red; g = green; b = blue;
}

void DepthCam::getPointCloud(PointCloud<PointXYZ>::Ptr pointcloud)
{
	depthconverter.cvmat2pointcloud(depthimage, pointcloud, resolution);
	
	CloudHandler::removeNaN(pointcloud);
	CloudHandler::transform(pointcloud, transformationmatrix);
}

void DepthCam::getColorPointCloud(PointCloud<PointXYZRGB>::Ptr pointcloud_color)
{
	depthconverter.cvmat2pointcloud_color(depthimage, pointcloud_color, resolution, r, g, b);
	CloudHandler::removeNaN(pointcloud_color);
	CloudHandler::transform(pointcloud_color, transformationmatrix);
}

void DepthCam::getDepthImage(cv::Mat & depthmat)
{
	if (depthimage.cols == 0 || depthimage.rows == 0)
	{
		depthmat = cv::Mat::zeros(480, 640, CV_16U);
		cv::putText(depthmat, "No Depthimage", cv::Point(0, 0), CV_FONT_HERSHEY_PLAIN, 1, 60000);
		return;
	}
		
	depthmat = depthimage.clone();
}

ModelCoefficients DepthCam::getCameraCone()
{
	return CloudHandler::create_cone(200.0, transformationmatrix);
}

void DepthCam::capture()
{
	listener->getFrame(depthimage);

}

DepthCam::~DepthCam()
{
	depthStream.stop();
}




