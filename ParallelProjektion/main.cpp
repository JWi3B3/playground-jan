#define _USE_MATH_DEFINES

#include <opencv2\opencv.hpp>
#include <vector>
#include <string>
#include <math.h>
#include "ImageDisplay.hpp"
#include "Timer.hpp"

cv::Mat depth2XPos(cv::Mat & depthimage, double fov)
{
    cv::Mat lineimageOrig = depthimage.clone();
    cv::Mat lineimage;

    int height = depthimage.rows;
    int width = depthimage.cols;

    cv::Mat coordinateImage = cv::Mat::zeros(height, width, CV_32FC1);
    cv::Mat mask = cv::Mat::ones(height, width, CV_32FC1);

    double xPos = 0, yPos = 0, grayVal = 0;
    double a1 = width / 2.;
    double f = a1 / (tan(fov/2 * M_PI / 180.));

    ushort* ptDepth;
    float* ptXPos;
    float* ptMask;

    for (int y = 0; y < height; y++)
    {
        ptDepth = depthimage.ptr<ushort>(y, 0);
        ptXPos = coordinateImage.ptr<float>(y, 0);
        ptMask = mask.ptr<float>(y, 0);

        for (int x = 0; x < width; x++)
        {
            if (ptDepth[x] != 0)
                ptXPos[x] = ((x - a1) * ptDepth[x]) / (f * sqrt(1 + pow((x + a1) / f, 2)));

            else
            {
                ptXPos[x] = 0;
                ptMask[x] = 0;
            }
        }
    }

    double minval, maxval;
    cv::minMaxLoc(coordinateImage, &minval, &maxval);

    cv::add(coordinateImage, abs(minval) + 10., coordinateImage);
    cv::multiply(coordinateImage, mask, coordinateImage);

    return coordinateImage;
}


void parallelProjection(cv::Mat & inOutImage, cv::Mat & depthimage, int width)
{
    double minval = 0, maxval = 0, slope = 0, offset = 0, p1 = 0, p2 = 0;
    int lastposition = 0;
    bool nextPixel = false;

    cv::Mat xPosImage = depth2XPos(depthimage, 58.59066069);

    // Pointer for fast image access
    cv::Vec3b*  ptNew;
    cv::Vec3b*  ptCol;
    float*      ptPos;

    cv::Mat maskEdge1, maskEdge2, mask;
    cv::Mat imgNew = cv::Mat::zeros(xPosImage.rows, width, CV_8UC3);

    // Create Mask with Edges
    cv::threshold(xPosImage, mask, 0, 1, CV_THRESH_BINARY);
    cv::Laplacian(xPosImage, maskEdge1, CV_32F);


    maskEdge1 = cv::abs(maskEdge1);

    cv::threshold(maskEdge1, maskEdge1, 500, 1, CV_THRESH_BINARY_INV);

    cv::Laplacian(mask, maskEdge2, CV_32F);
    cv::threshold(maskEdge2, maskEdge2, 0, 1, CV_THRESH_BINARY_INV);
    cv::multiply(maskEdge1, maskEdge2, maskEdge1);
    cv::multiply(maskEdge1, mask, mask);
    mask.convertTo(mask, CV_8UC1);

    // Calculate conversion from coordinate to pixel
    xPosImage.setTo(std::numeric_limits<float>::quiet_NaN(), 1. - mask);
    cv::minMaxLoc(xPosImage, &minval, &maxval);
    slope = width / (maxval - minval);
    offset = -slope * minval;

    for (int y = 0; y < imgNew.rows; y++)
    {
        ptNew = imgNew.ptr<cv::Vec3b>(y, 0);
        ptCol = inOutImage.ptr<cv::Vec3b>(y, 0);
        ptPos = xPosImage.ptr<float>(y, 0);

        // Right half
        lastposition = 0;

        for (int x = imgNew.cols / 2 - 10; x < imgNew.cols; x++)
        {
            for (int x2 = lastposition; x2 < xPosImage.cols; x2++)
            {
                if (isnormal(ptPos[x2]) && isnormal(ptPos[x2 + 1]))
                {
                    p1 = ptPos[x2] * slope + offset;
                    p2 = ptPos[x2 + 1] * slope + offset;

                    if (p1 <= x && p2 >= x)
                    {
                        ptNew[x] = ptCol[x2];
                        lastposition = x2;
                        nextPixel = true;
                        break;
                    }

                }
            }
            if (nextPixel)
            {
                nextPixel = false;
                continue;
            }
        }

        // Left half
        lastposition = xPosImage.cols;

        for (int x = imgNew.cols / 2 + 10; x > -1; x--)
        {
            for (int x2 = lastposition; x2 > 0; x2--)
            {
                if (isnormal(ptPos[x2]) && isnormal(ptPos[x2 - 1]))
                {
                    p1 = ptPos[x2] * slope + offset;
                    p2 = ptPos[x2 - 1] * slope + offset;

                    if (p1 >= x && p2 <= x)
                    {
                        ptNew[x] = ptCol[x2];
                        lastposition = x2;
                        nextPixel = true;
                        break;
                    }
                }
            }
            if (nextPixel)
            {
                nextPixel = false;
                continue;
            }
        }

        if (y % 20 == 0)
        {
            cv::imshow("imgNew", imgNew);
            cv::waitKey(1);
        }
    }

    inOutImage = imgNew.clone();
}


cv::Mat depth2world(cv::Mat & depthimage, float horizontalFov = 1.02259994, float verticalFov = 0.796615660)
{
    int resolutionX = depthimage.cols;
    int resolutionY = depthimage.rows;

    float xzFactor = tan(horizontalFov / 2) * 2;
    float yzFactor = tan(verticalFov / 2) * 2;

    float normalizedY;

    cv::Mat coordImage = cv::Mat::zeros(depthimage.size(), CV_32FC2);

    ushort*     ptDepth;
    cv::Vec2f*  ptCoord;

    for (int y = 0; y < resolutionY; y++)
    {
        ptDepth = depthimage.ptr<ushort>(y, 0);
        ptCoord = coordImage.ptr<cv::Vec2f>(y, 0);

        normalizedY = 0.5 - static_cast<float>(y) / resolutionY;

        for (int x = 0; x < resolutionX; x++)
        {
            ptCoord[x][0] = normalizedY * ptDepth[x] * yzFactor;
            ptCoord[x][1] = ptDepth[x];
        }
    }
  
    return coordImage;
}


void main()
{
    cv::Mat depth = cv::imread("depthmap2.png", cv::IMREAD_ANYDEPTH);
    cv::Mat imgCol = cv::imread("sharpedge2.png", cv::IMREAD_COLOR);

    
    cv::Mat depthframe = cv::imread("24.png", cv::IMREAD_ANYDEPTH);
    //cv::imshow("original", imgCol);

    Timer mytime;
    mytime.start();
    cv::Mat coordImage = depth2world(depthframe);
    mytime.stop();

    cv::Mat channels[2];
    cv::split(coordImage, channels);


    ImageDisplay::show(channels[0], "image");
    ImageDisplay::show(channels[1], "image");


    //cv::transpose(depth, depth);
    //cv::flip(depth, depth, 1);


    //parallelProjection(imgCol, depth, imgCol.cols * 1.4);


}