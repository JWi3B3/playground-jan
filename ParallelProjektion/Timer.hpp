/**
 * \file     Timer.hpp
 *
 * \brief    This class...
 *
 * \details  ...
 *
 * \author   Gennadi Eirich (gennadi.eirich@pdr-team.com)
 * \date     17.05.2016
 *
 * \version  0.1.0
 *
 * \note     Copyright (c) 2016, PDR-Team GmbH
 */


#ifndef TIMER_HPP
#define TIMER_HPP


#include <chrono>


class Timer
{
    public:
        Timer();
        ~Timer();

        void start();
        void stop();

    private:
        std::chrono::high_resolution_clock::time_point _start;
        std::chrono::high_resolution_clock::time_point _end;

        void printDuration() const;
};


#endif //TIMER_HPP
