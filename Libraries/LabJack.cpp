#include "LabJack.h"
#include <stdio.h>
#include <iostream>
#include <vector>
#include <windows.h>
#include "c:\Program Files (x86)\LabJack\Drivers\LabJackUD.h"


LabJack::LabJack()
{
	_lngHandle = 0;
	_lngIOType = 0, _lngChannel = 0;
	_voltage = 0;
	_mm_slope = 0;
	_mm_offset = -1;

	OpenLabJack(LJ_dtU6, LJ_ctUSB, "1", 1, &(_lngHandle));

	ePut(_lngHandle, LJ_ioPUT_AIN_RANGE, 0, LJ_rgBIP10V, 0);

	AddRequest(_lngHandle, LJ_ioGET_AIN, 0, 0, 0, 0);
}


LabJack::~LabJack()
{
}

double LabJack::get_voltage()
{
	GoOne(_lngHandle);
	GetFirstResult(_lngHandle, &(_lngIOType), &(_lngChannel), &(_voltage), 0, 0);
	return _voltage;
}

double LabJack::get_mm()
{
	auto voltage = get_voltage();
	return voltage2mm(voltage);
}

double LabJack::voltage2mm(double voltage)
{
	return _mm_slope * voltage + _mm_offset;
}

void LabJack::set_position(double position_mm, double voltage)
{
	std::vector<double> linearfunction;

	_data_mm.push_back(position_mm);
	_data_voltage.push_back(voltage);

	if (_data_mm.size() > 1 && _data_mm.size() == _data_voltage.size())
	{
		linearfunction = GetLinearFit(_data_mm, _data_voltage);
		_mm_slope = linearfunction[0];
		_mm_offset = linearfunction[1];
	}
}

std::vector<double> LabJack::GetLinearFit(const std::vector<double>& data_mm, const std::vector<double>& data_voltage)
{
	if (data_mm.size() < 2 || data_mm.size() != data_voltage.size())
	{
		return std::vector<double> { 0, -1};
	}

	double xSum = 0, ySum = 0, xxSum = 0, xySum = 0, slope, intercept;

	for (long i = 0; i < data_mm.size(); i++)
	{
		xSum += data_voltage[i];
		ySum += data_mm[i];
		xxSum += data_voltage[i] * data_voltage[i];
		xySum += data_voltage[i] * data_mm[i];
	}

	slope = (data_mm.size() * xySum - xSum * ySum) / (data_mm.size() * xxSum - xSum * xSum);
	intercept = (ySum - slope * xSum) / data_mm.size();
	std::vector<double> res;
	res.push_back(slope);
	res.push_back(intercept);
	return res;
}