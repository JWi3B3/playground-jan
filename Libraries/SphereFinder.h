#include <pcl/common/common_headers.h>
#include <pcl/sample_consensus/ransac.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/passthrough.h>
#include <pcl/features/normal_3d.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>

#include <vector>

#pragma once
class SphereFinder
{
public:
	SphereFinder(double diameter, double tolerance);
	~SphereFinder();
	std::vector<double> find(const pcl::PointCloud<pcl::PointXYZ>::Ptr cloud);

private:
	pcl::ModelCoefficients coefficients;
	pcl::PointIndices inliers;
	//pcl::SACSegmentation<pcl::PointXYZ> seg;
	pcl::SACSegmentationFromNormals<pcl::PointXYZ, pcl::Normal> seg;
};

