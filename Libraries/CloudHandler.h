#include <iostream>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <math.h>
#include <pcl/common/transforms.h>
#include <pcl/filters/plane_clipper3D.h>
#include <boost/random/uniform_int.hpp>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/surface/mls.h>
#include <pcl/surface/organized_fast_mesh.h>
#include <pcl/features/normal_3d.h>
#include <pcl/surface/gp3.h>
#include <pcl/PolygonMesh.h>
#include <boost/thread/thread.hpp>
#include <pcl/common/common_headers.h>
#include <pcl/io/pcd_io.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/console/parse.h>
#include <pcl/PCLPointCloud2.h>

#pragma once
namespace CloudHandler
{
	// Transformation
	void transform(pcl::PointCloud<pcl::PointXYZ>::Ptr pointcloud, const Eigen::Matrix4f& matrix);
	void transform(pcl::PointCloud<pcl::PointXYZRGB>::Ptr pointcloud, const Eigen::Matrix4f& matrix);
	void rotate(pcl::PointCloud<pcl::PointXYZ>::Ptr pointcloud, double x_angle, double y_angle, double z_angle);
	void translate(pcl::PointCloud<pcl::PointXYZ>::Ptr pointcloud, double x_trans, double y_trans, double z_trans);
	void scale(pcl::PointCloud<pcl::PointXYZ>::Ptr pointcloud, double x_scale, double y_scale, double z_scale);
	void transformPointCloud2(pcl::PCLPointCloud2::Ptr pointcloud, const Eigen::Matrix4f& matrix);
	void transformPolygonMesh(pcl::PolygonMesh& mesh, Eigen::Matrix4f& matrix);

	Eigen::Matrix4f get_rotatematrix(double x_angle, double y_angle, double z_angle);
	Eigen::Matrix4f get_translatematrix(double x_trans, double y_trans, double z_trans);
	Eigen::Matrix4f get_scalematrix(double x_scale, double y_scale, double z_scale);

	// Bearbeitung
	void clipdistance(pcl::PointCloud<pcl::PointXYZ>::Ptr pointcloud, double distance);
	void clipplane(pcl::PointCloud<pcl::PointXYZ>::Ptr pointcloud, double xdir, double ydir, double zdir, double distance);
	void reducepoints(pcl::PointCloud<pcl::PointXYZ>::Ptr pointcloud, double size);
	void smooth(pcl::PointCloud<pcl::PointXYZ>::Ptr pointcloud, double size);
	void removeNaN(pcl::PointCloud<pcl::PointXYZ>::Ptr pointcloud);
	void removeNaN(pcl::PointCloud<pcl::PointXYZRGB>::Ptr pointcloud);

	// Meshing
	void pointcloud2mesh_organized(pcl::PointCloud<pcl::PointXYZ>::Ptr pointcloud, pcl::PolygonMesh& mesh, double maxedgelength);
	void pointcloud2mesh_unorganized(pcl::PointCloud<pcl::PointXYZ>::Ptr pointcloud, pcl::PolygonMesh& mesh, double maxedgelength);

	// Konvertierung
	void colorizePointCloud(pcl::PointCloud<pcl::PointXYZ>::Ptr pointcloud_in, pcl::PointCloud<pcl::PointXYZRGB>::Ptr pointcloud_out, int r, int g, int b);

	// Erzeugung
	pcl::PointCloud<pcl::PointXYZ> create_sphere(double diameter, int pointcount);
	pcl::PointCloud<pcl::PointXYZ> create_orientationarrows(double size);
	pcl::ModelCoefficients create_cone(double size, Eigen::Matrix4f transformation);

	// Ausgabe
	void printmatrix(Eigen::Matrix4f matrix);
	void printPointCloudBoundaries(pcl::PointCloud<pcl::PointXYZRGB>::Ptr pointcloud);
};

