#include "RangeVis.h"
#include <math.h>

using namespace pcl;


RangeVis::RangeVis(float angularRes, float fov_x, float fov_y) :
	_rangeImageWidget("Range image"),
	_angularResolution((float)(angularRes * (M_PI / 180.0f))),
	_maxAngleWidth(fov_x* (M_PI / 180.0f)),
	_maxAngleHeight(fov_y* (M_PI / 180.0f)),
	_cameraXPos(0),
	_cameraYPos(0),
	_cameraZPos(0),
	_cameraXRot(0),
	_cameraYRot(0),
	_cameraZRot(0),
	_sensorPose(Eigen::Affine3f::Identity()),
	_coordinate_frame(RangeImage::CAMERA_FRAME),
	_noiseLevel(0.0),
	_minRange(0.0),
	_borderSize(0),
	_cornerpoints(new PointCloud<PointXYZ>),
	updated(true)
{
	set_CameraPosition(_cameraXPos, _cameraYPos, _cameraZPos, _cameraXRot, _cameraYRot, _cameraZRot);
}

void RangeVis::show(PointCloud<PointXYZ>::Ptr pointcloud)
{
	_cloudptr = pointcloud;
	RangeImage::Ptr rangeimage(new RangeImage);
	render_rangeImage(rangeimage);
	_rangeImageWidget.showRangeImage(*rangeimage);
	updated = true;
}

void RangeVis::spin(int milliseconds)
{
	if (!updated)
	{
		RangeImage::Ptr rangeimage(new RangeImage);
		render_rangeImage(rangeimage);	
		_rangeImageWidget.showRangeImage(*rangeimage);
	}

	_rangeImageWidget.spinOnce(milliseconds);
}

bool RangeVis::wasStopped()
{
	return _rangeImageWidget.wasStopped();
}

void RangeVis::set_CameraPosition(double x, double y, double z, double x_rot, double y_rot, double z_rot)
{
	_cameraXPos = x;
	_cameraYPos = y;
	_cameraZPos = z;
	_cameraXRot = x_rot;
	_cameraYRot = y_rot;
	_cameraZRot = z_rot;

	_sensorPose = Eigen::Affine3f::Identity();
	_sensorPose.translation() << _cameraXPos, _cameraYPos, _cameraZPos;
	_sensorPose.rotate(Eigen::AngleAxisf(_cameraXRot * M_PI / 180., Eigen::Vector3f::UnitX()));
	_sensorPose.rotate(Eigen::AngleAxisf(_cameraYRot * M_PI / 180., Eigen::Vector3f::UnitY()));
	_sensorPose.rotate(Eigen::AngleAxisf(_cameraZRot * M_PI / 180., Eigen::Vector3f::UnitZ()));

	calculateCornerPoints();
	updated = false;
}

void RangeVis::set_CameraFoV(float fov_x, float fov_y)
{
	_maxAngleWidth = fov_x* (M_PI / 180.0f);
	_maxAngleHeight = fov_y* (M_PI / 180.0f);

	calculateCornerPoints();
	updated = false;
}

void RangeVis::calculateCornerPoints()
{
	_cornerpoints->clear();

	double x = tan(_maxAngleWidth / 2) * 10;
	double y = tan(_maxAngleHeight / 2) * 10;
	double z = 10;

	PointXYZ point1(x, y, z);
	PointXYZ point2(-x, y, z);
	PointXYZ point3(x, -y, z);
	PointXYZ point4(-x, -y, z);

	_cornerpoints->push_back(point1);
	_cornerpoints->push_back(point2);
	_cornerpoints->push_back(point3);
	_cornerpoints->push_back(point4);

	transformPointCloud(*_cornerpoints, *_cornerpoints, _sensorPose);

	for (int i = 0; i < 4; i++)
	{
		cout << _cornerpoints->points[i] << endl;
	}
}

void RangeVis::render_rangeImage(RangeImage::Ptr rangeImage)
{
	pcl::PointCloud<pcl::PointXYZ>::Ptr pointcloud_copy(new PointCloud<PointXYZ>);
	*pointcloud_copy += *_cloudptr;
	//*pointcloud_copy += *_cornerpoints;

	rangeImage->createFromPointCloud(*_cloudptr, _angularResolution,
		2 * M_PI, M_PI,
		_sensorPose, _coordinate_frame, _noiseLevel, _minRange, _borderSize);
}

RangeVis::~RangeVis()
{
}