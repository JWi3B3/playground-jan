#include "FileHandling.h"
#include "opencv2/opencv.hpp"
#include <string>

using namespace std;
using namespace cv;

void FileHandling::saveEigenMatrix4f(Eigen::Matrix4f matrix, string filename)
{
	FileStorage fs(filename + ".yml", FileStorage::WRITE);
	Mat cvmatrix = Mat(4, 4, CV_64F);
	for (int x = 0; x < 4; x++)
	{
		for (int y = 0; y < 4; y++)
		{
			cvmatrix.at<double>(y,x) = matrix(x,y);
		}
	}
		
	fs << "matrix" << cvmatrix;
	fs.release();
}

Eigen::Matrix4f FileHandling::openEigenMatrix4f(string filename)
{
	FileStorage fs2(filename + ".yml", FileStorage::READ);

	Mat cvmatrix = Mat(4, 4, CV_64F);
	Eigen::Matrix4f matrix;

	fs2["matrix"] >> cvmatrix;

	for (int x = 0; x < 4; x++)
	{
		for (int y = 0; y < 4; y++)
		{
			matrix(x, y) = cvmatrix.at<double>(y, x);
		}
	}

	fs2.release();

	return matrix;
}

