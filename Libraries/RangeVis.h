#include <math.h>
#include <pcl/common/transforms.h>
#include <pcl/filters/plane_clipper3D.h>
#include <boost/random/uniform_int.hpp>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/surface/mls.h>
#include <pcl/surface/organized_fast_mesh.h>
#include <pcl/features/normal_3d.h>
#include <pcl/surface/gp3.h>
#include <pcl/PolygonMesh.h>
#include <boost/thread/thread.hpp>
#include <pcl/common/common_headers.h>
#include <pcl/range_image/range_image.h>
#include <pcl/io/pcd_io.h>
#include <pcl/visualization/range_image_visualizer.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/console/parse.h>


#pragma once
class RangeVis
{
public:
	RangeVis(float angularRes = 0.2, float fov_x = 90, float fov_y = 90);
	~RangeVis();

	void show(pcl::PointCloud<pcl::PointXYZ>::Ptr pointcloud);
	void spin(int milliseconds = 10);
	bool wasStopped();

	void set_CameraPosition(double x, double y, double z, double x_rot, double y_rot, double z_rot);
	void set_CameraFoV(float fov_x, float fov_y);


private:
	// Variablen
	pcl::visualization::RangeImageVisualizer _rangeImageWidget;
	double _cameraXPos;
	double _cameraYPos;
	double _cameraZPos;
	double _cameraXRot;
	double _cameraYRot;
	double _cameraZRot;

	float _angularResolution;
	float _maxAngleWidth;
	float _maxAngleHeight;
	Eigen::Affine3f _sensorPose;
	pcl::RangeImage::CoordinateFrame _coordinate_frame;
	float _noiseLevel;
	float _minRange;
	int _borderSize;
	pcl::PointCloud<pcl::PointXYZ>::Ptr _cornerpoints;
	pcl::PointCloud<pcl::PointXYZ>::Ptr _cloudptr;
	bool updated;

	// Funktionen
	void render_rangeImage(pcl::RangeImage::Ptr rangeImage);
	void calculateCornerPoints();
};

