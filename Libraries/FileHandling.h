#include <pcl/common/transforms.h>
#include <string>

#pragma once
namespace FileHandling
{
	void saveEigenMatrix4f(Eigen::Matrix4f matrix, std::string filename);
	Eigen::Matrix4f openEigenMatrix4f(std::string filename);
};