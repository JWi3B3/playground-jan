#include <astra/astra.hpp>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include <boost/thread/thread.hpp>

#include <pcl/common/common_headers.h>
#include <pcl/range_image/range_image.h>
#include <pcl/io/pcd_io.h>
#include <pcl/visualization/range_image_visualizer.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/console/parse.h>


#pragma once
class DepthConverter
{
public:
	DepthConverter(const astra::DepthStream depthstream);
	~DepthConverter();

	void depthframe2pointcloud(const astra::DepthFrame& frame, pcl::PointCloud<pcl::PointXYZ>::Ptr pointcloud, const int stepsize=1);
	void cvmat2pointcloud(cv::Mat& frame, pcl::PointCloud<pcl::PointXYZ>::Ptr pointcloud, const int stepsize = 1);
	void cvmat2pointcloud_color(cv::Mat& frame, pcl::PointCloud<pcl::PointXYZRGB>::Ptr pointcloud, const int stepsize = 1, int r = 255, int g = 255, int b = 255);

	static cv::Mat depthframe2cvmat(astra::DepthFrame frame, int minval, int maxval);
	static cv::Mat depthframe2shadingmat(const astra::DepthFrame& frame, const double& degree, int addition, int division);

private:
	astra::CoordinateMapper coordinateMapper;
};

