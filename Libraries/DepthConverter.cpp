#include "DepthConverter.h"
#include <iostream>
#include <astra/astra.hpp>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include <boost/thread/thread.hpp>

#include <pcl/common/common_headers.h>
#include <pcl/range_image/range_image.h>
#include <pcl/io/pcd_io.h>
#include <pcl/visualization/range_image_visualizer.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/console/parse.h>
#include <pcl/point_types.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/features/normal_3d.h>
#include <pcl/surface/gp3.h>
#include <pcl/surface/organized_fast_mesh.h>
#include <pcl/common/common.h>

using namespace pcl;

DepthConverter::DepthConverter(const astra::DepthStream depthstream) : 
	coordinateMapper(depthstream.coordinateMapper())
{

}

DepthConverter::~DepthConverter() {}

void DepthConverter::depthframe2pointcloud(
	const astra::DepthFrame& frame,
	PointCloud<PointXYZ>::Ptr pointcloud,
	const int stepsize)
{
	cv::Mat depthmat = cv::Mat(480, 640, CV_16U);
	frame.copy_to((short*)depthmat.data);

	return cvmat2pointcloud(depthmat, pointcloud, stepsize);
}

void DepthConverter::cvmat2pointcloud(
	cv::Mat& frame,
	PointCloud<PointXYZ>::Ptr pointcloud,
	const int stepsize)
{
	int h = frame.rows/stepsize;
	int w = frame.cols/stepsize;

	pointcloud->height = 1;
	pointcloud->width = w*h;
	pointcloud->resize(w*h);

	for (int y = 0; y < h; y++)
	{
		for (int x = 0; x < w; x++)
		{
			if (frame.at<short>(y*stepsize, x*stepsize) != 0)
			{
				coordinateMapper.convert_depth_to_world(
					x*stepsize, y*stepsize, frame.at<short>(y*stepsize, x*stepsize),
					&(pointcloud->at(x + y*w).x),
					&(pointcloud->at(x + y*w).y),
					&(pointcloud->at(x + y*w).z));
			}
			else
			{
				pointcloud->at(x + y*w).x = std::numeric_limits<float>::quiet_NaN();
				pointcloud->at(x + y*w).y = std::numeric_limits<float>::quiet_NaN();
				pointcloud->at(x + y*w).z = std::numeric_limits<float>::quiet_NaN();
			}
		}
	}
	pointcloud->is_dense = false;
}

void DepthConverter::cvmat2pointcloud_color(
	cv::Mat& frame,
	PointCloud<PointXYZRGB>::Ptr pointcloud,
	const int stepsize,
	int r, int g, int b)
{
	int h = frame.rows / stepsize;
	int w = frame.cols / stepsize;

	pointcloud->height = 1;
	pointcloud->width = w*h;
	pointcloud->resize(w*h);

	for (int y = 0; y < h; y++)
	{
		for (int x = 0; x < w; x++)
		{
			if (frame.at<short>(y*stepsize, x*stepsize) != 0)
			{
				coordinateMapper.convert_depth_to_world(
					x*stepsize, y*stepsize, frame.at<short>(y*stepsize, x*stepsize),
					&(pointcloud->at(x + y*w).x),
					&(pointcloud->at(x + y*w).y),
					&(pointcloud->at(x + y*w).z));
			}
			else
			{
				pointcloud->at(x + y*w).x = std::numeric_limits<float>::quiet_NaN();
				pointcloud->at(x + y*w).y = std::numeric_limits<float>::quiet_NaN();
				pointcloud->at(x + y*w).z = std::numeric_limits<float>::quiet_NaN();
			}
			pointcloud->at(x + y*w).r = r;
			pointcloud->at(x + y*w).g = g;
			pointcloud->at(x + y*w).b = b;
		}
	}
	pointcloud->is_dense = false;
}

cv::Mat DepthConverter::depthframe2cvmat(astra::DepthFrame frame, int minval, int maxval)
{
	int h = frame.height();
	int w = frame.width();
	int grayval;
	cv::Mat image = cv::Mat(h, w, CV_8UC1);
	uchar* imdata = image.data;

	for (int i = 0; i < h*w; i++)
	{
		grayval = (int)((float(frame.data()[i]) - float(minval)) / ((float(maxval) - float(minval)) / 255.0));

		imdata[i] =
			(unsigned char)std::max(0, std::min(255, grayval));
	}
	return image;
}


cv::Mat DepthConverter::depthframe2shadingmat(const astra::DepthFrame& frame, const double& degree, int addition, int division)
{
	int h = frame.height();
	int w = frame.width();
	cv::Mat image = cv::Mat(h, w, CV_16U);
	cv::Mat sobel_x, sobel_y;
	cv::Mat combined;
	cv::Mat mask, mask2;
	cv::Mat returnimage;

	short* imdata = (short*)image.data;

	for (int i = 0; i < h*w; i++)
		imdata[i] = (short)frame.data()[i];

	image.convertTo(image, CV_32F);
	cv::threshold(image, mask, 1, 255, cv::THRESH_BINARY);

	mask.convertTo(mask, CV_8UC1);

	Sobel(image, sobel_x, CV_32F, 1, 0);
	Sobel(image, sobel_y, CV_32F, 0, 1);

	// TODO: Mask verwenden

	cv::addWeighted(sobel_x, sin(degree), sobel_y, cos(degree), 0, combined, CV_32F);

	combined = combined*((float)division / 5.0) + pow(2, addition);
	combined.convertTo(combined, CV_8UC1);

	threshold(combined, mask2, 254, 255, cv::THRESH_BINARY);
	mask -= mask2;
	combined.copyTo(returnimage, mask);

	return returnimage;
}