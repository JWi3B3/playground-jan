#include <stdio.h>
#include <vector>
#include <windows.h>
#include "c:\Program Files (x86)\LabJack\Drivers\LabJackUD.h"

#pragma once
class LabJack
{
public:
	LabJack();
	~LabJack();
	double get_voltage();
	double get_mm();
	double voltage2mm(double voltage);
	void set_position(double position_mm, double voltage);

private:
	LJ_HANDLE _lngHandle;
	long _lngIOType;
	long _lngChannel;
	double _voltage;
	double _mm_slope;
	double _mm_offset;
	std::vector<double> _data_mm;
	std::vector<double> _data_voltage;
	std::vector<double> LabJack::GetLinearFit(const std::vector<double>& data_mm, const std::vector<double>& data_voltage);
};

