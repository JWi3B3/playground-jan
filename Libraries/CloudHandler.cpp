#include "CloudHandler.h"
#include <iostream>

using namespace pcl;

#define _USE_MATH_DEFINES

void CloudHandler::transform(PointCloud<PointXYZ>::Ptr pointcloud, const Eigen::Matrix4f& matrix)
{
	transformPointCloud(*pointcloud, *pointcloud, matrix);
}

void CloudHandler::transform(PointCloud<PointXYZRGB>::Ptr pointcloud, const Eigen::Matrix4f& matrix)
{
	transformPointCloud(*pointcloud, *pointcloud, matrix);
}

void CloudHandler::rotate(PointCloud<PointXYZ>::Ptr pointcloud, double x_angle, double y_angle, double z_angle)
{
	auto matrix = CloudHandler::get_rotatematrix(x_angle, y_angle, z_angle);
	transformPointCloud(*pointcloud, *pointcloud, matrix);
}


void CloudHandler::translate(PointCloud<PointXYZ>::Ptr pointcloud, double x_trans, double y_trans, double z_trans)
{
	auto matrix = CloudHandler::get_translatematrix(x_trans, y_trans, z_trans);
	transformPointCloud(*pointcloud, *pointcloud, matrix);
}


void CloudHandler::scale(PointCloud<PointXYZ>::Ptr pointcloud, double x_scale, double y_scale, double z_scale)
{
	auto matrix = CloudHandler::get_scalematrix(x_scale, y_scale, z_scale);
	transformPointCloud(*pointcloud, *pointcloud, matrix);
}

void CloudHandler::pointcloud2mesh_organized(PointCloud<PointXYZ>::Ptr pointcloud, PolygonMesh& mesh, double maxedgelength)
{
	OrganizedFastMesh<PointXYZ> ofm;

	// Set parameters
	ofm.setInputCloud(pointcloud);
	ofm.setMaxEdgeLength(maxedgelength);
	ofm.setTrianglePixelSize(1);
	ofm.setTriangulationType(OrganizedFastMesh<PointXYZ>::TRIANGLE_ADAPTIVE_CUT);

	// Reconstruct
	ofm.reconstruct(mesh);
}


void CloudHandler::colorizePointCloud(PointCloud<PointXYZ>::Ptr pointcloud_in, PointCloud<PointXYZRGB>::Ptr pointcloud_out, int r, int g, int b)
{
	pointcloud_out->resize( pointcloud_in->width * pointcloud_in->height);



	for (int i = 0; i < pointcloud_in->size(); i++)
	{
		// Koordinaten kopieren
		pointcloud_out->points[i].x = pointcloud_in->points[i].x;
		pointcloud_out->points[i].y = pointcloud_in->points[i].y;
		pointcloud_out->points[i].z = pointcloud_in->points[i].z;

		// Farbe definieren
		pointcloud_out->points[i].r = r;
		pointcloud_out->points[i].g = g;
		pointcloud_out->points[i].b = b;
	}
}

void CloudHandler::pointcloud2mesh_unorganized(PointCloud<PointXYZ>::Ptr pointcloud, PolygonMesh& mesh, double maxedgelength)
{
	// TODO: Check if Cloud is organized

	// Normal estimation*
	NormalEstimation<PointXYZ, Normal> n;
	PointCloud<Normal>::Ptr normals(new PointCloud<Normal>);
	search::KdTree<PointXYZ>::Ptr tree(new search::KdTree<PointXYZ>);
	tree->setInputCloud(pointcloud);
	n.setInputCloud(pointcloud);
	n.setSearchMethod(tree);
	n.setKSearch(20);
	n.compute(*normals);
	//* normals should not contain the point normals + surface curvatures

	// Concatenate the XYZ and normal fields*
	PointCloud<PointNormal>::Ptr cloud_with_normals(new PointCloud<PointNormal>);
	concatenateFields(*pointcloud, *normals, *cloud_with_normals);
	//* cloud_with_normals = cloud + normals

	// Create search tree*
	search::KdTree<PointNormal>::Ptr tree2(new search::KdTree<PointNormal>);
	tree2->setInputCloud(cloud_with_normals);

	// Initialize objects
	GreedyProjectionTriangulation<PointNormal> gp3;
	PolygonMesh triangles;

	// Set the maximum distance between connected points (maximum edge length)
	gp3.setSearchRadius(0.025);

	// Set typical values for the parameters
	gp3.setMu(2.5);
	gp3.setMaximumNearestNeighbors(0.5);
	gp3.setMaximumSurfaceAngle(M_PI / 4); // 45 degrees
	gp3.setMinimumAngle(M_PI / 18); // 10 degrees
	gp3.setMaximumAngle(2 * M_PI / 3); // 120 degrees
	gp3.setNormalConsistency(false);

	// Get result
	gp3.setInputCloud(cloud_with_normals);
	gp3.setSearchMethod(tree2);
	gp3.reconstruct(triangles);

	// Additional vertex information
	std::vector<int> parts = gp3.getPartIDs();
	std::vector<int> states = gp3.getPointStates();
}

void CloudHandler::clipdistance(PointCloud<PointXYZ>::Ptr pointcloud, double distance)
{
	Eigen::Vector4f plane;
	plane(0) = 0.0;
	plane(1) = 0.0;
	plane(2) = 1.0;
	plane(3) = distance;
	PlaneClipper3D<PointXYZ> planeclipper(plane);
	std::vector<int> clipped;

	planeclipper.clipPointCloud3D(*pointcloud, clipped);

	int clip_idx = 0;
	for (int i = 0; i < pointcloud->size(); i++)
	{
		if (clipped[clip_idx] == i)
		{
			clip_idx++;
			continue;
		}
		
		pointcloud->at(i).x = std::numeric_limits<float>::quiet_NaN();
		pointcloud->at(i).y = std::numeric_limits<float>::quiet_NaN();
		pointcloud->at(i).z = std::numeric_limits<float>::quiet_NaN();
	}
}

void CloudHandler::removeNaN(PointCloud<PointXYZ>::Ptr pointcloud)
{
	std::vector<int> indices;
	removeNaNFromPointCloud(*pointcloud, *pointcloud, indices);
}

void CloudHandler::removeNaN(PointCloud<PointXYZRGB>::Ptr pointcloud)
{
	std::vector<int> indices;
	removeNaNFromPointCloud(*pointcloud, *pointcloud, indices);
}

void CloudHandler::clipplane(PointCloud<PointXYZ>::Ptr pointcloud, double xdir, double ydir, double zdir, double distance)
{
	ExtractIndices<PointXYZ> extract;
	Eigen::Vector4f plane;
	plane(0) = xdir;
	plane(1) = ydir;
	plane(2) = zdir;
	plane(3) = distance;
	PlaneClipper3D<PointXYZ> planeclipper(plane);
	std::vector<int> clipped;
	planeclipper.clipPointCloud3D(*pointcloud, clipped);

	auto indices = new IndicesPtr(&clipped);

	extract.setInputCloud(pointcloud);
	extract.setIndices(*indices);
	extract.filter(*pointcloud);
}

void CloudHandler::reducepoints(PointCloud<PointXYZ>::Ptr pointcloud, double size)
{
	VoxelGrid<PointXYZ> sor;
	sor.setInputCloud(pointcloud);
	sor.setLeafSize(size, size, size);
	sor.filter(*pointcloud);
}

void CloudHandler::smooth(PointCloud<PointXYZ>::Ptr pointcloud, double size)
{
	PointCloud<PointNormal>::Ptr mls_points (new PointCloud<PointNormal>);
	MovingLeastSquares<PointXYZ, PointNormal> mls;
	search::KdTree<PointXYZ>::Ptr tree(new search::KdTree<PointXYZ>);
	mls.setComputeNormals(false);
	mls.setInputCloud(pointcloud);
	mls.setPolynomialFit(true);
	mls.setSearchMethod(tree);
	mls.setSearchRadius(size);
	mls.process(*mls_points);
	pointcloud->clear();
	copyPointCloud(*mls_points, *pointcloud);
}

void CloudHandler::transformPointCloud2(PCLPointCloud2::Ptr pointcloud, const Eigen::Matrix4f& matrix)
{
	PointCloud<PointXYZ>::Ptr cloud(new PointCloud<PointXYZ>);
	fromPCLPointCloud2(*pointcloud, *cloud);
	CloudHandler::transform(cloud, matrix);
	toPCLPointCloud2(*cloud, *pointcloud);
}

void CloudHandler::transformPolygonMesh(pcl::PolygonMesh& mesh, Eigen::Matrix4f& matrix)
{
	PCLPointCloud2::Ptr cloudptr(&mesh.cloud);
	CloudHandler::transformPointCloud2(cloudptr, matrix);
}

ModelCoefficients CloudHandler::create_cone(double size, Eigen::Matrix4f transformationmatrix)
{
	PointXYZ position(0.0, 0.0, 0.0);
	PointXYZ direction(0.0, 0.0, size);

	PointCloud<PointXYZ>::Ptr cloud_pos(new PointCloud<PointXYZ>);
	PointCloud<PointXYZ>::Ptr cloud_dir(new PointCloud<PointXYZ>);
	cloud_pos->push_back(position);
	cloud_dir->push_back(direction);

	CloudHandler::transform(cloud_pos, transformationmatrix);
	
	transformationmatrix(0, 3) = 0;
	transformationmatrix(1, 3) = 0;
	transformationmatrix(2, 3) = 0;

	CloudHandler::transform(cloud_dir, transformationmatrix);

	//cout << cloud_dir->at(0).x << " " << cloud_dir->at(0).y << " " << cloud_dir->at(0).z << endl;
	
	ModelCoefficients cone_coeff;
	cone_coeff.values.resize(7);    // We need 7 values
	cone_coeff.values[0] = cloud_pos->at(0).x;
	cone_coeff.values[1] = cloud_pos->at(0).y;
	cone_coeff.values[2] = cloud_pos->at(0).z;
	cone_coeff.values[3] = cloud_dir->at(0).x;
	cone_coeff.values[4] = cloud_dir->at(0).y;
	cone_coeff.values[5] = cloud_dir->at(0).z;
	cone_coeff.values[6] = 20; // degrees

	return cone_coeff;
}

Eigen::Matrix4f CloudHandler::get_rotatematrix(double x_angle, double y_angle, double z_angle)
{
	x_angle = x_angle * M_PI / 180.0;
	y_angle = y_angle * M_PI / 180.0;
	z_angle = z_angle * M_PI / 180.0;

	Eigen::Matrix4f rotate_x_matrix = Eigen::Matrix4f::Identity();
	rotate_x_matrix(1, 1) = cos(x_angle);
	rotate_x_matrix(1, 2) = - sin(x_angle);
	rotate_x_matrix(2, 1) = sin(x_angle);
	rotate_x_matrix(2, 2) = cos(x_angle);

	Eigen::Matrix4f rotate_y_matrix = Eigen::Matrix4f::Identity();
	rotate_y_matrix(0, 0) = cos(y_angle);
	rotate_y_matrix(0, 2) = sin(y_angle);
	rotate_y_matrix(2, 0) = - sin(y_angle);
	rotate_y_matrix(2, 2) = cos(y_angle);

	Eigen::Matrix4f rotate_z_matrix = Eigen::Matrix4f::Identity();
	rotate_z_matrix(0, 0) = cos(z_angle);
	rotate_z_matrix(0, 1) = -sin(z_angle);
	rotate_z_matrix(1, 0) = sin(z_angle);
	rotate_z_matrix(1, 1) = cos(z_angle);

	Eigen::Matrix4f rotatematrix = rotate_z_matrix * rotate_y_matrix * rotate_x_matrix;

	return rotatematrix;
}


Eigen::Matrix4f CloudHandler::get_translatematrix(double x_trans, double y_trans, double z_trans)
{
	Eigen::Matrix4f translatematrix = Eigen::Matrix4f::Identity();
	translatematrix(0, 3) = x_trans;
	translatematrix(1, 3) = y_trans;
	translatematrix(2, 3) = z_trans;
	return translatematrix;
}


Eigen::Matrix4f CloudHandler::get_scalematrix(double x_scale, double y_scale, double z_scale)
{
	Eigen::Matrix4f scalematrix = Eigen::Matrix4f::Identity();
	scalematrix(0, 0) = x_scale;
	scalematrix(1, 1) = y_scale;
	scalematrix(2, 2) = z_scale;
	return scalematrix;
}

PointCloud<PointXYZ> CloudHandler::create_sphere(double diameter, int pointcount)
{
	PointCloud<PointXYZ> sphere;

	double radius = diameter / 2.0;

	for (int i = 0; i < pointcount; i++)
	{
		double x = (double)rand() / RAND_MAX * 2 - 1;
		double y = (double)rand() / RAND_MAX * 2 - 1;
		double z = (double)rand() / RAND_MAX * 2 - 1;

		double length = sqrt(x*x + y*y + z*z) * radius;


		PointXYZ point;
		point.x = x / length;
		point.y = y / length;
		point.z = z / length;
		sphere.push_back(point);
	}

	sphere.width = pointcount;
	sphere.height = 1;
	return sphere;
}


void CloudHandler::printmatrix(Eigen::Matrix4f matrix)
{
	printf("Matrix: \n  %+.10f %+.10f %+.10f %+.10f \n  %+.10f %+.10f %+.10f %+.10f \n  %+.10f %+.10f %+.10f %+.10f \n  %+.10f %+.10f %+.10f %+.10f\n",
		matrix(0,0), matrix(0, 1), matrix(0, 2), matrix(0, 3),
		matrix(1, 0), matrix(1, 1), matrix(1, 2), matrix(1, 3),
		matrix(2, 0), matrix(2, 1), matrix(2, 2), matrix(2, 3),
		matrix(3, 0), matrix(3, 1), matrix(3, 2), matrix(3, 3));
}

void CloudHandler::printPointCloudBoundaries(pcl::PointCloud<pcl::PointXYZRGB>::Ptr pointcloud)
{
	PointXYZRGB min_pt, max_pt;
	getMinMax3D(*pointcloud, min_pt, max_pt);
	cout << "Boundaries: \n";
	cout << "x: " << min_pt.x << ", " << max_pt.x << "\n";
	cout << "y: " << min_pt.y << ", " << max_pt.y << "\n";
	cout << "z: " << min_pt.z << ", " << max_pt.z << endl;
}

PointCloud<PointXYZ> CloudHandler::create_orientationarrows(double size)
{
	PointCloud<PointXYZ> arrows;
	PointXYZ point;

	int stepcount = 20;
	double step = size / stepcount;
	double stepw = step * 0.7;
	double steph = step * 0.5;

	for (int i = 0; i < stepcount; i++)
	{
		point.x = i*step;
		point.y = 0;
		point.z = 0;
		arrows.push_back(point);
	}

	for (int i = 1; i < stepcount; i++)
	{
		point.x = 0;
		point.y = i*step;
		point.z = 0;
		arrows.push_back(point);
	}

	for (int i = 1; i < stepcount; i++)
	{
		point.x = 0;
		point.y = 0;
		point.z = i*step;
		arrows.push_back(point);
	}
	
	// X 
	arrows.push_back(PointXYZ(size + 7 * stepw, -3 * steph, 0));
	arrows.push_back(PointXYZ(size + 6 * stepw, -2 * steph, 0));
	arrows.push_back(PointXYZ(size + 5 * stepw, -1 * steph, 0));
	arrows.push_back(PointXYZ(size + 4 * stepw, 0, 0));
	arrows.push_back(PointXYZ(size + 3 * stepw, 1 * steph, 0));
	arrows.push_back(PointXYZ(size + 2 * stepw, 2 * steph, 0));
	arrows.push_back(PointXYZ(size + 1 * stepw, 3 * steph, 0));

	arrows.push_back(PointXYZ(size + 7 * stepw, 3 * steph, 0));
	arrows.push_back(PointXYZ(size + 6 * stepw, 2 * steph, 0));
	arrows.push_back(PointXYZ(size + 5 * stepw, 1 * steph, 0));
	arrows.push_back(PointXYZ(size + 3 * stepw, -1 * steph, 0));
	arrows.push_back(PointXYZ(size + 2 * stepw, -2 * steph, 0));
	arrows.push_back(PointXYZ(size + 1 * stepw, -3 * steph, 0));
		
	// Y
	arrows.push_back(PointXYZ(-3 * steph, size + 7 * stepw, 0));
	arrows.push_back(PointXYZ(-2 * steph, size + 6 * stepw, 0));
	arrows.push_back(PointXYZ(-1 * steph, size + 5 * stepw, 0));
	arrows.push_back(PointXYZ(0 * steph, size + 4 * stepw, 0));
	arrows.push_back(PointXYZ(1 * steph, size + 5 * stepw, 0));
	arrows.push_back(PointXYZ(2 * steph, size + 6 * stepw, 0));
	arrows.push_back(PointXYZ(3 * steph, size + 7 * stepw, 0));
	arrows.push_back(PointXYZ(0 * steph, size + 3 * stepw, 0));
	arrows.push_back(PointXYZ(0 * steph, size + 2 * stepw, 0));
	arrows.push_back(PointXYZ(0 * steph, size + 1 * stepw, 0));

	//Z
	arrows.push_back(PointXYZ(0, -3 * steph, size + 7 * stepw));
	arrows.push_back(PointXYZ(0, -2 * steph, size + 7 * stepw));
	arrows.push_back(PointXYZ(0, -1 * steph, size + 7 * stepw));
	arrows.push_back(PointXYZ(0, 0 * steph, size + 7 * stepw));
	arrows.push_back(PointXYZ(0, 1 * steph, size + 7 * stepw));
	arrows.push_back(PointXYZ(0, 2 * steph, size + 7 * stepw));
	arrows.push_back(PointXYZ(0, 3 * steph, size + 7 * stepw));
	arrows.push_back(PointXYZ(0, 2 * steph, size + 6 * stepw));
	arrows.push_back(PointXYZ(0, 1 * steph, size + 5 * stepw));
	arrows.push_back(PointXYZ(0, 0 * steph, size + 4 * stepw));
	arrows.push_back(PointXYZ(0, -1 * steph, size + 3 * stepw));
	arrows.push_back(PointXYZ(0, -2 * steph, size + 2 * stepw));
	arrows.push_back(PointXYZ(0, -3 * steph, size + 1 * stepw));
	arrows.push_back(PointXYZ(0, -2 * steph, size + 1 * stepw));
	arrows.push_back(PointXYZ(0, -1 * steph, size + 1 * stepw));
	arrows.push_back(PointXYZ(0, 0 * steph, size + 1 * stepw));
	arrows.push_back(PointXYZ(0, 1 * steph, size + 1 * stepw));
	arrows.push_back(PointXYZ(0, 2 * steph, size + 1 * stepw));
	arrows.push_back(PointXYZ(0, 3 * steph, size + 1 * stepw));


	return arrows;
}