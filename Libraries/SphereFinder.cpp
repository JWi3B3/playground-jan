#include "SphereFinder.h"
#include <pcl/common/common_headers.h>
#include <pcl/features/normal_3d.h>
#include <vector>
#include <pcl/sample_consensus/ransac.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/passthrough.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>


SphereFinder::SphereFinder(double diameter, double tolerance)
{
	seg.setOptimizeCoefficients(true);
	seg.setModelType(pcl::SACMODEL_SPHERE);
	seg.setMethodType(pcl::SAC_RANSAC);
	seg.setMaxIterations(10000);
	seg.setDistanceThreshold(0.05);
	double minradius = diameter / 2 - tolerance / 2;
	double maxradius = diameter / 2 + tolerance / 2;
	seg.setRadiusLimits(minradius, maxradius);
	seg.setProbability(.99);
}


SphereFinder::~SphereFinder()
{

}

std::vector<double> SphereFinder::find(const pcl::PointCloud<pcl::PointXYZ>::Ptr cloud)
{
	pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> ne;
	pcl::search::KdTree<pcl::PointXYZ>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZ>());

	pcl::PointCloud<pcl::Normal> cloud_normals;

	// Estimate point normals
	ne.setSearchMethod(tree);
	ne.setInputCloud(cloud);
	ne.setKSearch(50);
	ne.compute(cloud_normals);

	seg.setInputCloud(cloud);
	seg.setInputNormals(cloud_normals.makeShared());

	seg.segment(inliers, coefficients);

	if (inliers.indices.size() == 0)
	{
		std::vector<double> falseparams = { -1, -1, -1, -1 };
		return falseparams;
	}

	std::vector<double> params;

	for (int i = 0; i < 4; i++)
		params.push_back(coefficients.values[i]);

	return params;
}