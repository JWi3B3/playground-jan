#define _USE_MATH_DEFINES

#include "opencv2\opencv.hpp"
#include "ImageDisplay.hpp"
#include <iostream>
#include <vector>
#include <string>
#include <math.h>
#include "Timer.hpp"

void main()
{
    auto image = cv::imread("image.png", cv::IMREAD_ANYCOLOR);

    int windowsize = 31;
    cv::Mat tempimage, mask;
    double meanval = 0;

    cv::Rect roi = cv::Rect(100, 100, windowsize, windowsize);

    tempimage = image(roi).clone();
    mask = tempimage > 0;
    mask.convertTo(mask, CV_8UC1);
    meanval = cv::mean(tempimage, mask)[0];
}