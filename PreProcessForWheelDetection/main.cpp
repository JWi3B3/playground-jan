#include <opencv2\opencv.hpp>

cv::Mat preprocess(cv::Mat &image, int threshold, int blurSize, int thresh1, int thresh2, int gauss, int appertureSize, int gradientQuality)
{
    blurSize = blurSize * 2 + 1;
    gauss = gauss * 2 + 1;
    appertureSize = appertureSize * 2;

    cv::Mat processedImage = image.clone();
    cv::blur(processedImage, processedImage, cv::Size(blurSize, blurSize));

    cv::threshold(processedImage, processedImage, threshold, 255, CV_THRESH_BINARY_INV);

    cv::Canny(processedImage, processedImage, thresh1, thresh2, 3 + appertureSize, bool(gradientQuality));

    cv::GaussianBlur(processedImage, processedImage, cv::Size(gauss, gauss), 2, 2);
    return processedImage;
}


void main()
{
    auto image = cv::imread("felge.png", cv::IMREAD_GRAYSCALE);
    cv::namedWindow("Image");

    int threshold = 0;
    int blurSize = 1;
    int gauss = 1;
    int thresh1 = 0;
    int thresh2 = 0;
    int appertureSize = 0;
    int gradientQuality = 0;


    cv::createTrackbar("Tresh", "Image", &threshold, 255);
    cv::createTrackbar("Blur", "Image", &blurSize, 100);
    cv::createTrackbar("Tresh1", "Image", &thresh1, 255);
    cv::createTrackbar("Tresh2", "Image", &thresh2, 255);
    cv::createTrackbar("Gauss", "Image", &gauss, 100);
    cv::createTrackbar("Apperture", "Image", &appertureSize, 2);
    cv::createTrackbar("gradientQuality", "Image", &gradientQuality, 1);



    cv::Mat processedImage;

    while (true)
    {
        processedImage = preprocess(image, threshold, blurSize, thresh1, thresh2, gauss, appertureSize, gradientQuality);

        cv::imshow("Image", processedImage);
        if (cv::waitKey(10) == 27)
            break;
    }
}