#define _USE_MATH_DEFINES

#include "Slider.h"
#include "opencv2\opencv.hpp"
#include "ImageDisplay.hpp"
#include <chrono>
#include <iostream>
#include <vector>
#include <string>
#include <thread>
#include <math.h>
#include <queue>
#include "Timer.hpp"

cv::Mat autoscale(cv::Mat& image, int opencvtype = CV_8UC1, double minimum = -1, double maximum = -1)
{
    cv::Mat dispimage;

    double minval, maxval;
    minMaxIdx(image, &minval, &maxval);

    if (minimum == -1) minimum = minval;
    if (maximum == -1) maximum = maxval;

    double typemax = 256.;
    if (opencvtype == CV_8UC1)
        typemax = 256.;
    else if (opencvtype == CV_16U)
        typemax = 65536;

    double a = typemax / (maximum - minimum);
    double b = -a * minimum;

    image.convertTo(dispimage, opencvtype, a, b);

    return dispimage;
}

cv::Mat combineFilterImages(std::vector<cv::Mat> images1, std::vector<cv::Mat> images2)
{
    cv::Mat combined = cv::Mat::zeros(images1[0].rows + images2[0].rows, images1[0].cols * images1.size(), CV_8UC1);

    if (images1.size() != images2.size())
    {
        std::cout << "combineFilterImages: Different vector sizes!" << std::endl;
        system("pause");
        return cv::Mat::zeros(100,100, CV_8UC1);
    }

    for (int i = 0; i < images1.size(); i++)
    {
        images1[i] = autoscale(images1[i]);
        images2[i] = autoscale(images2[i]);

        images1[i].copyTo(combined(cv::Rect(i*images1[0].cols, 0, images1[0].cols, images1[0].rows)));
        images2[i].copyTo(combined(cv::Rect(i*images1[0].cols, images1[0].rows, images1[0].cols, images1[0].rows)));
    }

    return combined;
}

void blobFilter(cv::Mat & inOutImage, int minsize = 0, int maxsize = 10000000)
{
    std::vector<std::vector<cv::Point> > contours;
    std::vector<cv::Vec4i> hierarchy;

    cv::Mat inImage = inOutImage.clone();
    cv::Mat outImage = cv::Mat::zeros(inImage.size(), inImage.type());

    cv::findContours(inImage, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

    for (int i = 0; i < contours.size(); i++)
    {
        //std::cout << hierarchy[i][0] << " " << hierarchy[i][1] << " " << hierarchy[i][2] << " " << hierarchy[i][3] << std::endl;

        // Loecher aussortieren
        if (hierarchy[i][3] >= 0)
            continue;
        
        if (cv::contourArea(contours[i]) >= minsize && cv::contourArea(contours[i]) <= maxsize)
        {
            cv::drawContours(outImage, contours, i, 255, -1);
        }
    }
    //cv::imshow("outimage", inOutImage);

    inOutImage = outImage.clone();
}

void gaborFilter(cv::Mat & inImage, cv::Mat & outImage, int ksize, double sigma, double theta, double lambda, double gamma, double psi)
{
    cv::Mat kernel = cv::getGaborKernel(cv::Size(ksize*2+1, ksize * 2 + 1), sigma, theta, lambda, gamma, psi, CV_32F);

    cv::resize(kernel, kernel, cv::Size(), 20, 20, cv::INTER_NEAREST);

    cv::Mat inImage2;
    inImage.convertTo(inImage2, CV_32F);

    cv::filter2D(inImage2, outImage, -1, autoscale(kernel));
    cv::imshow("kernel", kernel);
    //ImageDisplay::show(outImage, "filtered Image");
    outImage = cv::abs(outImage);
    cv::imshow("filtered image", autoscale(outImage));

    cv::waitKey(1);
}

double log2zero(double number)
{
    if (number != 0)
    {
        //std::cout << "other------" << std::endl;
        return std::log2(number);
    }
    else
    {
        //std::cout << "zero!!" << std::endl;
        return 0.0;
    }
}

std::vector<cv::Mat> cooccurrenceFilter(cv::Mat & inImage, int sizeX, int sizeY)
{
    cv::Mat coMatrix = cv::Mat::zeros(256, 256, CV_16UC1);
    cv::Mat coMatrixDisplay;

    cv::Mat imageEnergy = cv::Mat::zeros(inImage.rows - sizeY, inImage.cols - sizeX, CV_64F);
    cv::Mat imageEntropy = cv::Mat::zeros(inImage.rows - sizeY, inImage.cols - sizeX, CV_64F);
    cv::Mat imageContrast = cv::Mat::zeros(inImage.rows - sizeY, inImage.cols - sizeX, CV_64F);
    cv::Mat imageHomogeneity = cv::Mat::zeros(inImage.rows - sizeY, inImage.cols - sizeX, CV_64F);
    cv::Mat imageCorrelation = cv::Mat::zeros(inImage.rows - sizeY, inImage.cols - sizeX, CV_64F);

    double* ptr_energy;
    double* ptr_entropy;
    double* ptr_contrast;
    double* ptr_homogeneity;
    double* ptr_correlation;

    double energy = 0, entropy = 0, contrast = 0, homogeneity = 0, correlation = 0;

    uchar *inptr[256];
    uchar *outptr[256];

    double windowsize = sizeY * sizeX;

    double startVal = 0;
    double InVal = 0;
    double OutVal = 0;
    std::queue<double> energyQueue, entropyQueue, contrastQueue, homogeneityQueue, correlationQueue;

    auto mytime = Timer::Timer();
    mytime.start();    


    for (int y =0; y < inImage.rows - sizeY; y++)
    {
        // Fill co-occurrence matrix
        coMatrix *= 0;
        energy = 0; entropy = 0; contrast = 0; homogeneity = 0; correlation = 0;

        uchar* p;
        
        p = inImage.ptr<uchar>(y, 0);

        for (int x1 = 0; x1 < sizeX; x1++)
        {
            for (int y1 = 0; y1 < sizeY; y1++)
            {
                coMatrix.at<short>(p[x1 + y1*inImage.cols], p[x1 + 1 + y1*inImage.cols]) += 1;

                startVal = coMatrix.at<short>(p[x1 + y1*inImage.cols], p[x1 + 1 + y1*inImage.cols]);

                energy += cv::pow(startVal / windowsize, 2);
                energy -= cv::pow((startVal -1)/ windowsize, 2);

                entropy += log2zero(startVal) * startVal;
                entropy -= log2zero(startVal) * (startVal - 1);

                contrast += std::pow(p[x1] - p[x1 + 1], 2) * startVal;
                contrast -= std::pow(p[x1] - p[x1 + 1], 2) * (startVal - 1);
            }

            energyQueue.push(energy);
            entropyQueue.push(entropy);
            contrastQueue.push(contrast);

            energy = 0; entropy = 0; contrast = 0;
        }

        for (int i = 0; i < sizeY; i++)
        {
            inptr[i] = inImage.ptr<uchar>(y + i, sizeX);
            outptr[i] = inImage.ptr<uchar>(y + i, 0);
        }

        ptr_energy = imageEnergy.ptr<double>(y, 0);
        ptr_entropy = imageEntropy.ptr<double>(y, 0);
        ptr_contrast = imageContrast.ptr<double>(y, 0);
        ptr_homogeneity = imageHomogeneity.ptr<double>(y, 0);
        ptr_correlation = imageCorrelation.ptr<double>(y, 0);

        ptr_energy[0] = energy;
        ptr_entropy[0] = entropy;

        for (int x = 0; x < inImage.cols - sizeX; x++)
        {
            // Update co-occurrence matrix
            for (int i = 0; i < sizeY; i++)
            {
                coMatrix.at<short>(
                    inptr[i][x],
                    inptr[i][x + 1]) += 1;
                coMatrix.at<short>(
                    outptr[i][x],
                    outptr[i][x + 1]) -= 1;

                InVal = coMatrix.at<short>(inptr[i][x], inptr[i][x + 1]);
                OutVal = coMatrix.at<short>(outptr[i][x], outptr[i][x + 1]);

                energy += std::pow(InVal / windowsize, 2);
                energy -= std::pow((InVal - 1) / windowsize, 2);
                energy += std::pow((OutVal) / windowsize, 2);
                energy -= std::pow((OutVal + 1) / windowsize, 2);

                entropy += log2zero(InVal) * InVal;
                entropy -= log2zero(InVal - 1) * (InVal - 1);
                entropy += log2zero(OutVal) * OutVal;
                entropy -= log2zero(OutVal + 1) * (OutVal +1);

                contrast += std::pow(inptr[i][x] - inptr[i][x + 1], 2) * InVal;
                contrast -= std::pow(inptr[i][x] - inptr[i][x + 1], 2) * (InVal - 1);
                contrast += std::pow(outptr[i][x] - outptr[i][x + 1], 2) * OutVal;
                contrast -= std::pow(outptr[i][x] - outptr[i][x + 1], 2) * (OutVal + 1);
            }

            ptr_energy[x + 1] = energy;
            ptr_entropy[x + 1] = entropy;
            ptr_contrast[x + 1] = contrast;
        }
    }

    mytime.stop();
    std::cout << "Finish!!" << std::endl;

    std::vector<cv::Mat> outImages;
    outImages.push_back(imageEnergy.clone());
    outImages.push_back(imageEntropy.clone());
    outImages.push_back(imageContrast.clone());
    outImages.push_back(imageHomogeneity.clone());
    outImages.push_back(imageCorrelation.clone());

    return outImages;
}


void main()
{
    system("pause");

    int imagecount = 1;
    std::vector<cv::Mat> images;

    //for (int i = 0; i < imagecount; i++)
    //{
    //    std::string filename = "bluredge" + std::to_string(i) + ".png";
    //    std::cout << filename << std::endl;
    //    images.push_back(cv::imread(filename, cv::IMREAD_GRAYSCALE).clone());
    //}

    images.push_back(cv::imread("roi.png", cv::IMREAD_GRAYSCALE).clone());

    int scale = 100;

    Slider slider_ksize = Slider("Settings2", "ksize", 0, 50, 26, 50);
    Slider slider_sigma = Slider("Settings2", "sigma", 0, 30, 10, 100);
    Slider slider_theta = Slider("Settings2", "theta", 0, M_PI * 2, 0.5, 100);
    Slider slider_lambda = Slider("Settings2", "lambda", 0, 100, 7, 101);
    Slider slider_gamma = Slider("Settings2", "gamma", 0, 1, 0.1, 100);
    Slider slider_psi = Slider("Settings2", "psi", 0, 10, 1, 100);

    int filtersize = 1;

    auto images1 = cooccurrenceFilter(images[0], filtersize, filtersize);
    cv::transpose(images[0], images[0]);
    auto images2 = cooccurrenceFilter(images[0], filtersize, filtersize);

    for (int i = 0; i < images2.size(); i++)
    {
        cv::transpose(images2[i], images2[i]);
    }

    auto combined = combineFilterImages(images1, images2);

    cv::imshow("combined", combined);

    cv::waitKey();

    //cv::Mat currentimage, displayimage;
    //
    //while (true)
    //{
    //    currentimage = images[0].clone();
    //
    //    //cv::threshold(currentimage, currentimage, thresh, 255, CV_THRESH_BINARY);
    //
    //    //blobFilter(currentimage, minarea, 10000000);
    //
    //    gaborFilter(currentimage, currentimage, slider_ksize.getValue(), slider_sigma.getValue(), slider_theta.getValue(), slider_lambda.getValue(), slider_gamma.getValue(), slider_psi.getValue());
    //
    //    double scalefactor = (scale + 20.) / 100.;
    //    cv::resize(currentimage, displayimage, cv::Size(), scalefactor, scalefactor, cv::INTER_CUBIC);
    //    cv::imshow("Edge Remove Tool", displayimage);
    //
    //
    //    //cv::waitKey();
    //
    //    if (cv::waitKey(1) == 27)
    //        break;
    //}

    cv::destroyAllWindows();
}

