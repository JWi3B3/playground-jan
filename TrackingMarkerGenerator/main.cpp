#include "opencv2\opencv.hpp"
#include <hpdf.h>
#include <vector>

using namespace cv;

void drawImage(HPDF_Doc pdf, cv::Mat cvImage, float x, float y, float width, float height)
{
    float imwidth, imheight;

    HPDF_Page page = HPDF_GetCurrentPage(pdf);
    HPDF_Image image, mask;
    cv::Mat cvMask;

    cv::imwrite("pdfimage.png", cvImage*255);
    image = HPDF_LoadPngImageFromFile(pdf, "pdfimage.png");


    HPDF_Page_DrawImage(page, image, x, y, width, height);
}


std::vector<cv::Mat> generateMarkers()
{
    std::vector<cv::Mat> markers;

    Mat pattern;
    uchar data0[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data0);
    markers.push_back(pattern.clone());

    uchar data1[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data1);
    markers.push_back(pattern.clone());

    uchar data2[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data2);
    markers.push_back(pattern.clone());

    uchar data3[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data3);
    markers.push_back(pattern.clone());

    uchar data4[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data4);
    markers.push_back(pattern.clone());

    uchar data5[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data5);
    markers.push_back(pattern.clone());

    uchar data6[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data6);
    markers.push_back(pattern.clone());

    uchar data7[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data7);
    markers.push_back(pattern.clone());

    uchar data8[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data8);
    markers.push_back(pattern.clone());

    uchar data9[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data9);
    markers.push_back(pattern.clone());

    uchar data10[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data10);
    markers.push_back(pattern.clone());

    uchar data11[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data11);
    markers.push_back(pattern.clone());

    uchar data12[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data12);
    markers.push_back(pattern.clone());

    uchar data13[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data13);
    markers.push_back(pattern.clone());

    uchar data14[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data14);
    markers.push_back(pattern.clone());

    uchar data15[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data15);
    markers.push_back(pattern.clone());

    uchar data16[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data16);
    markers.push_back(pattern.clone());

    uchar data17[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data17);
    markers.push_back(pattern.clone());

    uchar data18[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data18);
    markers.push_back(pattern.clone());

    uchar data19[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data19);
    markers.push_back(pattern.clone());

    uchar data20[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data20);
    markers.push_back(pattern.clone());

    uchar data21[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data21);
    markers.push_back(pattern.clone());

    uchar data22[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data22);
    markers.push_back(pattern.clone());

    uchar data23[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data23);
    markers.push_back(pattern.clone());

    uchar data24[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data24);
    markers.push_back(pattern.clone());

    uchar data25[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data25);
    markers.push_back(pattern.clone());

    uchar data26[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data26);
    markers.push_back(pattern.clone());

    uchar data27[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data27);
    markers.push_back(pattern.clone());

    uchar data28[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data28);
    markers.push_back(pattern.clone());

    uchar data29[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data29);
    markers.push_back(pattern.clone());

    uchar data30[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data30);
    markers.push_back(pattern.clone());

    uchar data31[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data31);
    markers.push_back(pattern.clone());

    uchar data32[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data32);
    markers.push_back(pattern.clone());

    uchar data33[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data33);
    markers.push_back(pattern.clone());

    uchar data34[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data34);
    markers.push_back(pattern.clone());

    uchar data35[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data35);
    markers.push_back(pattern.clone());

    uchar data36[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data36);
    markers.push_back(pattern.clone());

    uchar data37[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data37);
    markers.push_back(pattern.clone());

    uchar data38[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data38);
    markers.push_back(pattern.clone());

    uchar data39[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data39);
    markers.push_back(pattern.clone());

    uchar data40[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data40);
    markers.push_back(pattern.clone());

    uchar data41[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data41);
    markers.push_back(pattern.clone());

    uchar data42[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data42);
    markers.push_back(pattern.clone());

    uchar data43[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data43);
    markers.push_back(pattern.clone());

    uchar data44[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data44);
    markers.push_back(pattern.clone());

    uchar data45[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data45);
    markers.push_back(pattern.clone());

    uchar data46[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data46);
    markers.push_back(pattern.clone());

    uchar data47[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data47);
    markers.push_back(pattern.clone());

    uchar data48[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data48);
    markers.push_back(pattern.clone());

    uchar data49[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data49);
    markers.push_back(pattern.clone());

    uchar data50[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data50);
    markers.push_back(pattern.clone());

    uchar data51[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data51);
    markers.push_back(pattern.clone());

    uchar data52[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data52);
    markers.push_back(pattern.clone());

    uchar data53[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data53);
    markers.push_back(pattern.clone());

    uchar data54[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data54);
    markers.push_back(pattern.clone());

    uchar data55[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data55);
    markers.push_back(pattern.clone());

    uchar data56[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data56);
    markers.push_back(pattern.clone());

    uchar data57[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data57);
    markers.push_back(pattern.clone());

    uchar data58[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data58);
    markers.push_back(pattern.clone());

    uchar data59[6][6] =
    {
        { 0, 0, 0, 0, 0, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 1, 1, 1, 1, 0 },
        { 0, 0, 1, 1, 1, 0 },
        { 0, 0, 0, 0, 0, 0 },
    };
    pattern = Mat(6, 6, CV_8UC1, data59);
    markers.push_back(pattern.clone());

    return markers;
}


void errorHandler(HPDF_STATUS error_no, HPDF_STATUS detail_no, void *user_data)
{
    /* throw exception when an error has occured */
    std::cout << "PDF ERROR: error_no= " << static_cast<unsigned int>(error_no) << "detail_no=" << static_cast<int>(detail_no) << std::endl;
    throw std::exception();
}

void main()
{

    jmp_buf     _env;
    HPDF_Doc    pdf;

    std::vector<bool> binary;

    pdf = HPDF_New(errorHandler, NULL);
    if (!pdf) {
        printf("error: cannot create PdfDoc object\n");
        return;
    }

    // Error Handling
    if (setjmp(_env)) {
        HPDF_Free(pdf);
        return;
    }

    HPDF_Page   _page;

    auto markers = generateMarkers();

    char        _filename[256];
    std::string filename = "Markers.pdf";
    strcpy(_filename, filename.c_str());
    HPDF_Destination dst;


    for (int i = 0; i < markers.size(); i++)
    {
        // New Page
        _page = HPDF_AddPage(pdf);

        // Set Size to DinA4
        HPDF_Page_SetSize(_page, HPDF_PAGE_SIZE_A4, HPDF_PAGE_PORTRAIT);

        dst = HPDF_Page_CreateDestination(_page);
        HPDF_Destination_SetXYZ(dst, 0, HPDF_Page_GetHeight(_page), 1);
        HPDF_SetOpenAction(pdf, dst);

        // Draw all the stuff
        cv::resize(markers[i], markers[i], cv::Size(2000, 2000), 0, 0, cv::INTER_NEAREST);
        drawImage(pdf, markers[i], 54, 54, 425, 425);
    }

    // save the document to a file
    HPDF_SaveToFile(pdf, _filename);

    // clean up
    HPDF_Free(pdf);

}