#include <opencv2\opencv.hpp>
#include <opencv2\viz.hpp>
#include <opencv2\calib3d.hpp>
#include <chrono>
#include <thread>
#include <iostream>
#include "OrbbecDevice.hpp"
#include "MarkerDetection.hpp"
#include "ImageDisplay.hpp"


void count3DPoints(cv::Mat & cloud)
{
    cv::Mat cloudcounter;
    cloud.convertTo(cloudcounter, CV_8UC3, 255);
    cv::cvtColor(cloudcounter, cloudcounter, cv::COLOR_BGR2GRAY);
    cv::threshold(cloudcounter, cloudcounter, 1, 1, CV_THRESH_BINARY);

    std::cout << "Cloud has " << cv::sum(cloudcounter)[0] << " Points. " << std::endl;
}

void main()
{
    double maxdepth = 5000.;

    OrbbecDevice::initialize();
    auto md = MarkerDetection();
    
    /// Create a window
    cv::viz::Viz3d myWindow("Coordinate Frame");

    /// Add coordinate axes
    //myWindow.showWidget("Coordinate Widget", cv::viz::WCoordinateSystem());

    // Nur zum Test
    //cv::viz::WCube cube_widget(cv::Point3f(0.5, 0.5, 0.0), cv::Point3f(0.0, 0.0, -0.5), true, cv::viz::Color::blue());
    //cube_widget.setRenderingProperty(cv::viz::LINE_WIDTH, 4.0);

    ///// Display widget (update if already displayed)
    //myWindow.showWidget("Cube Widget", cube_widget);

    auto cam1 = OrbbecDevice::create();
    auto cam2 = OrbbecDevice::create();

    std::cout << cam1->getMirror() << std::endl;

    std::cout << "Cam 1: " << cam1->isOpen() << std::endl;
    std::cout << "Cam 2: " << cam2->isOpen() << std::endl;

    cam1->startStream(OrbbecDevice::STREAMTYPE_DEPTH);
    cam2->startStream(OrbbecDevice::STREAMTYPE_DEPTH);

    cv::Mat frame1, frame2;
    cv::Mat cloud1 = cv::Mat::ones(10, 1, CV_32FC3);
    cv::Mat cloud2 = cloud1.clone();

    //cv::viz::WCameraPosition camPosition(0.5);


    cv::Vec3f cam_pos(0.0f, 0.0f, 0.0f), cam_focal_point(0.0f, 0.0f, 1.0f), cam_y_dir(-1.0f, 0.0f, 0.0f);
    cv::Affine3f cam_pose = cv::viz::makeCameraPose(cam_pos, cam_focal_point, cam_y_dir);

    auto camPosition = cv::viz::WCameraPosition(cv::Vec2f(0.8, 0.5), 200);
    myWindow.showWidget("camPos", camPosition);

    cv::viz::WCloud cloud_widget(cloud2, cv::viz::Color::green());
    myWindow.showWidget("cam2", cloud_widget);

    //cam1->setEmitter(false);
    //cam2->setEmitter(false);

    bool firsttime = true;

    while (true)
    {

        cam1->getFrame(frame1, 100);
        cam2->getFrame(frame2, 100);

        if (frame1.rows == 0 || frame2.rows == 0)
            continue;

        cam2->convertMatToWorld(frame2, cloud2, 1);

        //myWindow.removeWidget("cam2");
        //delete &cloud_widget;
        cv::viz::WCloud cloud_widget(cloud2, cv::viz::Color::green());
        myWindow.showWidget("cam2", cloud_widget);

        /// Visualize widget
        //myWindow.removeAllWidgets();

        frame1.convertTo(frame1, CV_8UC1, 255. / maxdepth);
        frame2.convertTo(frame2, CV_8UC1, 255. / maxdepth);

        //md.findMarkers(frame1, 128, false, false, false);
        //frame1 = md.getDebugImage();

        //md.findMarkers(frame2, 128, false, false, false);
        //frame2 = md.getDebugImage();

        cv::imshow("Frame1", frame1);
        cv::imshow("Frame2", frame2);
        cv::waitKey(1);
     
        myWindow.spinOnce(30, true);
        //myWindow.spin();

        firsttime = false;
    }
    cv::destroyAllWindows();
}

